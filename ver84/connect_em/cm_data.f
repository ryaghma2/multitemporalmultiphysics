c$Id:$
      subroutine cm_data ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

c      include   'cdata.h'      ! numnp,numel,nummat,nen

      logical   setvar,ualloc

      integer   nsbc_pi,nsbc_a
      integer   nnbsurf_j

      integer   sbc_pi(2,10000),sbc_a(7,10000)
      integer   ix_nbsurf_j(6,10000)

      save

      call setarray_cm ( )

      call initialize_array ( 
     &     hr(up(2)),
     &     hr(up(3)),hr(up(4)),
     &     hr(up(5)),hr(up(6)),
     &     hr(up(7)),hr(up(8)),
     &     hr(up(9)),hr(up(10)),
     &     hr(up(11)),hr(up(12))
     &     )

c     get x,ix data from feap
      call get_x_ix ( hr(np(43)),hr(up(2)),
     &     mr(np(33)),mr(up(1)) ) 

      call get_sbc_pi_a ( 
     &     hr(up(2)),mr(up(1)),
     &     nsbc_pi,nsbc_a,nnbsurf_j,
     &     sbc_pi,sbc_a,ix_nbsurf_j )

      call setarray_cm_1 ( )

      call get_sbc_pi_a_1 (
     &     nsbc_pi,nsbc_a,nnbsurf_j,
     &     sbc_pi,sbc_a,ix_nbsurf_j,
     &     mr(up(18)),mr(up(19)),mr(up(20)) )


      end
