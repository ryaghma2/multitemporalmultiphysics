c$Id:$
      subroutine transferdata_cm ( 
     &     u_feap,u_rate_feap,
     &     mechdisp_cm,mechdisp_1_cm,mechdisp_2_cm,
     &     mechvel_cm,mechacc_cm
     &     )

c     u_feap(ndm,numnp,1) : u_n+1
c     u_feap(ndm,numnp,2) : u_n+1 - u_n : increment

      implicit  none

      include   'cdata.h'
      include   'cm_const_data.h'

      include   'time_data.h'

      integer   i,j,k

      real*8    u_feap(3,numnp,3),u_rate_feap(3,numnp,2)
      
      real*8    mechdisp_cm(3,numnp_cm),mechdisp_1_cm(3,numnp_cm)
      real*8    mechdisp_2_cm(3,numnp_cm)
      real*8    mechvel_cm(3,numnp_cm),mechacc_cm(3,numnp_cm)

      save

      do i = 1,numnp_cm
         do j = 1,3
            mechdisp_cm(j,i) = u_feap(j,i,1)
            mechdisp_2_cm(j,i) = mechdisp_1_cm(j,i)
            mechdisp_1_cm(j,i) = u_feap(j,i,1) - u_feap(j,i,2)

            mechvel_cm(j,i) = (mechdisp_cm(j,i) - mechdisp_1_cm(j,i))/dt
            mechacc_cm(j,i) = (mechdisp_cm(j,i)
     &      - 2.0d0*mechdisp_1_cm(j,i) + mechdisp_2_cm(j,i))/(dt**2.0d0)

c            mechvel_cm(j,i) = u_rate_feap(j,i,1)
c            mechacc_cm(j,i) = u_rate_feap(j,i,2)
         end do 
      end do

c      write(*,*) u_feap(3,numnp,1),u_feap(3,numnp,2),u_feap(3,numnp,3),
c     &     (u_feap(3,numnp,1)-u_feap(3,numnp,2)))
c      write(*,*) mechvel_cm(3,14),mechacc_cm(3,14)

c      write(*,*) numnp_cm
c      write(*,*) 'mechdisp_cm'
c      do i = 1,numnp_cm
c         write(*,*) (mechdisp_cm(j,i),j=1,3)
c      end do
c      write(*,*) 'mehcvel_cm'
c      do i = 1,numnp_cm
c         write(*,*) (mechvel_cm(j,i),j=1,3)
c      end do
c      write(*,*) 'mechacc_cm'
c      do i = 1,numnp_cm
c         write(*,*) (mechacc_cm(j,i),j=1,3)
c      end do


      end
