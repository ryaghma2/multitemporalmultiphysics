c$Id:$
      subroutine get_sbc_pi_a_fs_1 (
     &     nsbc_pi,nsbc_a,nnbsurf,
     &     sbc_pi,sbc_a,ix_nbsurf,

     &     sbc_pi_fs,sbc_a_fs,ix_nbsurf_fs_j )

      implicit  none

      include   'fs_const_data.h'

      integer   i,j,k

      integer   nsbc_pi,nsbc_a,nnbsurf

      integer   sbc_pi(2,10000),sbc_a(7,10000)
      integer   ix_nbsurf(6,10000)

      integer   sbc_pi_fs(2,nsbc_pi_fs),sbc_a_fs(7,nsbc_a_fs)
      integer   ix_nbsurf_fs_j(6,nnbsurf_fs_j)   

      save

      do i = 1,nsbc_pi_fs
         do j = 1,2
            sbc_pi_fs(j,i) = sbc_pi(j,i)
         end do
      end do
      
      do i = 1,nsbc_a_fs
         do j = 1,7
            sbc_a_fs(j,i) = sbc_a(j,i)
         end do
      end do

      do i = 1,nnbsurf_fs_j
         do j = 1,6
            ix_nbsurf_fs_j(j,i) = ix_nbsurf(j,i)
         end do
      end do
      
c      write(*,*) nsbc_pi_fs
c      do i = 1,nsbc_pi_fs
c         write(*,*)(sbc_pi_fs(j,i),j=1,2)
c      end do

c      write(*,*) nsbc_a_fs
c      do i = 1,(nsbc_a_fs)
c         write(*,*)(sbc_a_fs(j,i),j=1,7)
c      end do

c      write(*,*) 'nnbsurf_fs_j',nnbsurf_fs_j
c      do i = 1,nnbsurf_fs_j
c         write(*,*)(ix_nbsurf_fs_j(j,i),j=1,6)
c      end do

      end
