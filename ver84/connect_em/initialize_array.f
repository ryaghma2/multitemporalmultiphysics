c$Id:$
      subroutine initialize_array (
     &     x_cm,
     &     a_n_1_cm,a_n_cm,
     &     d_a_n_cm,dd_a_n_cm,
     &     phi_n_cm,d_phi_n_cm,
     &     hfield_cm,dfield_cm,
     &     bfield_cm,efield_cm
     &     )

      implicit  none

      include   'cm_const_data.h'

      integer   i,j,k
      
      real*8    x_cm(3,numnp_cm)

      real*8    a_n_1_cm(3,numnp_cm),a_n_cm(3,numnp_cm)
      real*8    d_a_n_cm(3,numnp_cm),dd_a_n_cm(3,numnp_cm)

      real*8    phi_n_cm(numnp_cm),d_phi_n_cm(numnp_cm)

      real*8    hfield_cm(3,numnp_cm),dfield_cm(3,numnp_cm)
      real*8    bfield_cm(3,numnp_cm),efield_cm(3,numnp_cm)

      save

      do i = 1,numnp_cm
         do j = 1,3
            x_cm(j,i) = 0.0d0

            a_n_1_cm(j,i) = 0.0d0
            a_n_cm(j,i) = 0.0d0
            d_a_n_cm(j,i) = 0.0d0
            dd_a_n_cm(j,i) = 0.0d0

            hfield_cm(j,i) = 0.0d0
            dfield_cm(j,i) = 0.0d0
            bfield_cm(j,i) = 0.0d0
            efield_cm(j,i) = 0.0d0
         end do
         phi_n_cm(i) = 0.0d0
         d_phi_n_cm(i) = 0.0d0
      end do


      end
