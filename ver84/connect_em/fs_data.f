c$Id:$
      subroutine fs_data ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

c      include   'cdata.h'      ! numnp,numel,nummat,nen

      include   'cm_const_data.h'

      logical   setvar,ualloc

      integer   nsbc_pi,nsbc_a
      integer   nnbsurf_j

      integer   sbc_pi(2,10000),sbc_a(7,10000)
      integer   ix_nbsurf_j(6,10000)

      integer   numnp,numel

      integer   layers

      integer   ix(9,100000)

      real*8    x(3,100000)

      real*8   length(3)

      save

      length(1) = 0.5d0
      length(2) = 0.5d0
      length(3) = 0.25d0

      layers = 2

c     form x, ix for free space
      call get_x_ix_fs ( 
     &     hr(up(2)),numnp_cm,
     &     x,ix,numnp,numel,
     &     length,layers
     &     )

      call setarray_fs ( numnp,numel )

      call initialize_array_fs ( 
     &     hr(up(22)),
     &     hr(up(23)),hr(up(24)),
     &     hr(up(25)),hr(up(26)),
     &     hr(up(27)),hr(up(28)),
     &     hr(up(29)),hr(up(30)),
     &     hr(up(31)),hr(up(32))
     &     )

c     put x,ix data in array 
      call get_x_ix_fs_1 ( x,hr(up(22)),
     &     ix,mr(up(21)) ) 

c     need to check
      call get_sbc_pi_a_fs ( 
     &     hr(up(22)),mr(up(21)),
     &     nsbc_pi,nsbc_a,nnbsurf_j,
     &     sbc_pi,sbc_a,ix_nbsurf_j )

      call setarray_fs_1 ( )

      call get_sbc_pi_a_fs_1 (
     &     nsbc_pi,nsbc_a,nnbsurf_j,
     &     sbc_pi,sbc_a,ix_nbsurf_j,
     &     mr(up(38)),mr(up(39)),mr(up(40)) )


      end
