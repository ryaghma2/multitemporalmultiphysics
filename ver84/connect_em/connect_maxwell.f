c$Id:$
      subroutine connect_maxwell ( 
     &     mat_prop
     &    
     &     )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

      include   'cm_const_data.h' !
c      include   'fs_const_data.h'

c      include   'intfc_const_data.h'
      
      include   'time_data.h'   ! dt,ts

      logical   setvar,ualloc

c      integer   nintfcix ! dummy
c      integer   intfcix(4,1) ! dummy
c      integer   ix_nbsurf_cm(6,1),ix_nbsurf_fs(6,1) ! dummy

      real*8    mat_prop(3,2)

      real*8    mag_em,frq_em,w_em

      real*8    pi

      save

c      nintfcix = 1 ! dummy

      pi = 3.1415926535 ! pi
      
      mat_prop(1,1) = 8.854*10.d0**(-12.d0) ! ep_0 : free space permittivity
      mat_prop(2,1) = 4.0d0*pi*10.d0**(-7.d0) ! mu_0 : free space permeability
      mat_prop(3,1) = 1.0d0     ! to avoid singularity
      mat_prop(1,2) = 8.0d0*8.854*10.d0**(-12.d0) ! ep_r*ep_0 : aluminium
      mat_prop(2,2) = 1.256665*10.d0**(-6.d0) ! mu : aluminium
      mat_prop(3,2) = 37.8*10.d0**(6.d0) ! sigma : aluminium conductivity

      mag_em = 100              ! mag. of current flux
      frq_em = 200      ! Hz
      w_em = 2.0d0*pi*frq_em        ! angular velocity
c     dt_em = 1.0d0/(frq_em*2*10) % sec


c     hsk_fem_maxwell
      call hsk_fem_maxwell_3d (
     &     mat_prop,
     &     ts,dt,
     &     w_em,mag_em,

     &     numnp_cm,numel_cm,
     &     hr(up(2)),mr(up(1)),
     &     hr(up(13)),hr(up(14)),
     &     hr(up(15)),hr(up(16)),
     &     nsbc_pi_cm,nsbc_a_cm,
     &     mr(up(18)),mr(up(19)),
     &     nnbsurf_cm_j,
     &     mr(up(20)),
     &     hr(up(3)),hr(up(4)),hr(up(5)),hr(up(6)),
     &     hr(up(7)),hr(up(8)),
     &     hr(up(9)),hr(up(10)),
     &     hr(up(11)),hr(up(12))
     &     )
      
      

      end
