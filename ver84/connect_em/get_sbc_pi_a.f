c$Id:$
      subroutine get_sbc_pi_a (
     &     x,ix,
     &     nsbc_pi,nsbc_a,nnbsurf,
     &     sbc_pi,sbc_a,ix_nbsurf )

      implicit  none

      include   'cm_const_data.h'

      include   'lmn_data_cm.h'

      integer   i,j,k,n,node,flag,nnnbsurf,m1,m2

      integer   nsbc_pi,nsbc_a,nnbsurf

      integer   sbc_pi(2,10000),sbc_a(7,10000)
      integer   ix_nbsurf(6,10000)

      integer   ix(9,numel_cm)

      real*8    x(3,numnp_cm)

      real*8    max_x(3),min_x(3)
      real*8    lx,ly,lz

      logical   eqoperator

      save

c     get max & min in x, y, z direc.
      do i = 1,3
         max_x(i) = 0.0d0
         min_x(i) = 0.0d0
      end do

      do i = 1,numnp_cm
         do j = 1,3
            if( x(j,i) .gt. max_x(j) ) then
               max_x(j) = x(j,i)
            endif
            if( x(j,i) .lt. min_x(j) ) then
               min_x(j) = x(j,i)
            endif
         end do
      end do
      
c     get lx,ly,lz
      lx = (max_x(1) - min_x(1))/nx
      ly = (max_x(2) - min_x(2))/ny
      lz = (max_x(3) - min_x(3))/nz       

c      write(*,*) nx,ny,nz
c      write(*,*) lx,ly,lz

c     specify natural boundary surfaces ccccccccccccccccccccccccccccccccccccc
      n = 0
      do i = 1,numel_cm
         if(ix(9,i) .eq. 2) then
            node = ix(1,i)
            if(x(2,node) .eq. 0.0d0) then
               n = n + 1 
c               do j = 1,4
                  ix_nbsurf(1,n) = ix(1,i)
                  ix_nbsurf(2,n) = ix(5,i)
                  ix_nbsurf(3,n) = ix(6,i)
                  ix_nbsurf(4,n) = ix(2,i)
c               end do
               ix_nbsurf(5,n) = 2 ! mat'l index
               ix_nbsurf(6,n) = -2 ! normal direc.
            endif
         endif
      end do
      nnbsurf = n ! number of nat.boundary surfaces
      nnbsurf_cm_j = nnbsurf

c      write(*,*) nnbsurf_cm_j
c      do i = 1,nnbsurf_cm_j
c         write(*,*)(ix_nbsurf(j,i),j=1,6)
c      end do

      n = 0
      do i = 1,numnp_cm
         flag = 0
         do j = 1,nnbsurf
            do k = 1,4
               node = ix_nbsurf(k,j)
               if(i .eq. node) then
                  flag = 1
               endif
            end do
         end do
         if(flag .eq. 1) then
            n = n + 1
         endif
      end do
      nnnbsurf = n  ! number of nodes on nat. boundary surfaces

c     specified boundary nodes cccccccccccccccccccccccccccccccccccccccccccccc
c     for pi
      nsbc_pi = (nx+1)*(nz+1)   !*(nz+1)-(nx-1)*(ny-1)*(nz-1) - nnnbsurf;
      nsbc_pi_cm = nsbc_pi

      n = 0
      do i = 1,numnp_cm
         flag = 1
         do j = 1,nnbsurf
            do k = 1,4
               node = ix_nbsurf(k,j)
               if(i .eq. node) then
                  flag = 0
               endif
            end do
         end do
         if(flag .eq. 1) then
            if(
     &           eqoperator( x(2,i), ny*ly )
     &           ) then
               n = n + 1
               sbc_pi(1,n) = i
               sbc_pi(2,n) = 0
            endif
         endif
      end do

c      write(*,*) nsbc_pi
c      do i = 1,nsbc_pi
c         write(*,*)(sbc_pi(j,i),j=1,2)
c      end do

c     for a
      nsbc_a = (nx+1)*(ny+1)*(nz+1)-(nx-1)*(ny-1)*(nz-1) ! - nnnbsurf;
      nsbc_a_cm = nsbc_a

      n = 1
      do i = 1,numnp_cm
         flag = 1
c         do j = 1,nnbsurf
c            do k = 1,4
c               node = ix_nbsurf(k,j)
c               if(i .eq. node) then
c                  flag = 0
c               endif
c            end do
c         end do
         if(flag .eq. 1) then
            if(
     &           eqoperator( x(1,i),0.0d0 ) 
     &           .or.
     &           eqoperator( x(1,i),nx*lx )
     &           .or.
     &           eqoperator( x(2,i),0.0d0 )
     &           .or.
     &           eqoperator( x(2,i),ny*ly )
     &           .or.
     &           eqoperator( x(3,i),0.0d0 )
     &           .or.
     &           eqoperator( x(3,i),nz*lz )
     &           ) then
               if( 
     &              eqoperator( x(1,i),0.0d0 )
     &              .or.
     &              eqoperator( x(1,i),nx*lx )
     &              ) then
                  sbc_a(1,n) = i
                  sbc_a(2,n) = 1
                  sbc_a(3,n) = 1
                  sbc_a(4,n) = 1
                  sbc_a(5,n) = 0
                  sbc_a(6,n) = 0
                  sbc_a(7,n) = 0
                  if(
     &                 eqoperator( x(2,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(2,i),ny*ly )
     &                 .or.
     &                 eqoperator( x(3,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(3,i),nz*lz )
     &                 ) then
                     sbc_a(1,n) = i
                     sbc_a(2,n) = 1
                     sbc_a(3,n) = 1
                     sbc_a(4,n) = 1
                     sbc_a(5,n) = 0
                     sbc_a(6,n) = 0
                     sbc_a(7,n) = 0
                  endif
               endif
               if( 
     &              eqoperator( x(2,i),0.0d0 )
     &              .or.
     &              eqoperator( x(2,i),ny*ly )
     &              ) then
                  sbc_a(1,n) = i
                  sbc_a(2,n) = 1
                  sbc_a(3,n) = 1
                  sbc_a(4,n) = 1
                  sbc_a(5,n) = 0
                  sbc_a(6,n) = 0
                  sbc_a(7,n) = 0
                  if( 
     &                 eqoperator( x(1,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(1,i),nx*lx )
     &                 .or.
     &                 eqoperator( x(3,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(3,i),nz*lz )
     &                 ) then
                     sbc_a(1,n) = i
                     sbc_a(2,n) = 1
                     sbc_a(3,n) = 1
                     sbc_a(4,n) = 1
                     sbc_a(5,n) = 0
                     sbc_a(6,n) = 0
                     sbc_a(7,n) = 0
                  endif
               endif
               if(
     &              eqoperator( x(3,i),0.0d0 )
     &              .or.
     &              eqoperator( x(3,i),nz*lz )
     &              ) then
                  sbc_a(1,n) = i
                  sbc_a(2,n) = 1
                  sbc_a(3,n) = 1
                  sbc_a(4,n) = 1
                  sbc_a(5,n) = 0
                  sbc_a(6,n) = 0
                  sbc_a(7,n) = 0
                  if(
     &                 eqoperator( x(1,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(1,i),nx*lx )
     &                 .or.
     &                 eqoperator( x(2,i),0.0d0 )
     &                 .or.
     &                 eqoperator( x(2,i),ny*ly )
     &                 ) then
                     sbc_a(1,n) = i
                     sbc_a(2,n) = 1
                     sbc_a(3,n) = 1
                     sbc_a(4,n) = 1
                     sbc_a(5,n) = 0
                     sbc_a(6,n) = 0
                     sbc_a(7,n) = 0
                  endif
               endif
               n = n + 1
            endif
         endif
      end do

c      write(*,*) nsbc_a
c      do i = 1,nsbc_a
c         write(*,*)(sbc_a(j,i),j=1,7)
c      end do
      
c     count dof will be removed ccccccccccccccccccccccccccccccccccccccccccccc
      m1 = nsbc_pi
      m2 = 0
      do i = 1,nsbc_a
         do j = 1,3
            flag = sbc_a(j+1,i)
            if(flag .eq. 1) then
               m2 = m2 + 1
            endif
         end do
      end do


      end

