c$Id:$
      subroutine setarray_fs ( numnp,numel )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

c      include   'cdata.h'      ! numnp,numel,nummat,nen

      include   'fs_const_data.h' ! numnp_cm, etc...

      logical    setvar,ualloc

      integer    ndm,numnp,numel

      save

      ndm = 3

      numnp_fs = numnp
      numel_fs = numel      

      setvar = ualloc( 21,'VAR20',9*(numel_fs),1) ! ix_fs
      setvar = ualloc( 22,'VAR21',ndm*(numnp_fs),2) ! x_fs

      setvar = ualloc( 23,'VAR22',ndm*(numnp_fs),2) ! a_n_1_fs
      setvar = ualloc( 24,'VAR23',ndm*(numnp_fs),2) ! a_n_fs
      setvar = ualloc( 25,'VAR24',ndm*(numnp_fs),2) ! d_a_n_fs
      setvar = ualloc( 26,'VAR25',ndm*(numnp_fs),2) ! dd_a_n_fs

      setvar = ualloc( 27,'VAR26',numnp_fs,2) ! phi_n_fs
      setvar = ualloc( 28,'VAR27',numnp_fs,2) ! d_phi_n_fs

      setvar = ualloc( 29,'VAR28',ndm*(numnp_fs),2) ! hfield_fs
      setvar = ualloc( 30,'VAR29',ndm*(numnp_fs),2) ! dfield_fs
      setvar = ualloc( 31,'VAR30',ndm*(numnp_fs),2) ! bfield_fs
      setvar = ualloc( 32,'VAR31',ndm*(numnp_fs),2) ! efield_fs

      setvar = ualloc( 33,'VAR32',ndm*(numnp_fs),2) ! mechvel_fs
      setvar = ualloc( 34,'VAR33',ndm*(numnp_fs),2) ! mechacc_fs
      setvar = ualloc( 35,'VAR34',ndm*(numnp_fs),2) ! mechdisp_fs
      setvar = ualloc( 36,'VAR35',ndm*(numnp_fs),2) ! mechdisp_1_fs

      end
