c$Id:$
      subroutine interface_em (
     &     ix_cm,x_cm,
     &     ix_fs,x_fs,
     &     intfcix,
     &     ix_nbsurf_cm,ix_nbsurf_fs
     & )

c     generate interface connectivity

      implicit  none

      include   'cm_const_data.h'
      include   'fs_const_data.h'

      include   'intfc_const_data.h'! nintfcix

      integer   cnx,i,j,k

      integer   ixx(8),ix1,ix2,ix3,ix4,ix5,ix6,ix7,ix8
      integer   iyy(8),iy1,iy2,iy3,iy4,iy5,iy6,iy7,iy8
      integer   izz(8),iz1,iz2,iz3,iz4,iz5,iz6,iz7,iz8

      integer   jxx(8),jx1,jx2,jx3,jx4,jx5,jx6,jx7,jx8
      integer   jyy(8),jy1,jy2,jy3,jy4,jy5,jy6,jy7,jy8
      integer   jzz(8),jz1,jz2,jz3,jz4,jz5,jz6,jz7,jz8

      integer   dirc,elemn

      integer   ix_cm(9,numel_cm),ix_fs(9,numel_fs)

      integer   intfcix(4,10000)
c      integer   nintfcix ! intfc_const_data.h
      integer   ix_nbsurf_cm(6,10000),ix_nbsurf_fs(6,10000)

      real*8    x_cm(3,numnp_cm),x_fs(3,numnp_fs)

      logical   eqoperator

      logical   flag(8),flag1,flag2,flag3,flag4

      save

c     generate interface element connectivity 1
      cnx = 0
      do i = 1,numel_cm
         if(ix_cm(9,i) .eq. 2) then
c        % + x-direc.
            ixx(1) = ix_cm(2,i)
            ixx(2) = ix_cm(3,i)
            ixx(3) = ix_cm(7,i)
            ixx(4) = ix_cm(6,i)
c        % - x-direc.
            ixx(5) = ix_cm(1,i)
            ixx(6) = ix_cm(4,i)
            ixx(7) = ix_cm(8,i)
            ixx(8) = ix_cm(5,i)
        
c        % + y-direc.
            iyy(1) = ix_cm(4,i)
            iyy(2) = ix_cm(8,i)
            iyy(3) = ix_cm(7,i)
            iyy(4) = ix_cm(3,i)
c        % - y-direc.
            iyy(5) = ix_cm(1,i)
            iyy(6) = ix_cm(5,i)
            iyy(7) = ix_cm(6,i)
            iyy(8) = ix_cm(2,i)

c        % + z-direc.
            izz(1) = ix_cm(5,i)
            izz(2) = ix_cm(6,i)
            izz(3) = ix_cm(7,i)
            izz(4) = ix_cm(8,i)
c        % - z-direc.
            izz(5) = ix_cm(1,i)
            izz(6) = ix_cm(2,i)
            izz(7) = ix_cm(3,i)
            izz(8) = ix_cm(4,i)

            do j = 1,numel_fs
               if(ix_fs(9,j) .eq. 1) then
c                % x-direc.
                  jxx(1) = ix_fs(1,j)
                  jxx(2) = ix_fs(4,j)
                  jxx(3) = ix_fs(8,j)
                  jxx(4) = ix_fs(5,j)
                
                  jxx(5) = ix_fs(2,j)
                  jxx(6) = ix_fs(3,j)
                  jxx(7) = ix_fs(7,j)
                  jxx(8) = ix_fs(6,j)
                
c                % y-direc.
                  jyy(1) = ix_fs(1,j)
                  jyy(2) = ix_fs(5,j)
                  jyy(3) = ix_fs(6,j)
                  jyy(4) = ix_fs(2,j)
                  
                  jyy(5) = ix_fs(4,j)
                  jyy(6) = ix_fs(8,j)
                  jyy(7) = ix_fs(7,j)
                  jyy(8) = ix_fs(3,j)

c                % z-direc.
                  jzz(1) = ix_fs(1,j)
                  jzz(2) = ix_fs(2,j)
                  jzz(3) = ix_fs(3,j)
                  jzz(4) = ix_fs(4,j)
                  
                  jzz(5) = ix_fs(5,j)
                  jzz(6) = ix_fs(6,j)
                  jzz(7) = ix_fs(7,j)
                  jzz(8) = ix_fs(8,j)

c     x - direc.
                  do k = 1,8
                     flag(k) = .false.
                  end do
                  do k = 1,8
                     if( eqoperator(x_cm(1,ixx(k)),x_fs(1,jxx(k))) 
     &                    ) then
                        if( eqoperator(x_cm(2,ixx(k)),x_fs(2,jxx(k)))
     &                       ) then
                           if( eqoperator(x_cm(3,ixx(k)),x_fs(3,jxx(k)))
     &                          ) then
                              flag(k) = .true.
                           endif
                        endif
                     endif
                  end do

                  if( 
     &                 flag(1) .and. flag(2) .and.
     &                 flag(3) .and. flag(4) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i ! % element number
                     intfcix(2,cnx) = 1 !% 1:+x -direc. & -1:-x-direc.
                     intfcix(3,cnx) = j ! %element number
                     intfcix(4,cnx) = -1 !% 1:+x -direc. & -1:-x-direc.
                  endif
                  if(
     &                 flag(5) .and. flag(6) .and.
     &                 flag(7) .and. flag(8) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i 
                     intfcix(2,cnx) = -1 !% 1:+x -direc. & -1:-x-direc.
                     intfcix(3,cnx) = j 
                     intfcix(4,cnx) = 1 !% 1:+x -direc. & -1:-x-direc.
                  endif

c     y - direc.
                  do k = 1,8
                     flag(k) = .false.
                  end do
                  do k = 1,8
                     if( eqoperator(x_cm(1,iyy(k)),x_fs(1,jyy(k))) 
     &                    ) then
                        if( eqoperator(x_cm(2,iyy(k)),x_fs(2,jyy(k)))
     &                       ) then
                           if( eqoperator(x_cm(3,iyy(k)),x_fs(3,jyy(k)))
     &                          ) then
                              flag(k) = .true.
                           endif
                        endif
                     endif
                  end do

                  if(
     &                 flag(1) .and. flag(2) .and.
     &                 flag(3) .and. flag(4) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i 
                     intfcix(2,cnx) = 2 !% 2:+y -direc. & -2:-y-direc.
                     intfcix(3,cnx) = j 
                     intfcix(4,cnx) = -2 !% 2:+y -direc. & -2:-y-direc.
                  endif
                  if(
     &                 flag(5) .and. flag(6) .and.
     &                 flag(7) .and. flag(8) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i 
                     intfcix(2,cnx) = -2 !% 2:+y -direc. & -2:-y-direc.
                     intfcix(3,cnx) = j 
                     intfcix(4,cnx) = 2 !% 2:+y -direc. & -2:-y-direc.
                  endif

c     z - direc.
                  do k = 1,8
                     flag(k) = .false.
                  end do
                  do k = 1,8
                     if( eqoperator(x_cm(1,izz(k)),x_fs(1,jzz(k))) 
     &                    ) then
                        if( eqoperator(x_cm(2,izz(k)),x_fs(2,jzz(k)))
     &                       ) then
                           if( eqoperator(x_cm(3,izz(k)),x_fs(3,jzz(k)))
     &                          ) then
                              flag(k) = .true.
                           endif
                        endif
                     endif
                  end do

                  if(
     &                 flag(1) .and. flag(2) .and.
     &                 flag(3) .and. flag(4) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i 
                     intfcix(2,cnx) = 3 !% 3:+z -direc. & -3:-z-direc.
                     intfcix(3,cnx) = j 
                     intfcix(4,cnx) = -3 !% 3:+z -direc. & -3:-z-direc.
                  endif
                  if(
     &                 flag(5) .and. flag(6) .and.
     &                 flag(7) .and. flag(8) ) then
                     cnx = cnx + 1
                     intfcix(1,cnx) = i 
                     intfcix(2,cnx) = -3 !% 3:+z -direc. & -3:-z-direc.
                     intfcix(3,cnx) = j 
                     intfcix(4,cnx) = 3 !% 3:+z -direc. & -3:-z-direc.
                  endif

               endif
            end do
         endif
      end do

      nintfcix = cnx

c      write(*,*) 'nintfcix =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(intfcix(j,i),j=1,4)
c      end do
      

c     ix_nbsurf_cm $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      do i = 1,nintfcix
         elemn = intfcix(1,i)
         dirc = intfcix(2,i)
         if(dirc .eq. 1) then
            ix_nbsurf_cm(1,i) = ix_cm(2,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(3,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(7,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(6,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         elseif(dirc .eq. -1) then
            ix_nbsurf_cm(1,i) = ix_cm(1,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(4,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(8,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(5,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         elseif(dirc .eq. 2) then
            ix_nbsurf_cm(1,i) = ix_cm(4,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(8,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(7,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(3,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         elseif(dirc .eq. -2) then
            ix_nbsurf_cm(1,i) = ix_cm(1,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(5,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(6,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(2,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         elseif(dirc .eq. 3) then
            ix_nbsurf_cm(1,i) = ix_cm(5,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(6,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(7,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(8,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         elseif(dirc .eq. -3) then
            ix_nbsurf_cm(1,i) = ix_cm(1,elemn)
            ix_nbsurf_cm(2,i) = ix_cm(2,elemn)
            ix_nbsurf_cm(3,i) = ix_cm(3,elemn)
            ix_nbsurf_cm(4,i) = ix_cm(4,elemn)
            ix_nbsurf_cm(5,i) = 2
            ix_nbsurf_cm(6,i) = dirc
         endif
      end do
c      ix_nbsurf_cm
c      write(*,*) 'nintfcix =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(ix_nbsurf_cm(j,i),j=1,6)
c      end do


c     ix_nbsurf_fs $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      do i = 1,nintfcix
         elemn = intfcix(3,i)
         dirc = intfcix(4,i)
         if(dirc .eq. 1) then
            ix_nbsurf_fs(1,i) = ix_fs(2,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(3,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(7,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(6,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         elseif(dirc .eq. -1) then
            ix_nbsurf_fs(1,i) = ix_fs(1,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(4,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(8,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(5,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         elseif(dirc .eq. 2) then
            ix_nbsurf_fs(1,i) = ix_fs(4,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(8,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(7,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(3,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         elseif(dirc .eq. -2) then
            ix_nbsurf_fs(1,i) = ix_fs(1,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(5,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(6,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(2,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         elseif(dirc .eq. 3) then
            ix_nbsurf_fs(1,i) = ix_fs(5,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(6,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(7,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(8,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         elseif(dirc .eq. -3) then
            ix_nbsurf_fs(1,i) = ix_fs(1,elemn)
            ix_nbsurf_fs(2,i) = ix_fs(2,elemn)
            ix_nbsurf_fs(3,i) = ix_fs(3,elemn)
            ix_nbsurf_fs(4,i) = ix_fs(4,elemn)
            ix_nbsurf_fs(5,i) = 1
            ix_nbsurf_fs(6,i) = dirc
         endif
      end do
c      ix_nbsurf_fs      
c      write(*,*) 'nintfcix =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(ix_nbsurf_fs(j,i),j=1,6)
c      end do






      end
