c$Id:$
      subroutine getdt( dto )

c     set 'startflag as .true.
c     get dt from feap
c     set timestep 't' equal to  1

      implicit  none

      include   'time_data.h'
      include   'time_flag.h'

      real*8    dto ! dt from feap

      save

      startflag = .true.

      dt = dto ! real

      ts = 1 ! integer

      end
