c$Id:$
      subroutine shp3d_20( x20,ss,output )

      implicit  none
      
      integer   i,j
      
      real*8    shp(20)
      real*8    x20(3,20),ss(3),output(3)
      real*8    ap1,am1,ap2,am2,ap3,am3,temp

      save

c     mapping of free space mesh
c     input---
c     x(20) : nodal value vector
c     shp(20) : 20 node shape function
c     output---
c     output(3) : interpolated vector
      
c     20 nodes shape func.
      ap1 = 1.0d0 + ss(1)
      am1 = 1.0d0 - ss(1)
      ap2 = 1.0d0 + ss(2)
      am2 = 1.0d0 - ss(2)
      ap3 = 1.0d0 + ss(3)
      am3 = 1.0d0 - ss(3)

      shp(1) = - 0.125d0*am1*am2*am3*(2.0d0 + ss(1) + ss(2) + ss(3))
      shp(2) = - 0.125d0*ap1*am2*am3*(2.0d0 - ss(1) + ss(2) + ss(3))
      shp(3) = - 0.125d0*ap1*ap2*am3*(2.0d0 - ss(1) - ss(2) + ss(3))
      shp(4) = - 0.125d0*am1*ap2*am3*(2.0d0 + ss(1) - ss(2) + ss(3))
      
      shp(5) = - 0.125d0*am1*am2*ap3*(2.0d0 + ss(1) + ss(2) - ss(3))
      shp(6) = - 0.125d0*ap1*am2*ap3*(2.0d0 - ss(1) + ss(2) - ss(3))
      shp(7) = - 0.125d0*ap1*ap2*ap3*(2.0d0 - ss(1) - ss(2) - ss(3))
      shp(8) = - 0.125d0*am1*ap2*ap3*(2.0d0 + ss(1) - ss(2) - ss(3))
      
      shp(9) = 0.25d0*am1*ap1*am2*am3
      shp(10) = 0.25d0*ap1*am2*ap2*am3
      shp(11) = 0.25d0*am1*ap1*ap2*am3
      shp(12) = 0.25d0*am1*am2*ap2*am3
      
      shp(13) = 0.25d0*am1*ap1*am2*ap3
      shp(14) = 0.25d0*ap1*am2*ap2*ap3
      shp(15) = 0.25d0*am1*ap1*ap2*ap3
      shp(16) = 0.25d0*am1*am2*ap2*ap3
      
      shp(17) = 0.25d0*am1*am2*am3*ap3
      shp(18) = 0.25d0*ap1*am2*am3*ap3
      shp(19) = 0.25d0*ap1*ap2*am3*ap3
      shp(20) = 0.25d0*am1*ap2*am3*ap3

c     interpolation

      do i = 1,3
         temp = 0.0d0
         do j = 1,20
            temp = temp + shp(j)*x20(i,j)
         end do
         output(i) = temp
      end do

      end
