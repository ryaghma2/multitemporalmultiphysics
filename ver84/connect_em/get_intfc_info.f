c$Id:$
      subroutine get_intfc_info (
     &     intfcixo,
     &     ix_nbsurf_cmo,ix_nbsurf_fso,
     &     samelocanodeso,
     &     intfcix,
     &     ix_nbsurf_cm,ix_nbsurf_fs,
     &     samelocanodes
     &     )

      implicit  none

      include   'intfc_const_data.h'! nintfcix

      integer   i,j,k

      integer   intfcixo(4,10000)
      integer   ix_nbsurf_cmo(6,10000),ix_nbsurf_fso(6,10000)

      integer   intfcix(4,nintfcix)
      integer   ix_nbsurf_cm(6,nintfcix),ix_nbsurf_fs(6,nintfcix)

      integer   samelocanodeso(2,10000),samelocanodes(2,nsamelocanodes)
      
      save

      do i = 1,nintfcix
         do j = 1,4
            intfcix(j,i) = intfcixo(j,i)
         end do
      end do
      
      do i = 1,nintfcix
         do j = 1,6
            ix_nbsurf_cm(j,i) = ix_nbsurf_cmo(j,i)
            ix_nbsurf_fs(j,i) = ix_nbsurf_fso(j,i)
         end do
      end do

      do i = 1,nsamelocanodes
         do j = 1,2
            samelocanodes(j,i) = samelocanodeso(j,i)
         end do
      end do

      
c      write(*,*) 'intfcix =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(intfcix(j,i),j=1,4)
c      end do

c      write(*,*) 'ix_nbsurf_cm =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(ix_nbsurf_cm(j,i),j=1,6)
c      end do

c      write(*,*) 'ix_nbsurf_fs =',nintfcix
c      do i = 1,nintfcix
c         write(*,*) i,(ix_nbsurf_fs(j,i),j=1,6)
c      end do

c      write(*,*) 'nsamelocanodes =',nsamelocanodes
c      do i = 1,nsamelocanodes
c         write(*,*) i,(samelocanodes(j,i),j=1,2)
c      end do




      end
