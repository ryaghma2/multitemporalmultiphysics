c$Id:$
      subroutine get_x_ix_fs_1( x,x_em,
     &     ix,ix_em )

      implicit  none

c      include   'cdata.h' ! numnp,numel
c      include   'sdata.h' ! nen1
      
      include   'fs_const_data.h'

      integer   i,j,k

      integer   ix(9,100000),ix_em(9,numel_fs)

      real*8    x(3,100000),x_em(3,numnp_fs)

      save

      do i = 1,numnp_fs
         do j = 1,3
            x_em(j,i) = x(j,i)
         end do
      end do

      do i = 1,numel_fs
         do j = 1,8
            ix_em(j,i) = ix(j,i)
         end do
         ix_em(9,i) = 1 ! index for free space
      end do

c      write(*,*) 'numnp_fs',numnp_fs
c      do i = 1,numnp_fs
c         write(*,*) i,(x_em(j,i),j=1,3)
c      end do

c      write(*,*) 'numel_fs',numel_fs
c      do i = 1,numel_fs
c         write(*,*)(ix_em(j,i),j=1,9)
c      end do
      

      end

