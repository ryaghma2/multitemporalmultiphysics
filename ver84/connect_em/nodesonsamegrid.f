c$Id:$
      subroutine nodesonsamegrid (
     &     x_cm,
     &     x_fs,
     &     samelocanodes
     & )

c     generate interface connectivity

      implicit  none

      include   'cm_const_data.h'
      include   'fs_const_data.h'

      include   'intfc_const_data.h'! nintfcix

      integer   cnx,i,j,k

c      integer   nsamelocanodes ! intfc_const_data.h
      integer   samelocanodes(2,10000)

      real*8    x_cm(3,numnp_cm),x_fs(3,numnp_fs)

      logical   eqoperator

      logical   flag

      save

c     get info of nodes on same location
      cnx = 0
      do i = 1,numnp_cm
         do j = 1,numnp_fs
            
            flag = .false.
            
            if( eqoperator(x_cm(1,i),x_fs(1,j)) ) then
               if( eqoperator(x_cm(2,i),x_fs(2,j)) ) then
                  if( eqoperator(x_cm(3,i),x_fs(3,j)) ) then
                     flag = .true.
                  endif
               endif
            endif
            
            if( flag ) then
               cnx = cnx + 1
               samelocanodes(1,cnx) = i ! % node_cm
               samelocanodes(2,cnx) = j ! % node_fs
            endif
         end do
      end do
      
      nsamelocanodes = cnx
      
c      write(*,*) 'nsamelocanodes =',nsamelocanodes
c      do i = 1,nsamelocanodes
c         write(*,*) i,(samelocanodes(j,i),j=1,2)
c      end do
      


      end
