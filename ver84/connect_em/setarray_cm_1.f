c$Id:$
      subroutine setarray_cm_1 ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

      include   'cm_const_data.h' ! numnp_cm, etc...

      logical   setvar,ualloc

      integer   ndm

      save

      ndm = 3

      setvar = ualloc( 18,'VAR17',2*nsbc_pi_cm,1) ! sbc_pi_cm
      setvar = ualloc( 19,'VAR18',7*nsbc_a_cm,1) ! sbc_a_cm

      setvar = ualloc( 20,'VAR19',6*nnbsurf_cm_j,1) ! ix_nbsurf_cm_j


      end
