c$Id:$
      subroutine setarray_cm ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

      include   'cdata.h'      ! numnp,numel,nummat,nen

      include   'cm_const_data.h' ! numnp_cm, etc...

      logical   setvar,ualloc

      integer   ndm

      save

      ndm = 3

      numnp_cm = numnp
      numel_cm = numel      

      setvar = ualloc(  1,'VAR00',9*(numel_cm),1) ! ix_cm
      setvar = ualloc(  2,'VAR01',ndm*(numnp_cm),2) ! x_cm

      setvar = ualloc(  3,'VAR02',ndm*(numnp_cm),2) ! a_n_1_cm
      setvar = ualloc(  4,'VAR03',ndm*(numnp_cm),2) ! a_n_cm
      setvar = ualloc(  5,'VAR04',ndm*(numnp_cm),2) ! d_a_n_cm
      setvar = ualloc(  6,'VAR05',ndm*(numnp_cm),2) ! dd_a_n_cm

      setvar = ualloc(  7,'VAR06',numnp_cm,2) ! phi_n_cm
      setvar = ualloc(  8,'VAR07',numnp_cm,2) ! d_phi_n_cm

      setvar = ualloc(  9,'VAR08',ndm*(numnp_cm),2) ! hfield_cm
      setvar = ualloc( 10,'VAR09',ndm*(numnp_cm),2) ! dfield_cm
      setvar = ualloc( 11,'VAR10',ndm*(numnp_cm),2) ! bfield_cm
      setvar = ualloc( 12,'VAR11',ndm*(numnp_cm),2) ! efield_cm

      setvar = ualloc( 13,'VAR12',ndm*(numnp_cm),2) ! mechvel_cm
      setvar = ualloc( 14,'VAR13',ndm*(numnp_cm),2) ! mechacc_cm
      setvar = ualloc( 15,'VAR14',ndm*(numnp_cm),2) ! mechdisp_cm
      setvar = ualloc( 16,'VAR15',ndm*(numnp_cm),2) ! mechdisp_1_cm

      setvar = ualloc( 45,'VAR44',ndm*(numnp_cm),2) ! mechdisp_2_cm

      end
