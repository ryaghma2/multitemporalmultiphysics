c$Id:$
      subroutine get_x_ix_fs(
     &     x,numnp,
     &     xfs,ixfs,numnpfs,numelfs,
     &     length,layers
     &     )

      implicit  none

      include   'lmn_data_cm.h' ! nx,ny,nz

      integer   numnp

      integer   numnpfs,numelfs,ndm

      integer   layers

      integer   i,j,k,p,ll,mm,nn,lnodes,tempnum,node,node1,ndn
      integer   el

      integer   mzd(4),pzd(4),mxdn(4),mxd(4),pxdn(4),pxd(4)
      integer   pydn(4),pyd(4)

      integer   ixfs(9,100000)

      real*8    x(3,numnp)
      
      real*8    xfs(3,100000)

      real*8    length(3),x20(3,20),diff(3),cnode(3),scalconst(3)
      real*8    ss(3),temp,coord(3),coeff,wfunc

      logical   eqoperator

      save

      ndm = 3

      ll = nx
      mm = ny
      nn = nz

c     number of layers for free space meshs
      layers = layers + 1

      lnodes = (ll+1)*(mm+1)*(nn+1)-(ll-1)*mm*(nn-1)

c     number of nodes for free space meshs
      numnpfs = layers*lnodes

c     set the free space dimension
c      length(1) = xlength
c      length(2) = ylength
c      length(3) = zlength

c     x20 = zeros(3,20);
c     diff = zeros(1,3);
c     cnode = zeros(1,3);
c     scalconst = zeros(1,3);
c     xfs = zeros(3,numnpfs);
  
c    coord for conducting matl. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c    for i = 1:numnp
c      for j = 1:3
c         xfs(j,i) = x(j,i);
c      end
c   end

c     generate coordinate for free space mesh
c     get center coord.
      do i = 1,3
         cnode(i) = 0.0d0
      end do

      tempnum = 0
      do i = 1,numnp
         if( eqoperator( x(2,i), 0.0d0 ) ) then
            do j = 1,3
               cnode(j) = cnode(j) + x(j,i)
            end do
            tempnum = tempnum + 1
         endif
      end do

      do i = 1,3
         cnode(i) = cnode(i)/tempnum
      end do

      do i = 1,3
         scalconst(i) = length(i)/(x(i,numnp) - cnode(i))
      end do

c     w/ 20 node shape func.
c     - z dirc.
      mzd(1) = 1
      mzd(2) = ll+1
      mzd(3) = (ll+1)*(mm+1)
      mzd(4) = (ll+1)*(mm+1)-ll
      do i = 1,3
         do j = 1,4
            node = mzd(j)
            diff(i) = x(i,node) - cnode(i)
            x20(i,j) = scalconst(i)*diff(i) + cnode(i)
         end do

         x20(i,5) = x(i,1)
         x20(i,6) = x(i,ll+1)
         x20(i,7) = x(i,(ll+1)*(mm+1))
         x20(i,8) = x(i,(ll+1)*(mm+1)-ll)
         
         x20(i,9) = (x20(i,1) + x20(i,2))/2.0d0
         x20(i,10) = (x20(i,2) + x20(i,3))/2.0d0
         x20(i,11) = (x20(i,3) + x20(i,4))/2.0d0
         x20(i,12) = (x20(i,4) + x20(i,1))/2.0d0
         
         x20(i,13) = (x20(i,5) + x20(i,6))/2.0d0
         x20(i,14) = (x20(i,6) + x20(i,7))/2.0d0
         x20(i,15) = (x20(i,7) + x20(i,8))/2.0d0
         x20(i,16) = (x20(i,5) + x20(i,8))/2.0d0
         
         x20(i,17) = (x20(i,1) + x20(i,5))/2.0d0
         x20(i,18) = (x20(i,2) + x20(i,6))/2.0d0
         x20(i,19) = (x20(i,3) + x20(i,7))/2.0d0
         x20(i,20) = (x20(i,4) + x20(i,8))/2.0d0
      end do

      do i = 1,layers
         do j = 1,(mm+1)
            do k = 1,(ll+1)
               node = (i-1)*lnodes + (j-1)*(ll+1) + k
               if(i .ne. layers) then
                  ss(1) = -1.0d0 + 2.0d0/ll*(k-1)
                  ss(2) = -1.0d0 + 2.0d0/mm*(j-1)
                  temp = 0.0d0
                  do p = 2,i
                     temp = temp + 2.0d0/(2.0d0**(p-1))
                  end do
                  ss(3) = -1.0d0 + temp
                  call shp3d_20( x20,ss,coord )
                  do p = 1,3
                     xfs(p,node) = coord(p)
                  end do 
               else
                  node1 = (j-1)*(ll+1) + k
                  do p = 1,3
                     xfs(p,node) = x(p,node1)
                  end do
               endif
            end do
         end do
      end do

c     + z dirc.
      pzd(1) = (ll+1)*(mm+1)*nn+1
      pzd(2) = (ll+1)*(mm+1)*nn+ll+1
      pzd(3) = (ll+1)*(mm+1)*(nn+1)
      pzd(4) = (ll+1)*(mm+1)*(nn+1)-ll

      do i = 1,3
         x20(i,1) = x(i,pzd(1))
         x20(i,2) = x(i,pzd(2))
         x20(i,3) = x(i,pzd(3))
         x20(i,4) = x(i,pzd(4))

         do j = 1,4
            node = pzd(j)
            diff(i) = x(i,node) - cnode(i)
            x20(i,j+4) = scalconst(i)*diff(i) + cnode(i)
         end do

         x20(i,9) = (x20(i,1) + x20(i,2))/2.0d0
         x20(i,10) = (x20(i,2) + x20(i,3))/2.0d0
         x20(i,11) = (x20(i,3) + x20(i,4))/2.0d0
         x20(i,12) = (x20(i,4) + x20(i,1))/2.0d0
         
         x20(i,13) = (x20(i,5) + x20(i,6))/2.0d0
         x20(i,14) = (x20(i,6) + x20(i,7))/2.0d0
         x20(i,15) = (x20(i,7) + x20(i,8))/2.0d0
         x20(i,16) = (x20(i,5) + x20(i,8))/2.0d0
         
         x20(i,17) = (x20(i,1) + x20(i,5))/2.0d0
         x20(i,18) = (x20(i,2) + x20(i,6))/2.0d0
         x20(i,19) = (x20(i,3) + x20(i,7))/2.0d0
         x20(i,20) = (x20(i,4) + x20(i,8))/2.0d0
      end do

      do i = 1,layers
         do j = 1,(mm+1)
            do k = 1,(ll+1)
               node = (i-1)*lnodes + ((ll+1)*(mm+1)*nn-(ll-1)*mm*(nn-1))
     &              + (j-1)*(ll+1) + k
               if(i .ne. layers) then 
                  ss(1) = -1.0d0 + 2.0d0/ll*(k-1)
                  ss(2) = -1.0d0 + 2.0d0/mm*(j-1)
                  temp = 0.0d0
                  do p = 2,i
                     temp = temp + 2.0d0/(2.0d0**(p-1))
                  end do
                  ss(3) = 1.0d0 - temp
                  call shp3d_20( x20,ss,coord )
                  do p = 1,3
                     xfs(p,node) = coord(p)
                  end do
               else
                  node1 = (ll+1)*(mm+1)*nn + (j-1)*(ll+1) + k
                  do p = 1,3
                     xfs(p,node) = x(p,node1)
                  end do
               endif
            enddo
         end do
      end do

c     - x dirc.
      mxdn(1) = 1
      mxdn(2) = 4
      mxdn(3) = 5
      mxdn(4) = 8
      mxd(1) = 1
      mxd(2) = (ll+1)*mm+1
      mxd(3) = (ll+1)*(mm+1)*nn+1
      mxd(4) = (ll+1)*(mm+1)*(nn+1)-ll

      do i = 1,3
         do j = 1,4
            ndn = mxdn(j)
            node = mxd(j)
            diff(i) = x(i,node) - cnode(i)
            x20(i,ndn) = scalconst(i)*diff(i) + cnode(i)
         end do

         x20(i,2) = x(i,mxd(1))
         x20(i,3) = x(i,mxd(2))
         x20(i,6) = x(i,mxd(3))
         x20(i,7) = x(i,mxd(4))
         
         x20(i,9) = (x20(i,1) + x20(i,2))/2.0d0
         x20(i,10) = (x20(i,2) + x20(i,3))/2.0d0
         x20(i,11) = (x20(i,3) + x20(i,4))/2.0d0
         x20(i,12) = (x20(i,4) + x20(i,1))/2.0d0
         
         x20(i,13) = (x20(i,5) + x20(i,6))/2.0d0
         x20(i,14) = (x20(i,6) + x20(i,7))/2.0d0
         x20(i,15) = (x20(i,7) + x20(i,8))/2.0d0
         x20(i,16) = (x20(i,5) + x20(i,8))/2.0d0
         
         x20(i,17) = (x20(i,1) + x20(i,5))/2.0d0
         x20(i,18) = (x20(i,2) + x20(i,6))/2.0d0
         x20(i,19) = (x20(i,3) + x20(i,7))/2.0d0
         x20(i,20) = (x20(i,4) + x20(i,8))/2.0d0
      end do

      do i = 1,layers
         do j = 1,(nn-1)
            do k = 1,(mm+1)
               node = (i-1)*lnodes + (ll+1)*(mm+1)
     &              + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + 1 + (k-1)*2
               if(i .ne. layers) then
                  ss(2) = -1.0d0 + 2.0d0/mm*(k-1)
                  ss(3) = -1.0d0 + 2.0d0/nn*j
                  temp = 0.0d0
                  do p = 2,i
                     temp = temp + 2.0d0/(2.0d0**(p-1))
                  end do
                  ss(1) = -1.0d0 + temp
                  call shp3d_20( x20,ss,coord )
                  do p = 1,3
                     xfs(p,node) = coord(p)
                  end do
               else
                  node1 = 1 + (k-1)*(ll+1) + j*(ll+1)*(mm+1)
                  do p = 1,3
                     xfs(p,node) = x(p,node1)
                  end do
               endif
            end do
         end do
      end do

c     + x dirc.
      pxdn(1) = 2
      pxdn(2) = 3
      pxdn(3) = 6
      pxdn(4) = 7
      pxd(1) = (ll+1)
      pxd(2) = (ll+1)*(mm+1)
      pxd(3) = (ll+1)*(mm+1)*nn+(ll+1)
      pxd(4) = (ll+1)*(mm+1)*(nn+1)

      do i = 1,3
         do j = 1,4
            ndn = pxdn(j)
            node = pxd(j)
            diff(i) = x(i,node) - cnode(i)
            x20(i,ndn) = scalconst(i)*diff(i) + cnode(i)
         end do

         x20(i,1) = x(i,pxd(1))
         x20(i,4) = x(i,pxd(2))
         x20(i,5) = x(i,pxd(3))
         x20(i,8) = x(i,pxd(4))
         
         x20(i,9) = (x20(i,1) + x20(i,2))/2.0d0
         x20(i,10) = (x20(i,2) + x20(i,3))/2.0d0
         x20(i,11) = (x20(i,3) + x20(i,4))/2.0d0
         x20(i,12) = (x20(i,4) + x20(i,1))/2.0d0
         
         x20(i,13) = (x20(i,5) + x20(i,6))/2.0d0
         x20(i,14) = (x20(i,6) + x20(i,7))/2.0d0
         x20(i,15) = (x20(i,7) + x20(i,8))/2.0d0
         x20(i,16) = (x20(i,5) + x20(i,8))/2.0d0
         
         x20(i,17) = (x20(i,1) + x20(i,5))/2.0d0
         x20(i,18) = (x20(i,2) + x20(i,6))/2.0d0
         x20(i,19) = (x20(i,3) + x20(i,7))/2.0d0
         x20(i,20) = (x20(i,4) + x20(i,8))/2.0d0
      end do

      do i = 1,layers
         do j = 1,(nn-1)
            do k = 1,(mm+1)
               node = (i-1)*lnodes + (ll+1)*(mm+1)
     &              + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + 2 + (k-1)*2
               if(k .eq. (mm+1)) then
                  node = node + ll - 1
               endif
               if( i .ne. layers) then
                  ss(2) = -1.0d0 + 2.0d0/mm*(k-1)
                  ss(3) = -1.0d0 + 2.0d0/nn*j
                  temp = 0.0d0
                  do p = 2,i
                     temp = temp + 2.0d0/(2.0d0**(p-1))
                  end do
                  ss(1) = 1.0d0 - temp
                  call shp3d_20( x20,ss,coord )
                  do p = 1,3
                     xfs(p,node) = coord(p)
                  end do
               else
                  node1 = k*(ll+1) + j*(ll+1)*(mm+1)
                  do p = 1,3
                     xfs(p,node) = x(p,node1)
                  end do
               endif
            end do
         end do
      end do

c     + y dirc.
      pydn(1) = 3
      pydn(2) = 4
      pydn(3) = 7
      pydn(4) = 8
      pyd(1) = (ll+1)*(mm+1)
      pyd(2) = (ll+1)*(mm+1)-ll
      pyd(3) = (ll+1)*(mm+1)*(nn+1)
      pyd(4) = (ll+1)*(mm+1)*(nn+1)-ll
      do i = 1,3
         do j = 1,4
            ndn = pydn(j)
            node = pyd(j)
            diff(i) = x(i,node) - cnode(i)
            x20(i,ndn) = scalconst(i)*diff(i) + cnode(i)
         end do

         x20(i,2) = x(i,pyd(1))
         x20(i,1) = x(i,pyd(2))
         x20(i,6) = x(i,pyd(3))
         x20(i,5) = x(i,pyd(4)) 
         
         x20(i,9) = (x20(i,1) + x20(i,2))/2.0d0
         x20(i,10) = (x20(i,2) + x20(i,3))/2.0d0
         x20(i,11) = (x20(i,3) + x20(i,4))/2.0d0
         x20(i,12) = (x20(i,4) + x20(i,1))/2.0d0
         
         x20(i,13) = (x20(i,5) + x20(i,6))/2.0d0
         x20(i,14) = (x20(i,6) + x20(i,7))/2.0d0
         x20(i,15) = (x20(i,7) + x20(i,8))/2.0d0
         x20(i,16) = (x20(i,5) + x20(i,8))/2.0d0
         
         x20(i,17) = (x20(i,1) + x20(i,5))/2.0d0
         x20(i,18) = (x20(i,2) + x20(i,6))/2.0d0
         x20(i,19) = (x20(i,3) + x20(i,7))/2.0d0
         x20(i,20) = (x20(i,4) + x20(i,8))/2.0d0
      end do

      do i = 1,layers
         do j = 1,(nn-1)
            do k = 1,(ll-1)
               node = (i-1)*lnodes + (ll+1)*(mm+1)
     &              + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm)
     &              + (ll+1)*mm-(ll-1)*mm + 1 + k
               if( i  .ne. layers) then
                  ss(1) = -1.0d0 + 2.0d0/ll*k
                  ss(3) = -1.0d0 + 2.0d0/nn*j
                  temp = 0.0d0
                  do p = 2,i
                     temp = temp + 2.0d0/(2.0d0**(p-1))
                  end do
                  ss(2) = 1.0d0 - temp
c                 weighted averageing
                  coeff = 0.3d0
                  wfunc = (1.0d0 - ss(2)**2)**0.5d0
                  temp = ss(2) + 1.0d0
                  temp = temp*(1.0d0 + coeff*wfunc)
                  ss(2) = temp - 1.0d0
                  call shp3d_20( x20,ss,coord )
                  do p = 1,3
                     xfs(p,node) = coord(p)
                  end do
               else
                  node1 = j*(ll+1)*(mm+1) + (ll+1)*mm + k + 1
                  do p = 1,3
                     xfs(p,node) = x(p,node1)
                  end do
               endif
            end do
         end do
      end do

c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      layers = layers - 1
      numelfs = (layers)*(2*ll*mm + 2*mm*nn + ll*nn)

c      ixfs = zeros(9,numelfs)

c% %     copy element connectivity
c%       for i = 1:numel
c%          for j = 1:nel
c%             ixfs(j,i) = ix(j,i);
c%          end
c%          ixfs(nel+1,i) = ix(9,i);
c%       end
c%       for i = numel:(numel+numelfs)
c%          ixfs(nel+1,i) = nummat;
c%       end
      
c     generate connectivity for free space element
      el = 1
c     negative x-direc.
      do k = 1,layers
         do j = 1,nn
            do i = 1,mm
               if(j .eq. 1) then 
                  ixfs(1,el) = (k-1)*lnodes + 1 + (i-1)*(ll+1)
                  ixfs(4,el) = ixfs(1,el) + (ll+1)
               else
                  ixfs(1,el) = (k-1)*lnodes + (ll+1)*(mm+1) 
     &                 + (j-2)*((ll+1)*(mm+1)-(ll-1)*mm) + 1 + (i-1)*2
                  ixfs(4,el) = ixfs(1,el) + 2
               endif
            
c%               if(k == layers) 
c%                  ixfs(2,el) = 1 + (i-1)*(ll+1) + (j-1)*(ll+1)*(mm+1);
c%                  ixfs(3,el) = ixfs(2,el) + (ll+1);
c%               else
               ixfs(2,el) = ixfs(1,el) + lnodes
               ixfs(3,el) = ixfs(4,el) + lnodes
c%               end
            
               if(j .eq. nn) then 
                  ixfs(5,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + 1
     &                 + (i-1)*(ll+1)
                  ixfs(8,el) = ixfs(5,el) + (ll+1)
               else
                  ixfs(5,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + 1 + (i-1)*2
                  ixfs(8,el) = ixfs(5,el) + 2
               endif
            
c%               if(k == layers) 
c%                  ixfs(6,el) = ixfs(2,el) + (ll+1)*(mm+1);
c%                  ixfs(7,el) = ixfs(6,el) + (ll+1);
c%               else
               ixfs(6,el) = ixfs(5,el) + lnodes
               ixfs(7,el) = ixfs(8,el) + lnodes
c%               end

               el = el + 1
            end do
         end do
      end do

c     positive x-direc.
      do k = 1,layers
         do j = 1,nn
            do i = 1,mm
               if(j .eq. 1) then 
                  ixfs(2,el) = (k-1)*lnodes + i*(ll+1)
                  ixfs(3,el) = ixfs(2,el) + (ll+1)
               else
                  ixfs(2,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-2)*((ll+1)*(mm+1)-(ll-1)*mm) + i*2
                  if(i .eq. mm) then 
                     ixfs(3,el) = ixfs(2,el) + (ll+1)
                  else
                     ixfs(3,el) = ixfs(2,el) + 2
                  endif
               endif
            
c%               if(k == layers) 
c%                  ixfs(1,el) = i*(ll+1) + (j-1)*(ll+1)*(mm+1);
c%                  ixfs(4,el) = ixfs(1,el) + (ll+1);
c%               else
               ixfs(1,el) = ixfs(2,el) + lnodes
               ixfs(4,el) = ixfs(3,el) + lnodes
c%               end
            
               if(j .eq. nn) then 
                  ixfs(6,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + i*(ll+1)
                  ixfs(7,el) = ixfs(6,el) + (ll+1)
               else
                  ixfs(6,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + i*2
                  if(i .eq. mm) then 
                     ixfs(7,el) = ixfs(6,el) + (ll+1)
                  else
                     ixfs(7,el) = ixfs(6,el) + 2
                  endif
               endif
            
c%               if(k == layers) 
c%                  ixfs(5,el) = j*(ll+1)*(mm+1) + i*(ll+1);
c%                  ixfs(8,el) = ixfs(5,el) + (ll+1);
c%               else
               ixfs(5,el) = ixfs(6,el) + lnodes
               ixfs(8,el) = ixfs(7,el) + lnodes
c%               end

               el = el + 1
            end do
         end do
      end do

c     positive y-direc.
      do k = 1,layers
         do j = 1,nn
            do i = 1,ll
               if(j .eq. 1) then 
                  ixfs(4,el) = (k-1)*lnodes + (ll+1)*mm + i
               else
                  ixfs(4,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-2)*((ll+1)*(mm+1)-(ll-1)*mm) + 2*mm + i
               endif
               ixfs(3,el) = ixfs(4,el) + 1
            
               if(j .eq. nn) then 
                  ixfs(8,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + (ll+1)*mm + i
               else
                  ixfs(8,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &                 + (j-1)*((ll+1)*(mm+1)-(ll-1)*mm) + 2*mm + i
               endif
               ixfs(7,el) = ixfs(8,el) + 1
            
c%               if(k == layers) 
c%                  ixfs(1,el) = (j-1)*(ll+1)*(mm+1) + (ll+1)*mm + i;
c%                  ixfs(5,el) = ixfs(1,el) + (ll+1)*(mm+1);
c%               else
               ixfs(1,el) = ixfs(4,el) + lnodes
               ixfs(5,el) = ixfs(8,el) + lnodes
c%               end
               ixfs(2,el) = ixfs(1,el) + 1
               ixfs(6,el) = ixfs(5,el) + 1
            
               el = el + 1
            end do
         end do
      end do

c     negative z-direc.
      do k = 1,layers
         do j = 1,mm
            do i = 1,ll
               ixfs(1,el) = (k-1)*lnodes + i + (j-1)*(ll+1)
               ixfs(2,el) = ixfs(1,el) + 1
               ixfs(4,el) = ixfs(1,el) + (ll+1)
               ixfs(3,el) = ixfs(4,el) + 1
            
c%               if(k == layers) 
c%                  ixfs(5,el) = i + (j-1)*(ll+1);
c%               else
               ixfs(5,el) = ixfs(1,el) + lnodes
c%               end
               ixfs(6,el) = ixfs(5,el) + 1
               ixfs(8,el) = ixfs(5,el) + (ll+1)
               ixfs(7,el) = ixfs(8,el) + 1
            
               el = el + 1
            end do
         end do
      end do

c     positive z-direc.
      do k = 1,layers
         do  j = 1,mm
            do i = 1,ll
               ixfs(5,el) = (k-1)*lnodes + (ll+1)*(mm+1)
     &              + (nn-1)*((ll+1)*(mm+1)-(ll-1)*mm) + i
     &              + (j-1)*(ll+1)
               ixfs(6,el) = ixfs(5,el) + 1
               ixfs(8,el) = ixfs(5,el) + (ll+1)
               ixfs(7,el) = ixfs(8,el) + 1
            
c%               if(k == layers) 
c%                  ixfs(1,el) = (ll+1)*(mm+1)*nn + i + (j-1)*(ll+1);
c%               else
               ixfs(1,el) = ixfs(5,el) + lnodes
c%               end
               ixfs(2,el) = ixfs(1,el) + 1
               ixfs(4,el) = ixfs(1,el) + (ll+1)
               ixfs(3,el) = ixfs(4,el) + 1
            
               el = el + 1
            end do
         end do
      end do

c      write(*,*) numnpfs
c      do i = 1,numnpfs
c         write(*,*) i,(xfs(j,i),j=1,3)
c      end do
c     get max & min in x, y, z direc.

c      write(*,*) numelfs
c      do i = 1,numelfs
c         write(*,*)(ixfs(j,i),j=1,9)
c      end do

      end
