c$Id:$
      subroutine get_sbc_pi_a_1 (
     &     nsbc_pi,nsbc_a,nnbsurf,
     &     sbc_pi,sbc_a,ix_nbsurf,

     &     sbc_pi_cm,sbc_a_cm,ix_nbsurf_cm_j )

      implicit  none

      include   'cm_const_data.h'

      integer   i,j,k

      integer   nsbc_pi,nsbc_a,nnbsurf

      integer   sbc_pi(2,10000),sbc_a(7,10000)
      integer   ix_nbsurf(6,10000)

      integer   sbc_pi_cm(2,nsbc_pi_cm),sbc_a_cm(7,nsbc_a_cm)
      integer   ix_nbsurf_cm_j(6,nnbsurf_cm_j)   

      save

      do i = 1,nsbc_pi_cm
         do j = 1,2
            sbc_pi_cm(j,i) = sbc_pi(j,i)
         end do
      end do
      
      do i = 1,nsbc_a_cm
         do j = 1,7
            sbc_a_cm(j,i) = sbc_a(j,i)
         end do
      end do

      do i = 1,nnbsurf_cm_j
         do j = 1,6
            ix_nbsurf_cm_j(j,i) = ix_nbsurf(j,i)
         end do
      end do
      
c      write(*,*) nsbc_pi_cm
c      do i = 1,nsbc_pi_cm
c         write(*,*)(sbc_pi_cm(j,i),j=1,2)
c      end do

c      write(*,*) nsbc_a_cm
c      do i = 1,(nsbc_a_cm-200)
c         write(*,*)(sbc_a_cm(j,i),j=1,7)
c      end do

c      write(*,*) nnbsurf_cm_j
c      do i = 1,nnbsurf_cm_j
c         write(*,*)(ix_nbsurf_cm_j(j,i),j=1,6)
c      end do

      end
