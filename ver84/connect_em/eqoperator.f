c$Id:$
      logical function eqoperator ( a,b )

      implicit  none

      real*8    a,b

      real*8    tol,diff

      save

      tol = 1.0d0*10.0d0**(-6.0d0)

      diff = a - b

      if( (diff.le.tol) .and. (diff.ge.-tol) ) then
         eqoperator = .true.
      else
         eqoperator = .false.
      endif


      end
