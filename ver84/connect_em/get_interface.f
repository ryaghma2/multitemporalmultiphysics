c$Id:$
      subroutine get_interface ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

      integer   intfcix(4,10000),nintfcix
      integer   ix_nbsurf_cm(6,10000),ix_nbsurf_fs(6,10000)

      integer   nsamelocanodes,samelocanodes(2,10000)

      save

      call interface_em(
     &     mr(up(1)),hr(up(2)),
     &     mr(up(21)),hr(up(22)),
     &     intfcix,
     &     ix_nbsurf_cm,ix_nbsurf_fs
     &     )

      call nodesonsamegrid (
     &     hr(up(2)),
     &     hr(up(22)),
     &     samelocanodes )

      call setarray_intfc ( )

      call get_intfc_info (
     &     intfcix,
     &     ix_nbsurf_cm,ix_nbsurf_fs,
     &     samelocanodes,
     &     mr(up(41)),
     &     mr(up(42)),mr(up(43)),
     &     mr(up(44))
     &     )




      end
