c$Id:$
      subroutine connect_em ( )

      implicit  none

      include   'pointer.h'
      include   'comblk.h'

      include   'time_flag.h'

c      include   'cdata.h'      ! numnp,numel,nummat,nen

      logical   setvar,ualloc

      save
      
      if(startflag) then
         call cm_data ( )
         
         setvar = ualloc( 17,'VAR16',3*2,2) ! mat_prop

c        call fs_data ( )

c        call get_interface ( )

      endif

      call transferdata_cm ( 
     &     hr(np(40)),hr(np(42)),
     &     hr(up(15)),hr(up(16)),hr(up(45)),
     &     hr(up(13)),hr(up(14))
     &     )

c     call transferdata_fs (
c    &     hr(up(15)),hr(up(16)),
c    &     hr(up(35)),hr(up(36)),
c    &     mr(up(44))
c    &     )
      
      call connect_maxwell ( 
     &     hr(up(17))
     &     
     &     ) 
      

      end
