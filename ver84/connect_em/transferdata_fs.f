c$Id:$
      subroutine transferdata_fs ( 
     &     mechdisp_cm,mechdisp_1_cm,

     &     mechdisp_fs,mechdisp_1_fs,

     &     samelocanodes
     &     )

c     u_feap(ndm,numnp,1) : u_n+1
c     u_feap(ndm,numnp,2) : u_n+1 - u_n : increment

      implicit  none

      include   'cm_const_data.h'
      include   'fs_const_data.h'

      include   'intfc_const_data.h'

      integer   i,j,k,cmnode,fsnode

      integer   samelocanodes(2,nsamelocanodes)
      
      real*8    mechdisp_cm(3,numnp_cm),mechdisp_1_cm(3,numnp_cm)

      real*8    mechdisp_fs(3,numnp_fs),mechdisp_1_fs(3,numnp_fs)

      save

      do i = 1,nsamelocanodes
         cmnode = samelocanodes(1,i)
         fsnode = samelocanodes(2,i)
         do j = 1,3
            mechdisp_fs(j,fsnode) = mechdisp_cm(j,cmnode)
c            mechdisp_1_fs(j,fsnode) = mechdisp_1_cm(j,cmnode)
         end do
      end do


c      write(*,*) 'nsamelocanodes =',nsamelocanodes
c      do i = 1,nsamelocanodes
c         write(*,*) i,(samelocanodes(j,i),j=1,2)
c      end do

c      write(*,*) 'mechdisp_cm'
c      do i = 1,numnp_cm
c         write(*,*) (mechdisp_cm(j,i),j=1,3)
c      end do

c      write(*,*) 'mechdisp_fs'
c      do i = 1,numnp_fs
c         write(*,*) (mechdisp_fs(j,i),j=1,3)
c      end do



      end
