c$Id:$
      subroutine get_x_ix( x_feap,x_em,
     &     ix_feap,ix_em )

      implicit  none

      include   'cdata.h' ! numnp,numel
      include   'sdata.h' ! nen1
      
      include   'cm_const_data.h'

      integer   i,j,k

      integer   ix_feap(nen1,numel),ix_em(9,numel_cm)

      real*8    x_feap(3,numnp),x_em(3,numnp_cm)

      save

      do i = 1,numnp
         do j = 1,3
            x_em(j,i) = x_feap(j,i)
         end do
      end do

      do i = 1,numel
         do j = 1,8
            ix_em(j,i) = ix_feap(j,i)
         end do
         ix_em(9,i) = 2 ! index for conducting material
      end do

c      write(*,*) numnp_cm
c      do i = 1,numnp_cm
c         write(*,*) i,(x_em(j,i),j=1,3)
c      end do

c      write(*,*) numel
c      do i = 1,numel
c         write(*,*)(ix_em(j,i),j=1,9)
c      end do
      

      end

