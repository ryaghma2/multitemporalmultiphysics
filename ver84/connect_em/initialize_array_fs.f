c$Id:$
      subroutine initialize_array_fs (
     &     x_fs,
     &     a_n_1_fs,a_n_fs,
     &     d_a_n_fs,dd_a_n_fs,
     &     phi_n_fs,d_phi_n_fs,
     &     hfield_fs,dfield_fs,
     &     bfield_fs,efield_fs
     &     )

      implicit  none

      include   'fs_const_data.h'

      integer   i,j,k
      
      real*8    x_fs(3,numnp_fs)

      real*8    a_n_1_fs(3,numnp_fs),a_n_fs(3,numnp_fs)
      real*8    d_a_n_fs(3,numnp_fs),dd_a_n_fs(3,numnp_fs)

      real*8    phi_n_fs(numnp_fs),d_phi_n_fs(numnp_fs)

      real*8    hfield_fs(3,numnp_fs),dfield_fs(3,numnp_fs)
      real*8    bfield_fs(3,numnp_fs),efield_fs(3,numnp_fs)

      save

      do i = 1,numnp_fs
         do j = 1,3
            x_fs(j,i) = 0.0d0

            a_n_1_fs(j,i) = 0.0d0
            a_n_fs(j,i) = 0.0d0
            d_a_n_fs(j,i) = 0.0d0
            dd_a_n_fs(j,i) = 0.0d0

            hfield_fs(j,i) = 0.0d0
            dfield_fs(j,i) = 0.0d0
            bfield_fs(j,i) = 0.0d0
            efield_fs(j,i) = 0.0d0
         end do
         phi_n_fs(i) = 0.0d0
         d_phi_n_fs(i) = 0.0d0
      end do


      end
