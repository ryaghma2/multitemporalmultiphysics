c$Id:$
      subroutine savedata (
     &     numnp,numel,
     &     x,ix,
     &     mechdisp,

     &     hfield,dfield,
     &     bfield,efield,

     &     a_n,phi_n
     &     )

c     1,2,3,4,7,8 units are used to save data

      implicit  none

      integer   i,j,k

      integer   numnp,numel

      integer   ix(9,numel)

      real*8    x(3,numnp)

      real*8    mechdisp(3,numnp)

      real*8    hfield(3,numnp),dfield(3,numnp)
      real*8    bfield(3,numnp),efield(3,numnp)

      real*8    a_n(3,numnp),phi_n(numnp)

      save

c     save data cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
      open(unit=1,status='unknown')
      write(1,*)'VARIABLES = "X","Y","Z",
     &     "HFIELD_X","HFIELD_Y","HFIELD_Z"'
      write(1,*)'ZONE N=',numnp,', E=',numel,', F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(1,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(hfield(j,i),j=1,3)
      end do
      write(1,*)'NODAL CONN.'
      do i = 1,numel
         write(1,1001)(ix(j,i),j=1,8)
      end do
      
      open(unit=2,status='unknown')
      write(2,*)'VARIABLES = "X","Y","Z",
     &     "DFIELD_X","DFIELD_Y","DFIELD_Z"'
      write(2,*)'ZONE N=',numnp,', E=',numel,'
     &     , F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(2,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(dfield(j,i),j=1,3)
      end do
      write(2,*)'NODAL CONN.'
      do i = 1,numel
         write(2,1001)(ix(j,i),j=1,8)
      end do
      
      open(unit=3,status='unknown')
      write(3,*)'VARIABLES = "X","Y","Z",
     &     "BFIELD_X","BFIELD_Y","BFIELD_Z"'
      write(3,*)'ZONE N=',numnp,', E=',numel,'
     &     , F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(3,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(bfield(j,i),j=1,3)
      end do
      write(3,*)'NODAL CONN.'
      do i = 1,numel
         write(3,1001)(ix(j,i),j=1,8)
      end do
      
      open(unit=4,status='unknown')
      write(4,*)'VARIABLES = "X","Y","Z",
     &     "EFIELD_X","EFIELD_Y","EFIELD_Z"'
      write(4,*)'ZONE N=',numnp,', E=',numel,'
     &     , F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(4,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(efield(j,i),j=1,3)
      end do
      write(4,*)'NODAL CONN.'
      do i = 1,numel
         write(4,1001)(ix(j,i),j=1,8)
      end do
      
      open(unit=7,status='unknown')
      write(7,*)'VARIABLES = "X","Y","Z",
     &     "AVECTOR_X","AVECTOR_Y","AVECTOR_Z"'
      write(7,*)'ZONE N=',numnp,', E=',numel,'
     &     , F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(7,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(a_n(j,i),j=1,3)
      end do
      write(7,*)'NODAL CONN.'
      do i = 1,numel
         write(7,1001)(ix(j,i),j=1,8)
      end do
      
      open(unit=8,status='unknown')
      write(8,*)'VARIABLES = "X","Y","Z",
     &     "PHI"'
      write(8,*)'ZONE N=',numnp,', E=',numel,'
     &     , F=FEPOINT, ET=BRICK'
      do i = 1,numnp
         write(8,1000)((x(j,i)+mechdisp(j,i)),j=1,3)
     &        ,(phi_n(i))
      end do
      write(8,*)'NODAL CONN.'
      do i = 1,numel
         write(8,1001)(ix(j,i),j=1,8)
      end do
      
      
 1000 format(6F10.6)
 1001 format(8I4)
      
      
      
      end
      
