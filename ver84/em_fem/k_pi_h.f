c$Id:$
      subroutine k_pi_h(
     &     e_matrix,e_vector,
     &     shp,xsj,nel,ndm,numnp,numel,nlint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      implicit  none

      integer   ndm,nel,lint,nlint

      integer   numnp,numel

      integer   i,j,k
      integer   row,col

      real*8    dt,ep,mu,sigma

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)

      real*8    shp(4,nel,nlint),xsj(nlint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_matrix(3*nel,nel)
      real*8    e_vector(3*nel)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    xn,yn,zn,a11,a22,a33

      real*8    vel(3)

      save

c$    formulate element k_matrix
c     
      do lint = 1,nlint
         do i = 1,nel
            xn = shp(4,i,lint)*xsj(lint)
            yn = shp(4,i,lint)*xsj(lint)
            zn = shp(4,i,lint)*xsj(lint)

            a11 = detfi(lint)*ep*xn
            a22 = detfi(lint)*ep*yn
            a33 = detfi(lint)*ep*zn
            
            row = 3*i

            do j = 1,nel
               xn = shp(1,j,lint)
               yn = shp(2,j,lint)
               zn = shp(3,j,lint)

c               do k = 1,ndm
c                  vel(k) = shp(4,j,lint)*e_mechvel(k,j)
c               end do
               
               col = j
               
               e_matrix(row-2,col) = e_matrix(row-2,col)
     &              - ci(3,1,lint)*a11*zn
     &              - ci(2,1,lint)*a11*yn
     &              - ci(1,1,lint)*a11*xn

               e_matrix(row-1,col) = e_matrix(row-1,col)
     &              - ci(3,2,lint)*a22*zn
     &              - ci(2,2,lint)*a22*yn
     &              - ci(1,2,lint)*a22*xn

               e_matrix(row  ,col) = e_matrix(row  ,col)
     &              - ci(3,3,lint)*a33*zn
     &              - ci(2,3,lint)*a33*yn
     &              - ci(1,3,lint)*a33*xn

            end do
         end do
      end do

cc     sigma{-grad(pi)}
c      do lint = 1,nlint
c         do i = 1,nel
c            xn = shp(4,i,lint)*xsj(lint)
c            yn = shp(4,i,lint)*xsj(lint)
c            zn = shp(4,i,lint)*xsj(lint)
c
c            a11 = sigma*xn
c            a22 = sigma*yn
c            a33 = sigma*zn
c
c            row = 3*i
c
c            do j = 1,nel
c               xn = shp(1,j,lint)
c               yn = shp(2,j,lint)
c               zn = shp(3,j,lint)
c            
c               col = j
c
c               e_matrix(row-2,col) = e_matrix(row-2,col)
c     &              -
c               e_matrix(row-1,col) = e_matrix(row-1,col)
c     &              -
c               e_matrix(row  ,col) = e_matrix(row  ,col)
c     &              -
c            end do
c         end do
c      end do

      end
