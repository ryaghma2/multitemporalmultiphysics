c$Id:$
      subroutine cross_product(b,u1,u2,numnp)
c
c     b = u1 x u2
c     

      implicit  none

      integer   numnp,i

      real*8    b(3,numnp),u1(3,numnp),u2(3,numnp)

      save

      do i = 1,numnp
         b(1,i) = u1(2,i)*u2(3,i) - u1(3,i)*u2(2,i)
         b(2,i) = u1(3,i)*u2(1,i) - u1(1,i)*u2(3,i)
         b(3,i) = u1(1,i)*u2(2,i) - u1(2,i)*u2(1,i)
      end do

      end
      
