c$Id:$
      subroutine natural_bc_d(
     &     e_vector,n,
     &     ix,
     &     shp,xsj,
     &     nel,ndm,numel,nlint,
     &     ep,mu,sigma,
     &     f,fi,c,ci,detfi,
     &     e_flux_input)

      implicit  none

      integer   n,nel,ndm,numnp,numel,nlint,lint

      integer   direcidx,i,row

      integer   ix(6,numel)

      integer   nv(3)

      real*8    ep,mu,sigma,zn,a33
      
      real*8    shp(4,4,4),xsj(4)
      real*8    e_flux_input(3,4)

      real*8    e_vector(4)

      real*8    f(3,3,4),fi(3,3,4),c(3,3,4),ci(3,3,4)
      real*8    detfi(4)

      save

c$     form element vector for natural b/c's
c     ep/sigma*J
      direcidx = ix(6,n)

      if(direcidx .eq. 1) then
         nv(1) = 1
         nv(2) = 0
         nv(3) = 0
      elseif(direcidx .eq. -1) then
         nv(1) = -1
         nv(2) = 0
         nv(3) = 0
      elseif(direcidx .eq. 2) then
         nv(1) = 0
         nv(2) = 1
         nv(3) = 0
      elseif(direcidx .eq. -2) then
         nv(1) = 0
         nv(2) = -1
         nv(3) = 0
      elseif(direcidx .eq. 3) then
         nv(1) = 0
         nv(2) = 0
         nv(3) = 1
      elseif(direcidx .eq. -3) then
         nv(1) = 0
         nv(2) = 0
         nv(3) = -1
      endif
    
      do lint = 1,nlint
         do i = 1,nel
            zn = shp(4,i,lint)*xsj(lint)
        
            a33 = detfi(lint)*ep/sigma*zn

            row = i
            
            e_vector(row) = e_vector(row) - a33*(
     &           nv(1)*( e_flux_input(1,i)*ci(1,1,lint)
     &                +  e_flux_input(2,i)*ci(2,1,lint)
     &                +  e_flux_input(3,i)*ci(3,1,lint) )

     &         + nv(2)*( e_flux_input(1,i)*ci(1,2,lint)
     &                +  e_flux_input(2,i)*ci(2,2,lint)
     &                +  e_flux_input(3,i)*ci(3,2,lint) )

     &         + nv(3)*( e_flux_input(1,i)*ci(1,3,lint)
     &                +  e_flux_input(2,i)*ci(2,3,lint)
     &                +  e_flux_input(3,i)*ci(3,3,lint) )
     &           )
         end do
      end do


      end
      
c            a33 = ep/sigma*zn*(
c     &           nv(1)*e_flux_input(1,i) + nv(2)*e_flux_input(2,i)
c     &           + nv(3)*e_flux_input(3,i) )
