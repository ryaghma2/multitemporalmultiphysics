c$Id:$
      subroutine grad_scalar(b,u,x,ix,numnp,numel)

      implicit  none

      integer   numnp,numel,lint,nel,ndm

      integer   n,i,k,j,l,nix,nix1

      integer   ix(17,numel)

      integer   count(numnp)

      integer   ig(4),jg(4)

      real*8    x(3,numnp)

      real*8    b(3,numnp),u(numnp)

      real*8    shp(4,8,8),sg(4,8),xsj(8),xl(3,8)
      
      real*8    eb(3,8),amtr(24,24),bmtr(24),cmtr(24)
      real*8    g

      save

      data       ig/-1,1,1,-1/,jg/-1,-1,1,1/

      lint = 8
      nel = 8
      ndm = 3
      
      do i = 1,numnp
         do j = 1,ndm
            b(j,i) = 0.0d0
         end do
      end do
      do i = 1,numnp
         count(i) = 0
      end do
      
      do n = 1,numel
c     initialize shp, sg, xsj, xl
         do i = 1,lint
            do j = 1,nel
               do k = 1,4
                  shp(k,j,i) = 0.0d0
               end do
            end do
         end do
         do i = 1,8
            do j = 1,4
               sg(j,i) = 0.0d0
            end do
         end do
         do i = 1,lint
            xsj(i) = 0.0d0
         end do
         do i = 1,8
            do j = 1,3
               xl(j,i) = 0.0d0
            end do
         end do
         
c     collect element coord.
         do i = 1,nel
            do j = 1,ndm
               xl(j,i) = x(j,ix(i,n))
            end do
         end do
         
         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg(1,i) = ig(i)*g
            sg(1,i+4) = sg(1,i)
            sg(2,i) = jg(i)*g
            sg(2,i+4) = sg(2,i)
            sg(3,i) = -g
            sg(3,i+4) = g 
            sg(4,i) = 1.0d0
            sg(4,i+4) = 1.0d0
         end do
         
         do i = 1,lint
            call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
         end do

c     evaluate gradient of u on integration points
         do i = 1,8
            do j = 1,3
               eb(j,i) = 0.0d0
            end do
         end do
         
         do j = 1,lint
            nix = ix(j,n)
            do k = 1,ndm
               do i = 1,nel
                  nix1 = ix(i,n)
                  eb(k,j) = eb(k,j) + shp(k,i,j)*u(nix1)
               end do
            end do
            count(nix) = count(nix) + 1
         end do

         do i = 1,24
            bmtr(i) = 0.0d0
            cmtr(i) = 0.0d0
         end do
    
         i = 1
         do j = 1,nel
            bmtr(3*j-2) = eb(1,i)
            bmtr(3*j-1) = eb(2,i)
            bmtr(3*j) = eb(3,i)
            i = i + 1
         end do

         do i = 1,(3*nel)
            do j = 1,(3*nel)
               amtr(j,i) = 0.0d0
            end do
         end do

         i = 1
         do j = 1,nel
            l = 1
            do k = 1,nel
               amtr(3*j-2,3*k-2) = shp(4,l,i)
               amtr(3*j-1,3*k-1) = shp(4,l,i)
               amtr(3*j  ,3*k  ) = shp(4,l,i)
               l = l + 1
            end do
            i = i + 1
         end do

         call solgau(amtr,cmtr,bmtr,24)
    
         do i = 1,nel
            nix = ix(i,n)
            b(1,nix) = b(1,nix) + cmtr(3*i-2)
            b(2,nix) = b(2,nix) + cmtr(3*i-1)
            b(3,nix) = b(3,nix) + cmtr(3*i)
         end do
      end do

c 
      do i = 1,numnp
         do j = 1,ndm
            b(j,i) = b(j,i)/count(i)
         end do
      end do

      end
