c$Id:$
      subroutine get_hfield(h,b,veld,numnp,ep,mu)
c     
c     h = 1/mu*b - vel x d 
c     

      implicit  none

      integer   numnp,i,j

      real*8    ep,mu

      real*8    h(3,numnp),b(3,numnp),veld(3,numnp)

      save

      do i = 1,numnp
         do j = 1,3
            h(j,i) = b(j,i) - veld(j,i)
         end do
      end do

      end
