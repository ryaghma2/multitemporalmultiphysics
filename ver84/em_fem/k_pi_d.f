c$Id:$
      subroutine k_pi_d(
     &     e_matrix,e_vector,
     &     shp,xsj,nel,ndm,numnp,numel,nlint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      implicit  none

      integer   ndm,nel,lint,nlint

      integer   numnp,numel

      integer   i,j
      integer   row,col

      real*8    dt,ep,mu,sigma

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)

      real*8    shp(4,nel,nlint),xsj(nlint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_matrix(nel,nel)
      real*8    e_vector(nel)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    xn,yn,zn,a11,a22,a33

      save

c$    formulate element k_matrix
c     
      do lint = 1,nlint
         do i = 1,nel
            xn = shp(1,i,lint)*xsj(lint)
            yn = shp(2,i,lint)*xsj(lint)
            zn = shp(3,i,lint)*xsj(lint)

            a11 = detfi(lint)*ep*xn
            a22 = detfi(lint)*ep*yn
            a33 = detfi(lint)*ep*zn
            
            row = i

            do j = 1,nel
               xn = shp(1,j,lint)
               yn = shp(2,j,lint)
               zn = shp(3,j,lint)
               
               col = j
               
               e_matrix(row,col) = e_matrix(row,col)
     &              + ci(3,3,lint)*a33*zn + ci(2,3,lint)*a33*yn
     &              + ci(3,2,lint)*a22*zn + ci(2,2,lint)*a22*yn
     &              + ci(1,3,lint)*a33*xn + ci(1,2,lint)*a22*xn
     &              + ci(3,1,lint)*a11*zn + ci(2,1,lint)*a11*yn
     &              + ci(1,1,lint)*a11*xn

            end do
         end do
      end do

      end
