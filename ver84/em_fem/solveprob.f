c$Id:$
      subroutine solveprob(k_matrix,f_vector,x_ans_s,numnp,m1,m2)

      implicit  none

      integer   numnp,numnp1,numnp2,m1,m2
      
      integer   i,j,k

      real*8    k_matrix(4*numnp,4*numnp)
      real*8    f_vector(4*numnp)
      
      real*8    k11(numnp-m1,numnp-m1)
      real*8    k12(numnp-m1,3*numnp-m2)
      real*8    k21(3*numnp-m2,numnp-m1)
      real*8    k22(3*numnp-m2,3*numnp-m2)

      real*8    f1(numnp-m1),f2(3*numnp-m2)

      real*8    ki11(numnp-m1,numnp-m1)
      real*8    ki1112(numnp-m1,3*numnp-m2)
      real*8    k21i11(3*numnp-m2,numnp-m1)

      real*8    x_ans_s(4*numnp)

      real*8    x_ans_pi(numnp-m1),x_ans_a(3*numnp-m2)

      real*8    temp

c      real*8    k22_tmp(3*numnp-m2,3*numnp-m2) !

      save

      numnp1 = numnp - m1
      numnp2 = 3*numnp - m2
    
c$    separate k_matrix & f_vector
      do i = 1,numnp1
         do j = 1,numnp1
            k11(i,j) = k_matrix(i,j)
         end do
         do j = 1,numnp2
            k12(i,j) = k_matrix(i,j+numnp1)
         end do
      end do

      do i = 1,numnp2
         do j = 1,numnp1
            k21(i,j) = k_matrix(i+numnp1,j)
         end do
         do j = 1,numnp2
            k22(i,j) = k_matrix(i+numnp1,j+numnp1)
         end do
      end do

c      do i = 1,numnp2
c         do j = 1,numnp2
c            k22_tmp(i,j) = k22(i,j)
c         end do
c      end do
c      call invert(k22_tmp,numnp2,numnp2)                   !
c      write(*,*) 'k22_tmp_inv'
c      do i = 1,numnp2
c         write(*,*) (k22_tmp(i,j),j=1,numnp2)
c      end do

      do i = 1,numnp1
         f1(i) = f_vector(i)
      end do
      do i = 1,numnp2
         f2(i)= f_vector(i+numnp1)
      end do

c$    substitution
      do i = 1,numnp1
         do j = 1,numnp1
            ki11(i,j) = k11(i,j)
         end do
      end do
      
c     matrix inversion
      call invert(ki11,numnp1,numnp1) ! call inverse(ki11,numnp1)

c     matrix
      do i = 1,numnp1
         do j = 1,numnp2
            temp = 0.0d0
            do k = 1,numnp1
               temp = temp + ki11(i,k)*k12(k,j)
            end do
            ki1112(i,j) = temp
         end do
      end do
      
      do i = 1,numnp2
         do j = 1,numnp2
            temp = 0.0d0
            do k = 1,numnp1
               temp = temp + k21(i,k)*ki1112(k,j)
            end do
            k22(i,j) = k22(i,j) - temp
         end do
      end do

c     vector
      do i = 1,numnp2
         do j = 1,numnp1
            temp = 0.0d0
            do k = 1,numnp1
               temp = temp + k21(i,k)*ki11(k,j)
            end do
            k21i11(i,j) = temp
         end do
      end do
      do i = 1,numnp2
         temp = 0.0d0
         do j = 1,numnp1
            temp = temp + k21i11(i,j)*f1(j)
         end do
         f2(i) = f2(i) - temp
      end do
      
c$    solve
      do i = 1,numnp1
         x_ans_pi(i) = 0.0d0
      end do
      do i = 1,numnp2
         x_ans_a(i) = 0.0d0
      end do

      call solgau(k22,x_ans_a,f2,numnp2)
      
      do i = 1,numnp1
         temp = 0.0d0
         do j = 1,numnp1
            temp = temp + ki11(i,j)*f1(j)
         end do
         x_ans_pi(i) = temp
      end do
      
      do i = 1,numnp1
         temp = 0.0d0
         do j = 1,numnp2
            temp = temp + ki1112(i,j)*x_ans_a(j)
         end do
         x_ans_pi(i) = x_ans_pi(i) - temp
      end do
      
c$    rearrange
      do i = 1,numnp1
         x_ans_s(i) = x_ans_pi(i)
      end do
      do i = 1,numnp2
         x_ans_s(i+numnp1) = x_ans_a(i)
      end do
      

      end
      
