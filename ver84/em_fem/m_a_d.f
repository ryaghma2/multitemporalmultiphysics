c$Id:$
      subroutine m_a_d(
     &     e_matrix,e_vector,
     &     shp,xsj,nel,ndm,numnp,numel,nlint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      implicit  none

      integer   ndm,nel,lint,nlint

      integer   numnp,numel

      integer   i,j,k
      integer   row,col

      real*8    dt,ep,mu,sigma

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)

      real*8    shp(4,nel,nlint),xsj(nlint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_matrix(nel,3*nel)
      real*8    e_vector(nel)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    xn,yn,zn,a11,a22,a33,ce

      save

      ce = 1.0d0/dt

c$    formulate element m_matrix
c     
      do lint = 1,nlint
         do i = 1,nel
            xn = shp(1,i,lint)*xsj(lint)
            yn = shp(2,i,lint)*xsj(lint)
            zn = shp(3,i,lint)*xsj(lint)

            a11 = detfi(lint)*ep*xn
            a22 = detfi(lint)*ep*yn
            a33 = detfi(lint)*ep*zn
            
            row = i

            do j = 1,nel
               xn = shp(4,j,lint)
               yn = shp(4,j,lint)
               zn = shp(4,j,lint)

               col = 3*j
               
               e_matrix(row,col-2) = e_matrix(row,col-2)
     &              + ci(1,3,lint)*a33*xn
     &              + ci(1,2,lint)*a22*xn
     &              + ci(1,1,lint)*a11*xn

               e_matrix(row,col-1) = e_matrix(row,col-1)
     &              + ci(2,3,lint)*a33*yn
     &              + ci(2,2,lint)*a22*yn
     &              + ci(2,1,lint)*a11*yn

               e_matrix(row,col  ) = e_matrix(row,col  )
     &              + ci(3,3,lint)*a33*zn
     &              + ci(3,2,lint)*a22*zn
     &              + ci(3,1,lint)*a11*zn

            end do
         end do
      end do

c$    form force vectors w/ e_a_n
c     e_a_n
      do i = 1,nel
         do j = 1,nel
            do k = 1,ndm
               e_vector(i) = e_vector(i)
     &              + ce*e_matrix(i,3*j-3+k)*e_a_n(k,j)
            end do
         end do
      end do

      end
