c$Id:$
      subroutine get_h_d_fields(
     &     h,d,b,e,ec,
     &     gradphi,phi_n,a_n,d_a_n,
     &     mat_prop,
     &     x,ix,
     &     numnp,numel,ndm,nel,lint,
     &     mechvel,mechdisp)

      implicit  none

      integer   numnp,numel,ndm,nel,lint

      integer   i,j,mtlidx

      integer   ix(9,numel)

      real*8    h(3,numnp),d(3,numnp),b(3,numnp),e(3,numnp),ec(3,numnp)

      real*8    gradphi(3,numnp),phi_n(numnp)
      real*8    a_n(3,numnp),d_a_n(3,numnp)

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    mechvel(3,numnp),mechdisp(3,numnp)

      real*8    fivel(3,numnp)

      real*8    velb(3,numnp),veld(3,numnp),etilda(3,numnp)

      real*8    h_tmp(3,numnp)

      real*8    ep,mu

      save

c      mtlidx = ix(9,1)

c      ep = mat_prop(1,mtlidx) ! need to be changed
c      mu = mat_prop(2,mtlidx)

c     curl a_n
      call curl_vector(b,a_n,x,ix,numnp,numel)

c     grad phi
      call grad_scalar(gradphi,phi_n,x,ix,numnp,numel)

c     fi.vel
      call fidotvector(fivel,mechvel,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,
     &     mechdisp)

c     vel x b
      call cross_product(velb,fivel,b,numnp)

c     e field tmp
      call get_efield(e,gradphi,d_a_n,numnp)

c     e_tilda field
      call get_efield_tilda(etilda,e,velb,numnp)

c     d field
c      call vectordotci(d,etilda,
c     &     numnp,numel,ndm,nel,lint,
c     &     x,ix,mat_prop,
c     &     mechdisp)

c     e field in current
      call vectordotfi(d,etilda,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp)

cc     d field
c      do i = 1,numnp
c         do j = 1,3
c            d(j,i) = etilda(j,i)
c         end do
c      end do

c     vel x d
c      call cross_product(veld,fivel,d,numnp)

c     h field_tmp
c      call vectordotc(h_tmp,b,
c     &     numnp,numel,ndm,nel,lint,
c     &     x,ix,mat_prop,
c     &     mechdisp)

c     h field
c      call get_hfield(h,h_tmp,veld,numnp,ep,mu)

      end


