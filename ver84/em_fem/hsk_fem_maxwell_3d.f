c$Id:$
      subroutine hsk_fem_maxwell_3d(
     &     mat_prop,
     &     ts,dt,
     &     w_em,mag_em,

     &     numnp_cm,numel_cm,
     &     x_cm,ix_cm,
     &     mechvel_cm,mechacc_cm,
     &     mechdisp_cm,mechdisp_1_cm,
     &     nsbc_pi_cm,nsbc_a_cm,
     &     sbc_pi_cm,sbc_a_cm,
     &     nnbsurf_cm_j,
     &     ix_nbsurf_cm_j,
     &     a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &     phi_n_cm,d_phi_n_cm,
     &     hfield_cm,dfield_cm,
     &     bfield_cm,efield_cm,ecfield_cm
     &     )



c     F.E.M. FOR MAXWELL EQNS SCALAR POTENTIAL AND VECTOR POTENTIAL 

c     input data
c     x_fs,numnp_fs,ix_fs,numel_fs,nsbc_pi_fs,sbc_pi_fs,nsbc_a_fs,sbc_a_fs,
c     nnbsurf_fs_j,ix_nbsurf_fs_j
c     x_cm,numnp_cm,ix_cm,numel_cm,nsbc_pi_cm,sbc_pi_cm,nsbc_a_cm,sbc_a_cm,
c     nnbsurf_cm_j,ix_nbsurf_cm_j
c     mat_prop
c     nintfcix,intfcix,ix_nbsurf_fs,ix_nbsurf_cm

c$     time integration
c     mag_em : mag. of current flux
c     frq_em : Hz

c     w_em = 2*pi*frq_em;
c     dt_em = 1.0d0/(frq_em*2*10) % sec

c     plotnode1 = 137;
c     plotnode2 = 173;

c$    etc
c     ndm = 3.0d0

      implicit  none

      include   'time_flag.h'

      integer   ts,timesteps
      integer   ndm,i,j,k


      integer   plotnode
 

c$    conductor
      integer   numnp_cm,numel_cm,nsbc_pi_cm,nsbc_a_cm
      integer   nnbsurf_cm_j

      integer   ix_cm(9,numel_cm)
      integer   sbc_pi_cm(2,nsbc_pi_cm),sbc_a_cm(7,nsbc_a_cm)
      integer   ix_nbsurf_cm_j(6,nnbsurf_cm_j)





      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    mat_prop(3,2)


c$    conductor
      real*8    x_cm(3,numnp_cm)

      real*8    hfield_cm(3,numnp_cm),dfield_cm(3,numnp_cm)
      real*8    bfield_cm(3,numnp_cm),efield_cm(3,numnp_cm)
      real*8    ecfield_cm(3,numnp_cm)

      real*8    a_n_1_cm(3,numnp_cm),a_n_cm(3,numnp_cm)
      real*8    d_a_n_cm(3,numnp_cm),dd_a_n_cm(3,numnp_cm)
      real*8    phi_n_cm(numnp_cm),d_phi_n_cm(numnp_cm)

      real*8    mechvel_cm(3,numnp_cm),mechacc_cm(3,numnp_cm)
      real*8    mechdisp_cm(3,numnp_cm),mechdisp_1_cm(3,numnp_cm)



      save

      plotnode = 156

c$    start problem
      write(*,*) 'ts =',ts
c      do ts = 1,timesteps
    
         if(ts .eq. 1) then
           call fem_maxwell_cm_init(
     &           hfield_cm,dfield_cm,bfield_cm,efield_cm,
     &           phi_n_cm,d_phi_n_cm,
     &           a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &           ts,dt,w_me,mag_me,w_em,mag_em,
     &           x_cm,ix_cm,mat_prop,numnp_cm,numel_cm,
     &           mechvel_cm,mechacc_cm,
     &           mechdisp_cm,mechdisp_1_cm,
     &           ix_nbsurf_cm_j,nnbsurf_cm_j,
     &           sbc_pi_cm,nsbc_pi_cm,sbc_a_cm,nsbc_a_cm
     &           )

            startflag = .false.
         endif

         call fem_maxwell_cm(
     &        hfield_cm,dfield_cm,bfield_cm,efield_cm,ecfield_cm,
     &        phi_n_cm,d_phi_n_cm,
     &        a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &        ts,dt,w_me,mag_me,w_em,mag_em,
     &        x_cm,ix_cm,mat_prop,numnp_cm,numel_cm,
     &        mechvel_cm,mechacc_cm,
     &        mechdisp_cm,mechdisp_1_cm,
     &        ix_nbsurf_cm_j,nnbsurf_cm_j,
     &        sbc_pi_cm,nsbc_pi_cm,sbc_a_cm,nsbc_a_cm
     &        )

c         if(ts .eq. 1) then
c            call fem_maxwell_fs_init(
c     &           hfield_fs,dfield_fs,bfield_fs,efield_fs,
c     &           phi_n_fs,d_phi_n_fs,
c     &           a_n_1_fs,a_n_fs,d_a_n_fs,dd_a_n_fs,
c     &           ts,dt,w_me,mag_me,w_em,mag_em,
c     &           x_fs,ix_fs,mat_prop,numnp_fs,numel_fs,
c     &           mechvel_fs,mechacc_fs,
c     &           mechdisp_fs,mechdisp_1_fs,
c     &           ix_nbsurf_fs_j,nnbsurf_fs_j,
c     &           sbc_pi_fs,nsbc_pi_fs,sbc_a_fs,nsbc_a_fs,
c     &           nintfcix,ix_nbsurf_fs,ix_nbsurf_cm,
c     &           numnp_cm,
c     &           dfield_cm,hfield_cm)
c         endif
         
c         call fem_maxwell_fs(
c     &        hfield_fs,dfield_fs,bfield_fs,efield_fs,
c     &        phi_n_fs,d_phi_n_fs,
c     &        a_n_1_fs,a_n_fs,d_a_n_fs,dd_a_n_fs,
c     &        ts,dt,w_me,mag_me,w_em,mag_em,
c     &        x_fs,ix_fs,mat_prop,numnp_fs,numel_fs,
c     &        mechvel_fs,mechacc_fs
c     &        mechdisp_fs,mechdisp_1_fs,
c     &        ix_nbsurf_fs_j,nnbsurf_fs_j,
c     &        sbc_pi_fs,nsbc_pi_fs,sbc_a_fs,nsbc_a_fs,
c     &        nintfcix,ix_nbsurf_fs,ix_nbsurf_cm,
c     &        numnp_cm,
c     &        dfield_cm,hfield_cm)

c      end do
         ts = ts + 1
         
         write(*,*) 'a_n_cm'
         write(*,*) (a_n_cm(i,plotnode),i=1,3)

         open(unit=1,status='unknown')
         write(1,*)(phi_n_cm(j),j=1,225)
         open(unit=2,status='unknown')
         write(2,*)(a_n_cm(2,j),j=1,225)
         open(unit=3,status='unknown')
         write(3,*)(bfield_cm(1,j),j=1,225)
         open(unit=4,status='unknown')
         write(4,*)(bfield_cm(3,j),j=1,225)
c         open(unit=3,status='unknown')
c         write(3,*)(bfield_cm(j,plotnode),j=1,3)
         open(unit=7,status='unknown')
         write(7,*)(efield_cm(2,j),j=1,225)
c         open(unit=7,status='unknown')
c         write(7,*)(a_n_cm(j,plotnode),j=1,3)
         open(unit=8,status='unknown')
         write(8,*)(dfield_cm(2,j),j=1,225)

c         open(unit=3,status='unknown')
c         write(3,*)(mechvel_cm(j,plotnode),j=1,3)
c         open(unit=4,status='unknown')
c         write(4,*)(mechdisp_cm(j,plotnode),j=1,3)

         
         
       end
