c$Id:$
      subroutine get_efield_tilda(et,e,velb,numnp)
c     
c     e_tilda = e + velb( vel x b )
c     

      implicit  none

      integer   numnp,i,j

      real*8    et(3,numnp),e(3,numnp),velb(3,numnp)

      save

      do i = 1,numnp
         do j = 1,3
            et(j,i) = e(j,i) + velb(j,i)
         end do
      end do
      
      end


