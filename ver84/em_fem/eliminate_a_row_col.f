c$Id:$
      subroutine eliminate_a_row_col(
     &     amatrix,bvector,
     &     nbc_pi,nbc,bcs,
     &     numnp,
     &     m1,m2)

c     nbc     : number of specified nodes
c     bcs     : info. of specified nodes 
c     bcs = [node num., value]
c     numnp   : number of nodes

      implicit  none

      integer   i,j,k,l,node,col

      integer   nbc_pi,nbc,numnp,m1,m2,stp

      integer   bcs(7,nbc)

      real*8    amatrix(4*numnp,4*numnp)
      real*8    bvector(4*numnp)

      save

c$    reduce size of k_matrix and f_vector for a
      stp = numnp - nbc_pi
c     reduce size of k_matrix and f_vector
      do i = 1,nbc
         node = bcs(1,i)
         do k = 1,3
            if(bcs(k+1,i) .eq. 1) then
c     f_vector
               do j = (stp+3*(node-1)+k+1-m2),(stp+3*numnp-m2)
                  col = stp+3*node-3+k-m2 ! column at specified d.o.f.
                  bvector(j-1) = bvector(j)-amatrix(j,col)*bcs(k+4,i)
               end do
               do j = ( 1 ),(stp+3*(node-1)+k-1-m2)
                  col = stp+3*node-3+k-m2 ! column at specified d.o.f.
                  bvector(j) = bvector(j) - amatrix(j,col)*bcs(k+4,i)
               end do

c     k_matrix
               do j = (stp+3*node-3+k+1-m2),(stp+3*numnp-m2)
                  do l = ( 1 ),(stp+3*numnp-m2)
                     amatrix(l,j-1) = amatrix(l,j)
                  end do
               end do
               do j = (stp+3*node-3+k+1-m2),(stp+3*numnp-m2)
                  do l = ( 1 ),(stp+3*numnp-m2-1)
                     amatrix(j-1,l) = amatrix(j,l)
                  end do
               end do
               m2 = m2 + 1
C               write (*,*) "m2=",m2
            endif
         end do
      end do
      
      end
      
