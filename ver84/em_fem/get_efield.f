c$Id:$
      subroutine get_efield(e,gp,da,numnp)
c     
c     e = - grad phi - d_a_n
c     

      implicit  none

      integer   numnp,i,j
      
      real*8    e(3,numnp),gp(3,numnp),da(3,numnp)

      save

      do i = 1,numnp
         do j = 1,3
            e(j,i) = - gp(j,i) - da(j,i)
         end do
      end do

      end
