c$Id:$
      subroutine rearrange_pi_soln(
     &     x_ans,x_ans_reduced,
     &     numnp,bcs,nbc)

      implicit  none
      
      integer   numnp,nbc
      
      integer   i,j,node,m,value
      
      integer   bcs(2,nbc)
      
      real*8    x_ans(numnp),x_ans_reduced(4*numnp)
      
      save
      
c$    re-arrange solutions from reduced_size to original size
c     for pi
      m = 1
      do i = 1,numnp
         value = 1
         do j = 1,nbc
            node = bcs(1,j)
            if(i .eq. node) then
               x_ans(i) = bcs(2,j)
               value = 0
            endif
         end do
         
         if(value .eq. 1) then
            x_ans(i) = x_ans_reduced(m)
            m = m + 1
         endif
      end do
      
      end
