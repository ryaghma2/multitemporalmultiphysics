subroutine inverse(xjac,nsize)
  implicit real*8(a-h,o-z)
  dimension xjac(nsize,nsize),ipiv(nsize),work(nsize)
  
  call dgetrf(nsize,nsize,xjac,nsize,ipiv,info)
  
  if(info.eq.0)then
     call dgetri(nsize,xjac,nsize,ipiv,work,nsize,info)
     if(info.ne.0)then
        write(*,*)'Inversion failed'
        stop
     endif
  else
     write(*,*)'LU decompose failed'
     stop
  endif
  
end subroutine inverse
