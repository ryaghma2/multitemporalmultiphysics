c$Id:$
      subroutine kine2d_lag(shp,ul,f,fi,c,ci,detf,lint)

c      Inputs:
c         shp(3,4,4)   - Reference configuration shape functions
c         ul(3,4,4) - Nodal displacements
c         ndm           - Number mesh dimensions
c         nel           - Number nodes/element
c         lint          - quadrature points

c      Outputs:
c         f(3,3,4)    - Deformation gradient
c         fi(3,3,4)     - Inverse deformation gradient
c         c(3,3,4)
c         ci(3,3,4)
c         detf(4)     - Determinant of deformation gradient

      implicit  none

      integer   ndm,ndf,nel,nen,lint, i,j,k,l

      real*8    detfi,temp,xx1,uu1,du1
      real*8    shp(3,4,4),xl(2,4),ul(2,4)
      real*8    f(3,3,4),fi(3,3,4),detf(4)
      real*8    c(3,3,4),ci(3,3,4),detfic(4),detic

      save

c     Deformation gradient at t_n+1 : F_n+1 = I + GRAD u_n+1
      
      do i = 1,3
         do j = 1,3
            f(j,i,lint) = 0.0d0
c            fi(j,i,lint) = 0.0d0
c            c(j,i,lint) = 0.0d0
c            ci(j,i,lint) = 0.0d0
         end do ! j
      end do ! i
      do i = 1,2
         do j = 1,2
            do k = 1,4
               f(i,j,lint) = f(i,j,lint) + ul(i,k)*shp(j,k,lint)
            end do ! k
         end do ! j
         f(i,i,lint) = f(i,i,lint) + 1.0d0
      end do ! i
      f(3,3,lint) = 1.0d0

cc     Invert F
c      
c      detf(lint) = f(1,1,lint)*f(2,2,lint) - f(1,2,lint)*f(2,1,lint)
c      
c      detfi   =  1.d0/detf(lint)
c      fi(1,1,lint) =  f(2,2,lint)*detfi
c      fi(1,2,lint) = -f(1,2,lint)*detfi
c      fi(1,3,lint) =  0.0d0
c      fi(2,1,lint) = -f(2,1,lint)*detfi
c      fi(2,2,lint) =  f(1,1,lint)*detfi
c      fi(2,3,lint) =  0.0d0
c      fi(3,3,lint) =  1.0d0/f(3,3,lint)
c      
cc     Determinants
c      
c      detf(lint) = detf(lint)*f(3,3,lint)
c
c      fi(3,1,lint) = 0.0d0
c      fi(3,2,lint) = 0.0d0

      end
