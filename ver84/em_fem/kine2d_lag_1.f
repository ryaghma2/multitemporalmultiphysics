c$Id:$
      subroutine kine2d_lag_1(shp,ul,f,fi,c,ci,detfi,lint)

c      Inputs:
c         shp(3,4,4)   - Reference configuration shape functions
c         ul(3,4,4) - Nodal displacements
c         ndm           - Number mesh dimensions
c         nel           - Number nodes/element
c         lint          - quadrature points

c      Outputs:
c         f(3,3,4)    - Deformation gradient
c         fi(3,3,4)     - Inverse deformation gradient
c         c(3,3,4)
c         ci(3,3,4)
c         detf(4)     - Determinant of deformation gradient

      implicit  none

      integer   ndm,ndf,nel,nen,lint, i,j,k,l

      real*8    deti,detfi(4)

      real*8    temp,xx1,uu1,du1
      real*8    shp(3,4,4),xl(2,4),ul(2,4)
      real*8    f(3,3,4),fi(3,3,4),detf(4)
      real*8    c(3,3,4),ci(3,3,4),detfic(4),detic

      save

c     Invert F
      detfi(lint) = f(1,1,lint)*f(2,2,lint)*f(3,3,lint)
     &            + f(1,2,lint)*f(2,3,lint)*f(3,1,lint)
     &            + f(1,3,lint)*f(2,1,lint)*f(3,2,lint)
     &            - f(3,1,lint)*f(2,2,lint)*f(1,3,lint)
     &            - f(3,2,lint)*f(2,3,lint)*f(1,1,lint)
     &            - f(3,3,lint)*f(2,1,lint)*f(1,2,lint)

      deti    = 1.0d0/detfi(lint)
      
      fi(1,1,lint) = (f(2,2,lint)*f(3,3,lint)
     &             - f(3,2,lint)*f(2,3,lint))*deti
      fi(1,2,lint) =-(f(1,2,lint)*f(3,3,lint)
     &             - f(3,2,lint)*f(1,3,lint))*deti
      fi(1,3,lint) = (f(1,2,lint)*f(2,3,lint)
     &             - f(2,2,lint)*f(1,3,lint))*deti
      fi(2,1,lint) =-(f(2,1,lint)*f(3,3,lint)
     &             - f(3,1,lint)*f(2,3,lint))*deti
      fi(2,2,lint) = (f(1,1,lint)*f(3,3,lint)
     &             - f(3,1,lint)*f(1,3,lint))*deti
      fi(2,3,lint) =-(f(1,1,lint)*f(2,3,lint)
     &             - f(2,1,lint)*f(1,3,lint))*deti
      fi(3,1,lint) = (f(2,1,lint)*f(3,2,lint)
     &             - f(3,1,lint)*f(2,2,lint))*deti
      fi(3,2,lint) =-(f(1,1,lint)*f(3,2,lint)
     &             - f(3,1,lint)*f(1,2,lint))*deti
      fi(3,3,lint) = (f(1,1,lint)*f(2,2,lint)
     &             - f(2,1,lint)*f(1,2,lint))*deti

c     C : Right Cauchy Green Tensor
      c(1,1,lint) = f(1,1,lint)*f(1,1,lint) + f(1,2,lint)*f(1,2,lint)
     &            + f(1,3,lint)*f(1,3,lint)
      c(2,1,lint) = f(1,1,lint)*f(2,1,lint) + f(1,2,lint)*f(2,2,lint)
     &            + f(1,3,lint)*f(2,3,lint)
      c(3,1,lint) = f(1,1,lint)*f(3,1,lint) + f(1,2,lint)*f(3,2,lint)
     &            + f(1,3,lint)*f(3,3,lint)
      c(1,2,lint) = f(2,1,lint)*f(1,1,lint) + f(2,2,lint)*f(1,2,lint)
     &            + f(2,3,lint)*f(1,3,lint)
      c(2,2,lint) = f(2,1,lint)*f(2,1,lint) + f(2,2,lint)*f(2,2,lint)
     &            + f(2,3,lint)*f(2,3,lint)
      c(3,2,lint) = f(2,1,lint)*f(3,1,lint) + f(2,2,lint)*f(3,2,lint)
     &            + f(2,3,lint)*f(3,3,lint)
      c(1,3,lint) = f(3,1,lint)*f(1,1,lint) + f(3,2,lint)*f(1,2,lint)
     &            + f(3,3,lint)*f(1,3,lint)
      c(2,3,lint) = f(3,1,lint)*f(2,1,lint) + f(3,2,lint)*f(2,2,lint)
     &            + f(3,3,lint)*f(2,3,lint)
      c(3,3,lint) = f(3,1,lint)*f(3,1,lint) + f(3,2,lint)*f(3,2,lint)
     &            + f(3,3,lint)*f(3,3,lint)

c     Invert C
      detfic(lint) = c(1,1,lint)*c(2,2,lint)*c(3,3,lint)
     &              + c(1,2,lint)*c(2,3,lint)*c(3,1,lint)
     &              + c(1,3,lint)*c(2,1,lint)*c(3,2,lint)
     &              - c(3,1,lint)*c(2,2,lint)*c(1,3,lint)
     &              - c(3,2,lint)*c(2,3,lint)*c(1,1,lint)
     &              - c(3,3,lint)*c(2,1,lint)*c(1,2,lint)

      detic    = 1.0d0/detfic(lint)

      ci(1,1,lint) = (c(2,2,lint)*c(3,3,lint) - c(3,2,lint)*c(2,3,lint))
     &             *detic
      ci(1,2,lint) =-(c(1,2,lint)*c(3,3,lint) - c(3,2,lint)*c(1,3,lint))
     &             *detic
      ci(1,3,lint) = (c(1,2,lint)*c(2,3,lint) - c(2,2,lint)*c(1,3,lint))
     &             *detic
      ci(2,1,lint) =-(c(2,1,lint)*c(3,3,lint) - c(3,1,lint)*c(2,3,lint))
     &             *detic
      ci(2,2,lint) = (c(1,1,lint)*c(3,3,lint) - c(3,1,lint)*c(1,3,lint))
     &             *detic
      ci(2,3,lint) =-(c(1,1,lint)*c(2,3,lint) - c(2,1,lint)*c(1,3,lint))
     &             *detic
      ci(3,1,lint) = (c(2,1,lint)*c(3,2,lint) - c(3,1,lint)*c(2,2,lint))
     &             *detic
      ci(3,2,lint) =-(c(1,1,lint)*c(3,2,lint) - c(3,1,lint)*c(1,2,lint))
     &             *detic
      ci(3,3,lint) = (c(1,1,lint)*c(2,2,lint) - c(2,1,lint)*c(1,2,lint))
     &             *detic
      
      end
