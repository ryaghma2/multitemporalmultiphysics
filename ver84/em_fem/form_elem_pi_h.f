c$Id:$
      subroutine form_elem_pi_h(
     &     k_matrix,f_vector,n,
     &     x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &     ts,dt,t,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     f_1,fi_1,c_1,ci_1,detfi_1,
     &     d_f,d_fi,d_c,d_ci,d_detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      implicit  none

      integer   ts
      integer   ndm,nel,lint

      integer   numnp,numel

      integer   ix(9,numel)

      integer   i,j
      integer   n,grow,lrow,gcol,lcol

      real*8    dt
      real*8    t
      real*8    ep,mu,sigma

      real*8    x(3,numnp)

      real*8    k_matrix(4*numnp,4*numnp),f_vector(4*numnp)

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)

      real*8    shp(4,nel,lint),xsj(lint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    k_matrix_e(3*nel,nel),c_matrix_e(3*nel,nel)
      real*8    m_matrix_e(3*nel,nel)
      real*8    k_vector_e(3*nel),c_vector_e(3*nel)
      real*8    m_vector_e(3*nel)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    f_1(3,3,8),fi_1(3,3,8),c_1(3,3,8),ci_1(3,3,8)
      real*8    detfi_1(8)

      real*8    d_f(3,3,8),d_fi(3,3,8),d_c(3,3,8),d_ci(3,3,8)
      real*8    d_detfi(8)

      real*8    ce

      save

      ce = 1.0d0/dt

c$    integration @ gaussian points
c     call element library to form stiffness matrix
c     and force vector
      do i = 1,3*nel
         do j = 1,nel
            k_matrix_e(i,j) = 0.0d0
            c_matrix_e(i,j) = 0.0d0
            m_matrix_e(i,j) = 0.0d0
         end do
         k_vector_e(i) = 0.0d0
         c_vector_e(i) = 0.0d0
         m_vector_e(i) = 0.0d0
      end do

c$    form k_matrix of pi

c     [ep*d_J*{-grad pi}.ci].AA
      call k_pi_h(
     &     k_matrix_e,k_vector_e,
     &     shp,xsj,nel,ndm,numnp,numel,lint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,d_detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c     [ep*J*{-grad pi}.d_ci].AA
      call k_pi_h(
     &     k_matrix_e,k_vector_e,
     &     shp,xsj,nel,ndm,numnp,numel,lint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,d_ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c     -[(ep*J*{-grad pi}.ci) x (fi.vel)].(curl AA)
      call k_pi_h_1(
     &     k_matrix_e,k_vector_e,
     &     shp,xsj,nel,ndm,numnp,numel,lint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c     [sigma*J*{-grad pi}.ci].AA
c     ep = sigma                 ! be carfule of the constant of sigma
      call k_pi_h(
     &     k_matrix_e,k_vector_e,
     &     shp,xsj,nel,ndm,numnp,numel,lint,
     &     dt,sigma,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      do i = 1,nel
         grow = 3*ix(i,n) + numnp
         lrow = 3*i

         do j = 1,nel
            gcol = ix(j,n)
            lcol = j
            
            k_matrix(grow-2,gcol) = k_matrix(grow-2,gcol) 
     &           + k_matrix_e(lrow-2,lcol)
            k_matrix(grow-1,gcol) = k_matrix(grow-1,gcol) 
     &           + k_matrix_e(lrow-1,lcol)
            k_matrix(grow  ,gcol) = k_matrix(grow  ,gcol) 
     &           + k_matrix_e(lrow  ,lcol)
         end do
         f_vector(grow-2) = f_vector(grow-2) + k_vector_e(lrow-2)
         f_vector(grow-1) = f_vector(grow-1) + k_vector_e(lrow-1)
         f_vector(grow  ) = f_vector(grow  ) + k_vector_e(lrow  )
      end do

c$    form m_matrix of a
c     [ep*J*{-grad d_pi}.ci].AA
      call m_pi_h(
     &     m_matrix_e,m_vector_e,
     &     shp,xsj,nel,ndm,numnp,numel,lint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      do i = 1,nel
         grow = 3*ix(i,n) + numnp
         lrow = 3*i

         do j = 1,nel
            gcol = ix(j,n)
            lcol = j
            
            k_matrix(grow-2,gcol) = k_matrix(grow-2,gcol) 
     &           + ce*m_matrix_e(lrow-2,lcol)
            k_matrix(grow-1,gcol) = k_matrix(grow-1,gcol) 
     &           + ce*m_matrix_e(lrow-1,lcol)
            k_matrix(grow  ,gcol) = k_matrix(grow  ,gcol) 
     &           + ce*m_matrix_e(lrow  ,lcol)
         end do
         f_vector(grow-2) = f_vector(grow-2) + m_vector_e(lrow-2)
         f_vector(grow-1) = f_vector(grow-1) + m_vector_e(lrow-1)
         f_vector(grow  ) = f_vector(grow  ) + m_vector_e(lrow  )
      end do

      end
