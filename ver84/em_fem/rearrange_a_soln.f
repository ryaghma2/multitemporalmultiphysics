c$Id:$
      subroutine rearrange_a_soln(
     &     x_ans,x_ans_reduced,
     &     numnp,bcs,nbc,nbc_pi)
      
      implicit  none
      
      integer   numnp,nbc,nbc_pi
      
      integer   i,j,node,m,value
      integer   stp,xvalue,yvalue,zvalue
      
      integer   bcs(7,nbc)
      
      real*8    x_ans(3,numnp),x_ans_reduced(4*numnp)
      
      save

c$    re-arrange solutions from reduced_size to original size
c     for a
      stp = numnp - nbc_pi
      m = 1
      do i = 1,numnp
         xvalue = 1
         yvalue = 1
         zvalue = 1
         do j = 1,nbc
            node = bcs(1,j)
            if(i .eq. node) then
               if(bcs(2,j) .eq. 1) then
                  x_ans(1,i) = bcs(5,j)
                  xvalue = 0
               else
                  xvalue = 1
               endif
               if(bcs(3,j) .eq. 1) then
                  x_ans(2,i) = bcs(6,j)
                  yvalue = 0
               else
                  yvalue = 1
               endif
               if(bcs(4,j) .eq. 1) then
                  x_ans(3,i) = bcs(7,j)
                  zvalue = 0
               else
                  zvalue = 1
               endif
            endif
         end do

         if(xvalue .eq. 1) then
            x_ans(1,i) = x_ans_reduced(stp+m)
            m = m + 1
         endif
         if(yvalue .eq. 1) then
            x_ans(2,i) = x_ans_reduced(stp+m)
            m = m + 1
         endif
         if(zvalue .eq. 1) then
            x_ans(3,i) = x_ans_reduced(stp+m)
            m = m + 1
         endif
      end do

      end
