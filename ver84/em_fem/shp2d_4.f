c$Id:$
      subroutine shp2d_4(shp,xsj,xl,ss,ndm,nel,lint)

      implicit  none

      integer   ndm,nel,lint,i,j,k
      real*8    xsj(4),temp
      real*8    shp(3,4,4),xl(2,4),s(4),t(4),xs(2,2),sx(2,2),ss(3,4)

      save

c     Set values of half natural coords at nodes

      data s/-0.5d0,0.5d0,0.5d0,-0.5d0/,t/-0.5d0,-0.5d0,0.5d0,0.5d0/

c     Form 4-node quadrilateral shape functions
      do i = 1,nel
         shp(3,i,lint) = (0.5d0+s(i)*ss(1,lint))*(0.5d0+t(i)*ss(2,lint))
         shp(1,i,lint) = s(i)*(0.5d0+t(i)*ss(2,lint))
         shp(2,i,lint) = t(i)*(0.5d0+s(i)*ss(1,lint))
      end do
      
      do i = 1,ndm
         do j = 1,2
            xs(i,j) = 0.0d0
            do k = 1,nel
               xs(i,j) = xs(i,j) + xl(i,k)*shp(j,k,lint)
            end do
         end do
      end do
      
      xsj(lint) = xs(1,1)*xs(2,2)-xs(1,2)*xs(2,1)
      temp = 1.0d0/xsj(lint)
      
      sx(1,1) = xs(2,2)*temp
      sx(2,2) = xs(1,1)*temp
      sx(1,2) =-xs(1,2)*temp
      sx(2,1) =-xs(2,1)*temp
      
      do i = 1,nel
         temp          = shp(1,i,lint)*sx(1,1)+shp(2,i,lint)*sx(2,1)
         shp(2,i,lint) = shp(1,i,lint)*sx(1,2)+shp(2,i,lint)*sx(2,2)
         shp(1,i,lint) = temp
      end do
      
      end
