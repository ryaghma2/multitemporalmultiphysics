c$Id:$
      subroutine update_phi_a_d_a_dd_a_n(
     &     x_ans_pi,x_ans_a,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     numnp,ndm,dt)
      
      implicit  none
      
      integer   numnp,ndm
      
      integer   i,j
      
      real*8    dt,as(2),ce
      
      real*8    x_ans_pi(numnp),x_ans_a(3,numnp)
      
      real*8    phi_n(numnp),d_phi_n(numnp)
      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      
      save

c$    integration constants for dynamic problem of a
      as(1) = 1.0d0/(dt**2.0d0)
      as(2) = 1.0d0/dt

c$    update a_n, d_a_n, dd_a_n
      do i = 1,numnp
         do j = 1,ndm
            dd_a_n(j,i) = as(1)*(x_ans_a(j,i) - 2*a_n(j,i) + a_n_1(j,i))
            d_a_n(j,i) = as(2)*( x_ans_a(j,i) - a_n(j,i) )
            a_n_1(j,i) = a_n(j,i)
            a_n(j,i) = x_ans_a(j,i)
         end do
      end do
      
c$    update pi
      ce = 1.0d0/dt
      do i = 1,numnp
         d_phi_n(i) = ce*( x_ans_pi(i) - phi_n(i) )
         phi_n(i) = x_ans_pi(i)
      end do
      
      end
      
