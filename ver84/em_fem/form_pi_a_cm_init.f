c$Id:$
      subroutine form_pi_a_cm_init(
     &     k_matrix,f_vector,
     &     x,ix,mat_prop,nel,ndm,numnp,numel,lint,
     &     ts,dt,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     w_me,mag_me,w_em,mag_em,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     ix_nbsurf,nnbsurf
     &     )

c     k_matrix : global tangent matrix
c     f_vector : global force vector
c     x        : global node coord.
c     ix       : nodal connectivity
c     ndm      : dimension
c     nel      : nodes per element
c     numel    : number of elements
c     dt       : time step

      implicit  none

      integer   ts
      integer   ndm,nel,lint

      integer   numnp,numel
c      integer   nsbc_pi,nsbc_a
      integer   nnbsurf

      integer   numnp2

      integer   ix(9,numel)
c      integer   sbc_pi(2,nsbc_pi),sbc_a(7,nsbc_a)
      integer   ix_nbsurf(6,nnbsurf)

      integer   i,j,k
      integer   n,node,nb,mtlidx,ndirec,node1
      integer   ig(4),jg(4)

      integer   grow,lrow

      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    t
      real*8    ep,mu,sigma,g

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      real*8    phi_n(numnp),d_phi_n(numnp)

      real*8    k_matrix(4*numnp,4*numnp),f_vector(4*numnp)

      real*8    mechvel(3,numnp),mechacc(3,numnp)
      real*8    mechdisp(3,numnp),mechdisp_1(3,numnp)

      real*8    e_flux_input(ndm,4)
      real*8    e_flux_input_d(ndm,4),e_flux_input_h(ndm,4)

      real*8    sg(4,lint),xl(ndm,nel)
      real*8    shp(4,nel,lint),xsj(lint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)
      real*8    e_mechdisp(ndm,nel),e_mechdisp_1(ndm,nel)

      real*8    nbc_vector_e(4)
      real*8    nbc_vector_e_d(4),nbc_vector_e_h(3*4)

      real*8    sg2(3,4),xl2d(2,4),shp2(3,4,4),xsj2(4)
      real*8    shp2d(4,4,4)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    f_1(3,3,8),fi_1(3,3,8),c_1(3,3,8),ci_1(3,3,8)
      real*8    detfi_1(8)

      real*8    d_f(3,3,8),d_fi(3,3,8),d_c(3,3,8),d_ci(3,3,8)
      real*8    d_detfi(8)

      real*8    f2(3,3,4),fi2(3,3,4),c2(3,3,4),ci2(3,3,4)
      real*8    f2d(3,3,4),fi2d(3,3,4),c2d(3,3,4),ci2d(3,3,4)
      real*8    detfi2(4)

      real*8    e_mechdisp2d(2,4)

      save

      data       ig/-1,1,1,-1/,jg/-1,-1,1,1/

c$    form matrices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      t = dt*ts
c$    construct stiffness matrix
      do n = 1,numel
c     get material constant and form inverse matrix
         ep = mat_prop(1,ix(nel+1,n))
         mu = mat_prop(2,ix(nel+1,n))
         sigma = mat_prop(3,ix(nel+1,n))

c$    get element coords. and element mech vel.
         do i = 1,nel
            node = ix(i,n)
            do j = 1,ndm
               xl(j,i) = x(j,node)
            
               e_a_n_1(j,i) = a_n_1(j,node)
               e_a_n(j,i) = a_n(j,node)
               e_d_a_n(j,i) = d_a_n(j,node)
               e_dd_a_n(j,i) = dd_a_n(j,node)
               
               e_mechvel(j,i) = mechvel(j,node)
               e_mechacc(j,i) = mechacc(j,node)
               e_mechdisp(j,i) = mechdisp(j,node)
               e_mechdisp_1(j,i) = mechdisp_1(j,node)
            end do
            e_phi_n(i) = phi_n(node)
            e_d_phi_n(i) = d_phi_n(node)
        
c            if(sigma .le. 10.0d0) then
c               do j = 1,ndm
c                  e_mechvel(j,i) = 0.0d0
c               end do
c            else
c               do j = 1,ndm
c                  e_mechvel(j,i) = mechvel(j,node)
c               end do
c            endif
         end do
    
c$    shpfunc. values @ integration points
         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg(1,i) = ig(i)*g
            sg(1,i+4) = sg(1,i)
            sg(2,i) = jg(i)*g
            sg(2,i+4) = sg(2,i)
            sg(3,i) = -g
            sg(3,i+4) = g
            sg(4,i) = 1.0d0
            sg(4,i+4) = 1.0d0
         end do

         do i = 1,lint ! lint
            call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
            xsj(i) = xsj(i)*sg(4,i)
         end do
         
c     F, Fi, C, Ci
         do i = 1,lint
            do j = 1,3
               do k = 1,3
                  f(k,j,i) = 0.0d0
                  fi(k,j,i) = 0.0d0
                  c(k,j,i) = 0.0d0
                  ci(k,j,i) = 0.0d0

                  f_1(k,j,i) = 0.0d0
                  fi_1(k,j,i) = 0.0d0
                  c_1(k,j,i) = 0.0d0
                  ci_1(k,j,i) = 0.0d0

                  d_f(k,j,i) = 0.0d0
                  d_fi(k,j,i) = 0.0d0
                  d_c(k,j,i) = 0.0d0
                  d_ci(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,lint
            detfi(i) = 0.0d0
            detfi_1(i) = 0.0d0
            d_detfi(i) = 0.0d0
         end do
         
c     n+1 : current step
         do i = 1,8 ! lint
            call kine3df_lag(shp,e_mechdisp,f,fi,c,ci,detfi,i)
         end do
c     n : previous step
         do i = 1,8 ! lint
            call kine3df_lag(shp,e_mechdisp_1,f_1,fi_1,c_1,ci_1,
     &           detfi_1,i)
         end do
c     rate of f, fi, c, ci
         do i = 1,8
            do j = 1,3
               do k = 1,3
                  d_f(k,j,i) = (f(k,j,i) - f_1(k,j,i))/dt
                  d_fi(k,j,i) = (fi(k,j,i) - fi_1(k,j,i))/dt
                  d_c(k,j,i) = (c(k,j,i) - c_1(k,j,i))/dt
                  d_ci(k,j,i) = (ci(k,j,i) - ci_1(k,j,i))/dt
               end do
            end do
            d_detfi(i) = (detfi(i) - detfi_1(i))/dt
         end do

c$    form elem. matrix of d eqn
c     form k & m elem. matrix of pi in d
         call form_elem_pi_d(
     &        k_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

cc     form k & c & m elem. matrix of a in d
c         call form_elem_a_d(
c     &        k_matrix,f_vector,n,
c     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
c     &        ts,dt,t,ep,mu,sigma,
c     &        e_mechvel,e_mechacc,
c     &        f,fi,c,ci,detfi,
c     &        f_1,fi_1,c_1,ci_1,detfi_1,
c     &        d_f,d_fi,d_c,d_ci,d_detfi,
c     &        e_phi_n,e_d_phi_n,
c     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c$    form elem. matrix of h eqn
c     form k & m elem. matrix of pi in h
         call form_elem_pi_h(
     &        k_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c     form k & c & m elem. matrix of a in h
         call form_elem_a_h_init(
     &        k_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)
      end do





c$    form vector from natural b/c's %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      do nb = 1,nnbsurf
         mtlidx = ix_nbsurf(5,nb)

         ep = mat_prop(1,mtlidx)
         mu = mat_prop(2,mtlidx)
         sigma = mat_prop(3,mtlidx)
    
c$    initialize
         do i = 1,4
            nbc_vector_e(i) = 0.0d0
            do j = 1,3
               e_flux_input(j,i) = 0.0d0
            end do
            do j = 1,3
               sg2(j,i) = 0.0d0
            end do
            xsj2(i) = 0.0d0
            do j = 1,4
               do k = 1,3
                  shp2(k,j,i) = 0.0d0
               end do
            end do
         end do
    
c$    element force input
         do i = 1,4
c            if(sigma .le. 10.0d0) then
c               do j = 1,3
c                  e_flux_input(j,i) = 0.0d0
c               end do
c            else
               do j = 1,ndm
c                  e_flux_input(j,i) = mag_em*sin(w_em*t)
                  e_flux_input(j,i) = mag_em
               end do
c            endif
         end do

c$    get 2D element coord.
         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            node1 = ix_nbsurf(i,nb)
            if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
               xl2d(1,i) = x(2,node1)
               xl2d(2,i) = x(3,node1)
               e_mechdisp2d(1,i) = mechdisp(2,node1)
               e_mechdisp2d(2,i) = mechdisp(3,node1)
            elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
               xl2d(1,i) = x(3,node1)
               xl2d(2,i) = x(1,node1)
               e_mechdisp2d(1,i) = mechdisp(3,node1)
               e_mechdisp2d(2,i) = mechdisp(1,node1)
            elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
               xl2d(1,i) = x(1,node1)
               xl2d(2,i) = x(2,node1)
               e_mechdisp2d(1,i) = mechdisp(1,node1)
               e_mechdisp2d(2,i) = mechdisp(2,node1)
            endif
         end do
         
c     $    shpfunc. values @ integration points
         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg2(1,i) = ig(i)*g
            sg2(2,i) = jg(i)*g
            sg2(3,i) = 1.0d0
         end do
         do i = 1,4 ! lint
            call shp2d_4(shp2,xsj2,xl2d,sg2,2,4,i)
            xsj2(i) = xsj2(i)*sg2(3,i)
         end do
         
c     F, Fi, C, Ci
         do i = 1,4
            do j = 1,3
               do k = 1,3
                  f2(k,j,i) = 0.0d0
                  fi2(k,j,i) = 0.0d0
                  c2(k,j,i) = 0.0d0
                  ci2(k,j,i) = 0.0d0
                  f2d(k,j,i) = 0.0d0
                  fi2d(k,j,i) = 0.0d0
                  c2d(k,j,i) = 0.0d0
                  ci2d(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,lint
            detfi2(i) = 0.0d0
         end do
         
c     n+1 : current step
c     get : F
         do i = 1,4 ! lint
            call kine2d_lag(shp2,e_mechdisp2d,f2,fi2,c2,ci2,detfi2,i)
         end do

c$    rearrange shp2 on shp2d
         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            do j = 1,4
               if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
                  shp2d(1,j,i) = 0.0d0
                  shp2d(2,j,i) = shp2(1,j,i)
                  shp2d(3,j,i) = shp2(2,j,i)
                  shp2d(4,j,i) = shp2(3,j,i)
                  
               elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
                  shp2d(1,j,i) = shp2(2,j,i)
                  shp2d(2,j,i) = 0.0d0
                  shp2d(3,j,i) = shp2(1,j,i)
                  shp2d(4,j,i) = shp2(3,j,i)
                  
               elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
                  shp2d(1,j,i) = shp2(1,j,i)
                  shp2d(2,j,i) = shp2(2,j,i)
                  shp2d(3,j,i) = 0.0d0
                  shp2d(4,j,i) = shp2(3,j,i)                  
                  
               endif
            end do
         end do

c$    rearrange F
         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
                  f2d(1,1,i) = 1.0d0
                  f2d(2,2,i) = f2(1,1,i)
                  f2d(2,3,i) = f2(1,2,i)
                  f2d(3,2,i) = f2(2,1,i)
                  f2d(3,3,i) = f2(2,2,i)
c               do j = 1,3
c                  f2d(1,j,i) = f2(3,j,i)
c                  f2d(2,j,i) = f2(1,j,i)
c                  f2d(3,j,i) = f2(2,j,i)
c               end do
               
            elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
                  f2d(1,1,i) = f2(2,2,i)
                  f2d(1,3,i) = f2(2,1,i)
                  f2d(2,2,i) = 1.0d0
                  f2d(3,1,i) = f2(1,2,i)
                  f2d(3,3,i) = f2(1,1,i)
c               do j = 1,3
c                  f2d(1,j,i) = f2(2,j,i)
c                  f2d(2,j,i) = f2(3,j,i)
c                  f2d(3,j,i) = f2(1,j,i)
c               end do
               
            elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
                  f2d(1,1,i) = f2(1,1,i)
                  f2d(1,2,i) = f2(1,2,i)
                  f2d(2,1,i) = f2(2,1,i)
                  f2d(2,2,i) = f2(2,2,i)
                  f2d(3,3,i) = 1.0d0
c               do j = 1,3
c                  f2d(1,j,i) = f2(1,j,i)
c                  f2d(2,j,i) = f2(2,j,i)
c                  f2d(3,j,i) = f2(3,j,i)
c               end do
               
            endif
         end do

c     get : detfi2,Fi, C, Ci
         do i = 1,4 ! lint
            call kine2d_lag_1(shp2,e_mechdisp2d,
     &           f2d,fi2d,c2d,ci2d,detfi2,i)
         end do

c     $    form external force vector
         call natural_bc_d(
     &        nbc_vector_e,nb,
     &        ix_nbsurf,
     &        shp2d,xsj2,
     &        4,2,nnbsurf,4,
     &        ep,mu,sigma,
     &        f2d,fi2d,c2d,ci2d,detfi2,
     &        e_flux_input)
         do i = 1,4
            grow = ix_nbsurf(i,nb)
            lrow = i
            
            f_vector(grow) = f_vector(grow) + nbc_vector_e(lrow)
         end do
      end do

      end
