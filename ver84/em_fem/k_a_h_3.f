c$Id:$
      subroutine k_a_h_3(
     &     e_matrix,e_vector,
     &     shp,xsj,nel,ndm,numnp,numel,nlint,
     &     dt,ep,mu,sigma,
     &     e_mechvel,e_mechacc,
     &     f,fi,c,ci,detfi,
     &     e_phi_n,e_d_phi_n,
     &     e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

      implicit  none

      integer   ndm,nel,lint,nlint

      integer   numnp,numel

      integer   i,j,k,l
      integer   row,col

      real*8    dt,ep,mu,sigma

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)

      real*8    shp(4,nel,nlint),xsj(nlint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_matrix(3*nel,3*nel)
      real*8    e_vector(3*nel)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    xn,yn,zn,a11,a22,a33

      real*8    vel(3),tmp_vel

      real*8    as(2)

      save
      
c$    formulate element k_matrix
c     
      do lint = 1,nlint
         do i = 1,nel
            xn = shp(1,i,lint)*xsj(lint)
            yn = shp(2,i,lint)*xsj(lint)
            zn = shp(3,i,lint)*xsj(lint)

            a11 = 0.1*xn/ep
            a22 = 0.1*yn/ep
            a33 = 0.1*zn/ep
            
            row = 3*i

            do j = 1,nel
               xn = shp(1,j,lint)
               yn = shp(2,j,lint)
               zn = shp(3,j,lint)

               col = 3*j
               
!               e_matrix(row-2,col-2) = e_matrix(row-2,col-2)
!     &              -a11*xn-a11*yn-a11*zn
!     &              -a22*xn-a22*yn-a22*zn
!     &              -a33*xn-a33*yn-a33*zn
               e_matrix(row-2,col-2) = e_matrix(row-2,col-2)
     &              -a11*xn
               
               e_matrix(row-2,col-1) = e_matrix(row-2,col-1)
     &              -a11*yn
               
               e_matrix(row-2,col  ) = e_matrix(row-2,col  )
     &              -a11*zn

                              
               e_matrix(row-1,col-2) = e_matrix(row-1,col-2)
     &              -a22*xn
               
               e_matrix(row-1,col-1) = e_matrix(row-1,col-1)
     &              -a22*yn
               
               e_matrix(row-1,col  ) = e_matrix(row-1,col  )
     &              -a22*zn
               
               
               e_matrix(row  ,col-2) = e_matrix(row  ,col-2)
     &              -a33*xn
               
               e_matrix(row  ,col-1) = e_matrix(row  ,col-1)
     &              -a33*yn
                              
               e_matrix(row  ,col  ) = e_matrix(row  ,col  )
     &              -a33*zn
               
            end do
         end do
      end do
      
      end
      
