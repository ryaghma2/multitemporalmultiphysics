c$Id:$
      subroutine fem_maxwell_cm(
     &     hfield,dfield,bfield,efield,ecfield,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     ts,dt,w_me,mag_me,w_em,mag_em,
     &     x,ix,mat_prop,numnp,numel,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     ix_nbsurf,nnbsurf,
     &     sbc_pi,nsbc_pi,sbc_a,nsbc_a
     &     )

c     element data
c     nel : nodes/elem.
c     ndm : dimension
c     mat_prop : material data
c     x : global coords.
c     xl : element coords.
c     ix : element connectivity
c     shp : shp func value @ integration points
c     sg : integration points
c     n : num of element
c     lint : number of integration point

      implicit none

      integer   ts
      integer   ndm,nel,lint

      integer   numnp,numel,nsbc_pi,nsbc_a
      integer   nnbsurf

      integer   numnp2

      integer   ix(9,numel)
      integer   sbc_pi(2,nsbc_pi),sbc_a(7,nsbc_a)
      integer   ix_nbsurf(6,nnbsurf)

      integer   m1,m2,i,j

      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    hfield(3,numnp),dfield(3,numnp)
      real*8    bfield(3,numnp),efield(3,numnp),ecfield(3,numnp)

      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      real*8    phi_n(numnp),d_phi_n(numnp)

      real*8    gradphi(3,numnp)

      real*8    k_matrix(4*numnp,4*numnp),f_vector(4*numnp)

      real*8    mechvel(3,numnp),mechacc(3,numnp)
      real*8    mechdisp(3,numnp),mechdisp_1(3,numnp)

      real*8    x_ans_s(4*numnp)
      real*8    x_ans_pi(numnp),x_ans_a(3,numnp)

      save

      ndm = 3
      nel = 8
      lint = 8

c$    start problem
c     initialize k & f
      do i = 1,4*numnp
         do j = 1,4*numnp
            k_matrix(i,j) = 0.0d0
         end do
         f_vector(i) = 0.0d0
      end do

c$    construct stiffness matrix
      call form_pi_a_cm(
     &     k_matrix,f_vector,
     &     x,ix,mat_prop,nel,ndm,numnp,numel,lint,
     &     ts,dt,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     w_me,mag_me,w_em,mag_em,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     ix_nbsurf,nnbsurf
     &     )

c$    reduce matrix size for nodes to which are applied b.c.s.
      m1 = 0
      m2 = 0
      call eliminate_pi_row_col(
     &     k_matrix,f_vector,
     &     nsbc_pi,sbc_pi,
     &     numnp,
     &     m1,m2)
      call eliminate_a_row_col(
     &     k_matrix,f_vector,
     &     nsbc_pi,nsbc_a,sbc_a,
     &     numnp,
     &     m1,m2)

c$    resize amatrix and bvector
c     dof_reduced = 4*numnp-m1-m2;
c     k_matrix_s = zeros(dof_reduced);
c     f_vector_s = zeros(dof_reduced,1);
c     for i = 1:dof_reduced
c     for j = 1:dof_reduced
c     k_matrix_s(j,i) = k_matrix(j,i);
c     end
c     f_vector_s(i) = f_vector(i);
c     end

c$    solve Ax=B
c     x_ans_s = zeros(dof_reduced,1);
c     % method 1
c     %   x_ans_s = k_matrix_s\f_vector_s;
c     % method 2 : static condensation
      call solveprob(k_matrix,f_vector,x_ans_s,numnp,m1,m2)

c$    rearrange solution vectors
      call rearrange_pi_soln(x_ans_pi,x_ans_s,numnp,sbc_pi,nsbc_pi)
      call rearrange_a_soln(x_ans_a,x_ans_s,numnp,sbc_a,nsbc_a,nsbc_pi)

c$    update phi_n, d_phi_n, a_n, d_a_n, dd_a_n
      call update_phi_a_d_a_dd_a_n(
     &     x_ans_pi,x_ans_a,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     numnp,ndm,dt)

c$    h, d, b, e fields
      call get_h_d_fields(
     &     hfield,dfield,bfield,efield,ecfield,
     &     gradphi,phi_n,a_n,d_a_n,
     &     mat_prop,
     &     x,ix,
     &     numnp,numel,ndm,nel,lint,
     &     mechvel,mechdisp)

      end
