c$Id:$
      subroutine shp3d_8(shp,xsj,xl,ss,ndm,nel,lint)

      implicit none

c      include

      integer    lint,i,j,k
      integer    ndm,nel

      real*8     shp(4,8,8),xs(3,3),xsj(8),rxsj
      real*8     xl(3,8)
      real*8     ap1,am1,ap2,am2,ap3,am3,ss(4,8),c1,c2,c3,ad(3,3)
      
c      logical
      
      save

c$     8-node brick
c$     Compute shape functions and their natural coord. derivatives
      ap1 = 1.0d0 + ss(1,lint)
      am1 = 1.0d0 - ss(1,lint)
      ap2 = 1.0d0 + ss(2,lint)
      am2 = 1.0d0 - ss(2,lint)
      ap3 = 1.0d0 + ss(3,lint)
      am3 = 1.0d0 - ss(3,lint)

c$     Compute for ( - , - ) values
      c1      = 0.125d0*am1*am2
      c2      = 0.125d0*am2*am3
      c3      = 0.125d0*am1*am3
      shp(1,1,lint) = -c2
      shp(1,2,lint) =  c2
      shp(2,1,lint) = -c3
      shp(2,4,lint) =  c3
      shp(3,1,lint) = -c1
      shp(3,5,lint) =  c1
      shp(4,1,lint) =  c1*am3
      shp(4,5,lint) =  c1*ap3

c$     Compute for ( + , + ) values
      c1      = 0.125d0*ap1*ap2
      c2      = 0.125d0*ap2*ap3
      c3      = 0.125d0*ap1*ap3
      shp(1,8,lint) = -c2
      shp(1,7,lint) =  c2
      shp(2,6,lint) = -c3
      shp(2,7,lint) =  c3
      shp(3,3,lint) = -c1
      shp(3,7,lint) =  c1
      shp(4,3,lint) =  c1*am3
      shp(4,7,lint) =  c1*ap3

c$     Compute for ( - , + ) values
      c1      = 0.125d0*am1*ap2
      c2      = 0.125d0*am2*ap3
      c3      = 0.125d0*am1*ap3
      shp(1,5,lint) = -c2
      shp(1,6,lint) =  c2
      shp(2,5,lint) = -c3
      shp(2,8,lint) =  c3
      shp(3,4,lint) = -c1
      shp(3,8,lint) =  c1
      shp(4,4,lint) =  c1*am3
      shp(4,8,lint) =  c1*ap3

c$     Compute for ( + , - ) values
      c1      = 0.125d0*ap1*am2
      c2      = 0.125d0*ap2*am3
      c3      = 0.125d0*ap1*am3
      shp(1,4,lint) = -c2
      shp(1,3,lint) =  c2
      shp(2,2,lint) = -c3
      shp(2,3,lint) =  c3
      shp(3,2,lint) = -c1
      shp(3,6,lint) =  c1
      shp(4,2,lint) =  c1*am3
      shp(4,6,lint) =  c1*ap3

c$     Compute jacobian transformation
      do i = 1,3
         do j = 1,3
            xs(j,i) = 0.0d0
         end do
      end do
      
      do i = 1,3
         do j =1,3
            xs(j,i) = 0.0d0
            do k = 1,8
               xs(j,i) = xs(j,i) + xl(j,k)*shp(i,k,lint)
            end do
         end do
      end do

c$     Compute adjoint to jacobian
      ad(1,1) = xs(2,2)*xs(3,3) - xs(2,3)*xs(3,2)
      ad(1,2) = xs(3,2)*xs(1,3) - xs(3,3)*xs(1,2)
      ad(1,3) = xs(1,2)*xs(2,3) - xs(1,3)*xs(2,2)
      
      ad(2,1) = xs(2,3)*xs(3,1) - xs(2,1)*xs(3,3)
      ad(2,2) = xs(3,3)*xs(1,1) - xs(3,1)*xs(1,3)
      ad(2,3) = xs(1,3)*xs(2,1) - xs(1,1)*xs(2,3)
      
      ad(3,1) = xs(2,1)*xs(3,2) - xs(2,2)*xs(3,1)
      ad(3,2) = xs(3,1)*xs(1,2) - xs(3,2)*xs(1,1)
      ad(3,3) = xs(1,1)*xs(2,2) - xs(1,2)*xs(2,1)

c$     Compute determinant of jacobian
      xsj(lint)  = xs(1,1)*ad(1,1) + xs(1,2)*ad(2,1) + xs(1,3)*ad(3,1)
      rxsj = 1.0d0/xsj(lint)

c$     Compute jacobian inverse
      do i = 1,3
         do j = 1,3
            xs(j,i) = ad(j,i)*rxsj
         end do
      end do

c$     Compute derivatives with repect to global coords.
      do i = 1,8! nel
         c1 = shp(1,i,lint)*xs(1,1) + shp(2,i,lint)*xs(2,1)
     &      + shp(3,i,lint)*xs(3,1)
         c2 = shp(1,i,lint)*xs(1,2) + shp(2,i,lint)*xs(2,2)
     &      + shp(3,i,lint)*xs(3,2)
         c3 = shp(1,i,lint)*xs(1,3) + shp(2,i,lint)*xs(2,3)
     &      + shp(3,i,lint)*xs(3,3)
    
         shp(1,i,lint) = c1
         shp(2,i,lint) = c2
         shp(3,i,lint) = c3
      end do

      end
