c$Id:$
      subroutine eliminate_pi_row_col(
     &     amatrix,bvector,
     &     nbc,bcs,
     &     numnp,
     &     m1,m2)

c     nbc     : number of specified nodes
c     bcs     : info. of specified nodes 
c     bcs = [node num., value]
c     numnp   : number of nodes

      implicit  none

      integer   i,j,k,l,node,col

      integer   nbc,numnp,m1,m2

      integer   bcs(2,nbc)

      real*8    amatrix(4*numnp,4*numnp)
      real*8    bvector(4*numnp)

      save

c$    reduce size of k_matrix and f_vector for pi
c     reduce size of k_matrix and f_vector
      do i = 1,nbc
         node = bcs(1,i)
         do k = 1,1
c     f_vector
            do j = (node+1-m1),(4*numnp-m1)
               col = node-m1    ! column at specified d.o.f.
               bvector(j-1) = bvector(j)-amatrix(j,col)*bcs(2,i)
            end do
            do j = 1,(node-1-m1)
               col = node-m1    ! column at specified d.o.f.
               bvector(j) = bvector(j) - amatrix(j,col)*bcs(2,i)
            end do
            
c     k_matrix
            do j = (node+1-m1),(4*numnp-m1)
               do l = 1,(4*numnp-m1)
                  amatrix(l,j-1) = amatrix(l,j)
               end do
            end do
            do j = (node+1-m1),(4*numnp-m1)
               do l = 1,(4*numnp-m1-1)
                  amatrix(j-1,l) = amatrix(j,l)
               end do
            end do
            m1 = m1 + 1
c            write (*,*) "m1=",m1
         end do
      end do

      end
