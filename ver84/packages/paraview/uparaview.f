c$Id:$
      subroutine uparaview(x,ix,u,ud,sig,psig,his, el_ptr, lct,
     &                     ndm,ndf,nen1,nen,istv,hplmax, numnp,numel)

c      * * F E A P * * A Finite Element Analysis Program

c....  Copyright (c) 1984-2014: Regents of the University of California
c                               All rights reserved

c-----[--.----+----.----+----.-----------------------------------------]
c     Modification log                                Date (dd/mm/year)
c       Original version                                    12/02/2014
c-----[--.----+----.----+----.-----------------------------------------]
c      Purpose:  Interface to PARAVIEW

c      Inputs:
c         x(ndm,numnp)   - Node coordinates
c         ix(nen1,numel) - Element connections
c         u(ndf,numel)   - Node displacements
c         ud(ndf,numel,2)- Node velocity and acceleration
c         sig(numnp,*)   - Node stresses
c         psig(numnp,*)  - Node principal stresses
c         his(numnp,*)   - History variables
c         lct(*)         - Control for filename
c         ndm            - Mesh dimension
c         ndf            - DOF's/node
c         nen1           - Dimension of ix array
c         nen            - Max nodes/element
c         istv           - Number stresses/node
c         nplmax         - Number of history variables

c      Working array
c         el_ptr(numel+1) - Pointer to elements

c      Outputs:
c         To file with appender *.vtu
c-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'comfil.h'
      include  'counts.h'
      include  'iofile.h'
      include  'pointer.h'

      logical   pcomp
      character lct*15, parafile*128,parext*4
      integer   ndm,ndf,nen1,nen,istv,hplmax, numnp,numel

      integer   ix(nen1,numel), el_ptr(0:numel)
      real*8    x(ndm,numnp),u(ndf,numnp),ud(ndf,numnp,2)
      real*8    sig(numnp,*),psig(numnp,*),his(numnp,*)
      integer   i,ii,node, plu

      save

      data      plu / 99 /

c     Set extender name

      parext = 'vtu '

c     Assign name from 'lct'

      if(.not.pcomp(lct,'time',4)) then
        parafile       = '    '
        if(.not.pcomp(lct,'    ',4)) then
          parafile(1:15) = lct(1:15)
        else
          parafile(1:13) = 'feap_paraview' 
        endif

c     Assign name from 'fplt': Allows for multiple file outputs

      else
        i = index(fplt,' ')
        parafile(1:128) = ' '
        parafile(1:i-1)=fplt(1:i-1)
        parafile(i:i+4) = '00000'
        if (nstep.le.9) then
          write(parafile(i+4:i+4),'(i1)') nstep
        elseif (nstep.le.99) then
          write(parafile(i+3:i+4),'(i2)') nstep
        elseif (nstep.le.999) then
          write(parafile(i+2:i+4),'(i3)') nstep
        elseif (nstep.le.9999) then
          write(parafile(i+1:i+4),'(i4)') nstep
        elseif (nstep.le.99999) then
          write(parafile(i:i+4),'(i5)') nstep
        endif
      end if
      call addext(parafile,parext,128,4)
      open(unit=plu,file=parafile,access='sequential')
      write(*,*) 'Saving PARAVIEW data to ',parafile

c     Write out top header
      write(plu,1000)

      write(plu,1020) numnp,numel               ! Start Mesh/Piece Section
      write(plu,1010) '<Points>'                ! Start Point/Node data

      write(plu,1030) 'Float64','nodes',3
      do i = 1,numnp
        write(plu,2010) (x(ii,i),ii = 1,ndm) ,(0.0d0,ii = ndm+1,3)
      end do ! i
      write(plu,1010) '</DataArray>'            ! Close Node data

      write(plu,1010) '</Points>'               ! Close Points section

      write(plu,1010) '<Cells>'                 ! Start Cell Section
      write(plu,1030) 'Int32','connectivity',1  ! Start Elements

c     Offsets memory allocation

      el_ptr(0) = 0
      do i = 1,numel
        el_ptr(i) = el_ptr(i-1)
        do ii = 1,nen
          node = ix(ii,i)
          if (node .gt. 0) then
            write(plu,2020) node-1
            el_ptr(i) = el_ptr(i) + 1
          endif
        end do ! ii
      end do ! i

      write(plu,1010) '</DataArray>'            ! Close Elements

c     Output offsets for elements

      write(plu,1030) 'Int32','offsets',1       ! Start Offsets
      do i = 1,numel
        write(plu,2020) el_ptr(i)
      end do ! i
      write(plu,1010) '</DataArray>'            ! Close Offsets

c     Output element connectivity type

      write(plu,1030) 'UInt8','types',1         ! Start Element types
      do i = 1,numel
        if (el_ptr(i)-el_ptr(i-1)    .eq.2) then   ! 2 node line
          write(plu,2020) 3
        elseif (el_ptr(i)-el_ptr(i-1).eq.3) then   ! 3 node triangle
          write(plu,2020) 5
        elseif (el_ptr(i)-el_ptr(i-1).eq.4) then   ! 4 node quad
          write(plu,2020) 9
        elseif (el_ptr(i)-el_ptr(i-1).eq.6) then   ! 6 node triangle
          write(plu,2020) 22
        elseif (el_ptr(i)-el_ptr(i-1).eq.8.and.ndm.eq.3) then ! 8 node brick
          write(plu,2020) 12
        elseif (el_ptr(i)-el_ptr(i-1).eq.8) then   ! 8 node quad
          write(plu,2020) 23
        elseif (el_ptr(i)-el_ptr(i-1).eq.9) then   ! 9 node quad
          write(plu,2020) 23
        elseif (el_ptr(i)-el_ptr(i-1).eq.20) then  ! 20 node brick
          write(plu,2020) 25
        elseif (el_ptr(i)-el_ptr(i-1).eq.27) then  ! 27 node brick
          write(plu,2020) 25
        endif
      end do ! i

      write(plu,1010) '</DataArray>'            ! Close Element types
      write(plu,1010) '</Cells>'                     ! Close Cell Section

c     Output displacements

      write(plu,1010) '<PointData>'                  ! Start Point Data

      write(plu,1030) 'Float64','Displacements',ndf  ! Start Displacements
      do i = 1,numnp
        do ii = 1,ndf
        write(plu,2000) u(ii,i)
        end do ! ii
      end do ! i
      write(plu,1010) '</DataArray>'                 ! Close Displacements

      if(np(42).ne.0) then
        write(plu,1030) 'Float64','Velocity',ndf     ! Start Velocity
        do i = 1,numnp
          do ii = 1,ndf
          write(plu,2000) ud(ii,i,1)
          end do ! ii
        end do ! i
        write(plu,1010) '</DataArray>'               ! Close Velocity

        write(plu,1030) 'Float64','Acceleration',ndf ! Start Acceleration
        do i = 1,numnp
          do ii = 1,ndf
          write(plu,2000) ud(ii,i,2)
          end do ! ii
        end do ! i
        write(plu,1010) '</DataArray>'               ! Close Acceleration
      endif

c     Output stresses

      if(istv.gt.0) then

        write(plu,1030) 'Float64','Stress/Strain',istv ! Start Stresses
        do i = 1,numnp
          do ii =1,istv
            write(plu,2000) sig(i,ii)
          end do ! ii
        end do ! i
        write(plu,1010) '</DataArray>'               ! Close Stresses

        write(plu,1030) 'Float64','Principal Stress',7 ! Start Prin Stresses
        do i = 1,numnp
          do ii =1,7
            write(plu,2000) psig(i,ii)
          end do ! ii
        end do ! i
        write(plu,1010) '</DataArray>'               ! Close Prin Stresses
      else
        write(*,*) ' No stresses output to PARAVIEW file'
      endif

c     Output history variables

      if(hplmax.gt.0) then

        write(plu,1030) 'Float64','History',hplmax   ! Start History
        do i = 1,numnp
          do ii =1,hplmax
            write(plu,2000) his(i,ii)
          end do ! ii
        end do ! i
        write(plu,1010) '</DataArray>'               ! Close History
      else
        write(*,*) ' No history variables output to PARAVIEW file'

      endif
      write(plu,1010) '</PointData>'              ! Close Point Data Section

      write(plu,1010) '</Piece>'                  ! Close Mesh/Piece

c     Close the XML file

      write(plu,1010) '</UnstructuredGrid> </VTKFile>'
      close(plu, status = 'keep')

c     Formats

1000  format('<?xml version="1.0"?>',/
     &       '<VTKFile type="UnstructuredGrid" version="0.1">',/
     &       '<UnstructuredGrid>')

1010  format(a)

1020  format('<Piece NumberOfPoints="',i10,
     &       '" NumberOfCells="',i10,'">')

1030  format('<DataArray type="',a,'" Name="',a,
     &       '" NumberOfComponents="',i2,'" format="ascii">')

2000  format(1p,1e14.5,$)
2010  format(3(1p,1e14.5),$)
2020  format(i8,$)

      end
