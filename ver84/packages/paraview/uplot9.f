c$Id:$
      subroutine uplot9(ctl)

c      * * F E A P * * A Finite Element Analysis Program

c....  Copyright (c) 1984-2014: Regents of the University of California
c                               All rights reserved

c-----[--.----+----.----+----.-----------------------------------------]
c     Modification log                                Date (dd/mm/year)
c       Original version                                    01/11/2006
c      1. Change call palloc to setvar = palloc             14/08/2013
c      2. Rewrite using uparaview                           14/02/2014
c      3. Add outputs for history variables                 15/02/2014
c-----[--.----+----.----+----.-----------------------------------------]
c      Purpose:  Plot interface to PARAVIEW

c      Inputs:
c         ctl(3)    - Command numerical parameters

c      Outputs:
c         N.B.  Users are responsible for command actions.  See
c               programmers manual for example.
c-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'cdata.h'
      include  'eldatp.h'
      include  'evdata.h'
      include  'iofile.h'
      include  'pltfac.h'
      include  'pointer.h'
      include  'sdata.h'
      include  'strnum.h'
      include  'umac1.h'

      include  'comblk.h'

      logical   pcomp, setvar, palloc
      character lct*15
      real*8    ctl(3)

      save

c     Set command word

      if(pcomp(uct,'plt9',4)) then      ! Usual    form
        uct = 'pvie'                    ! Specify 'name'

      else                              ! Perform user operation

c       Allocate array to store element offset pointers

        setvar = palloc(111,'TEMP1',numel+1,1)

c       Call paraview output routine

        lct = 'time'

        call uparaview(hr(npxx),mr(npix),hr(npuu),hr(npud),
     &                 hr(np(58)+numnp),hr(np(57)+numnp),
     &                 hr(np(305)),mr(np(111)),
     &                 lct,ndm,ndf,nxd,nxn,istv,hplmax, numnp,nne)

c       Deallocate temporary array

        setvar = palloc(111,'TEMP1',0,1)

        endif

      end
