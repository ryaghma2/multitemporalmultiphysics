c$Id:$
      logical function ualloc(num,name,length,precis)

c      * * F E A P * * A Finite Element Analysis Program

c....  Copyright (c) 1984-2014: Regents of the University of California
c                               All rights reserved

c-----[--.----+----.----+----.-----------------------------------------]
c     Modification log                                Date (dd/mm/year)
c       Original version                                    01/11/2006
c-----[--.----+----.----+----.-----------------------------------------]
c      Purpose: Define, delete, or resize a user dictionary entry.
c               Pointer defined for integer (single) and real
c               (double precision) arrays.

c               N.B. Currently limited to 200 names by dimension of
c               common blocks 'allotd','allotn','pointer'

c      Inputs:
c         num        - Entry number for array (see below)
c         name       - Name of array          (see below)
c         length     - Length of array defined: =0 for delete
c         precis     - Precision of array: 1 = integers; 2 = reals

c      Output:
c         up(num)    - Pointer to first word of array in blank common
c-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'allotd.h'

      logical   usetmem
      character name*(*)
      integer   i, num,length,precis

c     Storage definitions for UALLOC variables

      integer   list
      parameter (list = 100)

      character names(list)*5

      save

c     Define and store names

      data   (names(i),i=1,list)/

     &     'VAR00','VAR01','VAR02','VAR03','VAR04',
     &     'VAR05','VAR06','VAR07','VAR08','VAR09',

     &     'VAR10','VAR11','VAR12','VAR13','VAR14',
     &     'VAR15','VAR16','VAR17','VAR18','VAR19',! cm data

     &     'VAR20','VAR21','VAR22','VAR23','VAR24',
     &     'VAR25','VAR26','VAR27','VAR28','VAR29',

     &     'VAR30','VAR31','VAR32','VAR33','VAR34',
     &     'VAR35','VAR36','VAR37','VAR38','VAR39',! fs data

     &     'VAR40','VAR41','VAR42','VAR43', ! intfcix
     &     'VAR44',! cm data
     &     'VAR45','VAR46', ! cm + fs data for remesh

     &     'VAR47','VAR48','VAR49','VAR50','VAR51',
     &     'VAR52',! for interpolating variables

     &     'VAR53','VAR54','VAR55','VAR56','VAR57',
     &     'VAR58','VAR59','VAR60','VAR61','VAR62',
     &     'VAR63','VAR64',! for remesh

     &     'VAR65','VAR66','VAR67','VAR68','VAR69',

     &     'VAR70','VAR71','VAR72','VAR73','VAR74',
     &     'VAR75','VAR76','VAR77','VAR78','VAR79',

     &     'VAR80','VAR81','VAR82','VAR83','VAR84',
     &     'VAR85','VAR86','VAR87','VAR88','VAR89',

     &     'VAR90','VAR91','VAR92','VAR93','VAR94',
     &     'VAR95','VAR96','VAR97','VAR98','VAR99'/

c     &         'DUMMY'/

c     Short description of variables

c              'DUMMY',     !     1: Start here with user defined names

c     Do memory management operations

      ualloc = usetmem(list,names,num,name,length,precis)

      end
