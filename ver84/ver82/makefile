
# Set number of processors
  NPROC     = 4                                     

# Set path options for executable
  FEAPRUN   = $(FEAPHOME8_2)/parfeap/feap

# FEAPRUN   = valgrind -v --leak-check=full --show-reachable=yes $(FEAPHOME8_2)/parfeap/feap

# arpack library
  ARPACKLIB = $(FEAPHOME8_2)/packages/arpack/archive/arpacklib.a

# parpack library
  PARPACKLIB = $(FEAPHOME8_2)/parfeap/packages/arpack/parpacklib.a

# Local lapack library
# LAPACKLIB = $(FEAPHOME8_2)/packages/lapack/lapacklib.a

# Local blas library
# BLASLIB   = $(FEAPHOME8_2)/packages/blas/blaslib.a    

# FEAP make directives
  include $(FEAPHOME8_2)/makefile.in

# PETSc include files
  include ${PETSC_DIR}/bmake/common/base
  PETSC_INCLUDE := $(PETSC_INCLUDE) -I$(FINCLUDE)    # N.B. := avoids recursion

OBJECTS = $(FEAPHOME8_2)/main/feap82.o \
	adomnam.o filargs.o fparsop.o parbmat.o pcompress.o \
	pdomain.o pfeapmi.o pfeapsr.o pform.o parstop.o parmacio.o \
	pmacr7.o unix/pmetis.o pmodify.o ppmodin.o pparlo.o \
	pndata.o prwext.o psetb.o psproja.o psprojb.o pstart.o \
	psubsp.o scalev.o smodify.o uasble.o uasblem.o upremas.o \
	usolve.o upc.o pextnd.o tinput.o pprint.o connect_em_pa.o \
	cm_data_pa.o initialize_array.o transferdata_cm.o BC_pa.o \
	connectivity_pa.o connect_maxwell_pa.o testwrite.o assconne.o \
	fem_maxwell_3d_pa.o fem_maxwell_cm_init_pa.o form_pi_a_cm_pain.o \
	form_elem_a_h_init.o form_elem_pi_hpa.o form_elem_pi_dpa.o \
	natural_bc_d.o form_elem_a_dpa.o fem_maxwell_cm_pa.o \
	form_elem_a_hpa.o form_pi_a_cm_pa.o

install: $(OBJECTS) $(ARFEAP)
	ranlib $(ARFEAP)
	-${FLINKER} ${FFLAGS} -o feap $(OBJECTS) \
	$(ARPACKLIB) $(PARPACKLIB) $(ARFEAP) \
	${PETSC_FORTRAN_LIB} ${PETSC_LIB} $(LDOPTIONS)

checkout:
	co -q RCS/*.h,v
	co -q RCS/*.f,v
	co -q RCS/*.F,v

rcs:
	mkdir RCS
	ci -t-"" *.h
	ci -t-"" *.f
	ci -t-"" *.F
	ci -t-"" makefile

clean:
	rcsclean -q
	rm -f *.o

feaprun:
	-@${MPIEXEC} -s all -np $(NPROC) $(FEAPRUN) -ksp_type cg -ksp_monitor -pc_type jacobi -log_summary -on_error_attach_debugger -ksp_view -options_left

feaprun-gmres:
	-@${MPIEXEC} -s all -np $(NPROC) $(FEAPRUN) -ksp_type gmres -ksp_monitor -pc_type bjacobi -log_summary -on_error_attach_debugger

feaprun-mg:
	-@${MPIEXEC} -s all -np $(NPROC) $(FEAPRUN) -ksp_type cg -ksp_monitor -log_summary -out_verbose 2 -pc_type prometheus -pc_mg_type multiplicative -pc_mg_cycles 1-prometheus_preduce_base 500 -prometheus_top_grid_limit 2500 -aggmg_avoid_resmooth -aggmg_smooths 1  -prometheus_repartition -options_left

.f.o:
	$(FF) -c $(FFOPTFLAG) -I$(FINCLUDE) $*.f -o $*.o
#
#.c.o:
#	$(CC) -c $(CCOPTFLAG)  $*.c -o $*.o

#FEAPRUN   = valgrind -v --leak-check=full --show-reachable=yes $(FEAPHOME8_2)/parfeap/feap               # set path for executable

