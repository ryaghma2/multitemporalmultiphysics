c$Id:$
      subroutine BC_pa ( linp_bc, nphi,
     &     na, nnbsurf,ix,
     &     sbc_pi,sbc_a,
     &     ix_nsurf,x,geqix,nnbsurfa,
     &     ix_nsurfa)

      implicit  none

#     include   "include/finclude/petsc.h"
#     include   "pfeapa.h"
#     include   "pfeapb.h"
#     include   "pointer.h"
#     include   "comblk.h"
#     include   "comfil.h"
#     include   "sdata.h"
#     include   "cdata.h"

      integer linp_bc, nphi, na, nnbsurf,geqix(numnp)
      integer i,n,j,k,node,nod,m
      integer flag
      real*8  x(3,numnp)
      integer  ix(nen1,*)
      real*8  sbc_pi(2,*), sbc_a(2,*)
      integer  ix_nsurf(6,*)
      integer  nnbsurfa,ix_nsurfa(6,*)
      character*128  dummy !,bcname

      n = 0
!      do i = 1,numel
!            if ( (x(3,ix(1,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(2,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(3,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(1,i)
!                  ix_nsurf(2,n) = ix(4,i)
!                  ix_nsurf(3,n) = ix(3,i)
!                  ix_nsurf(4,n) = ix(2,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!            else if ( (x(3,ix(1,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(2,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(5,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(1,i)
!                  ix_nsurf(2,n) = ix(2,i)
!                  ix_nsurf(3,n) = ix(6,i)
!                  ix_nsurf(4,n) = ix(5,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!            else if ( (x(3,ix(1,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(4,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(5,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(1,i)
!                  ix_nsurf(2,n) = ix(5,i)
!                  ix_nsurf(3,n) = ix(8,i)
!                  ix_nsurf(4,n) = ix(4,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!            else if ( (x(3,ix(2,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(3,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(6,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(2,i)
!                  ix_nsurf(2,n) = ix(3,i)
!                  ix_nsurf(3,n) = ix(7,i)
!                  ix_nsurf(4,n) = ix(6,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!            else if ( (x(3,ix(3,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(4,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(7,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(3,i)
!                  ix_nsurf(2,n) = ix(4,i)
!                  ix_nsurf(3,n) = ix(8,i)
!                  ix_nsurf(4,n) = ix(7,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!            else if ( (x(3,ix(5,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(6,i)) .eq. 0.0d0) .and.
!     &           (x(3,ix(7,i)) .eq. 0.0d0) ) then
!               n = n + 1 
!                  ix_nsurf(1,n) = ix(5,i)
!                  ix_nsurf(2,n) = ix(6,i)
!                  ix_nsurf(3,n) = ix(7,i)
!                  ix_nsurf(4,n) = ix(8,i)
!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!               ix_nsurf(6,n) = -3 ! normal direc.
!             end if
!      end do
!!      do i = 1,numel
!!            if ( (x(2,ix(1,i)) .eq. 0.0d0) .and.
!!     &           (x(2,ix(2,i)) .eq. 0.0d0) .and.
!!     &           (x(2,ix(3,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(1,i)
!!                  ix_nsurf(2,n) = ix(4,i)
!!                  ix_nsurf(3,n) = ix(3,i)
!!                  ix_nsurf(4,n) = ix(2,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!            else if ( (x(2,ix(1,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(2,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(5,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(1,i)
!!                  ix_nsurf(2,n) = ix(2,i)
!!                  ix_nsurf(3,n) = ix(6,i)
!!                  ix_nsurf(4,n) = ix(5,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!            else if ( (x(2,ix(1,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(4,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(5,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(1,i)
!!                  ix_nsurf(2,n) = ix(5,i)
!!                  ix_nsurf(3,n) = ix(8,i)
!!                  ix_nsurf(4,n) = ix(4,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!            else if ( (x(2,ix(2,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(3,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(6,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(2,i)
!!                  ix_nsurf(2,n) = ix(3,i)
!!                  ix_nsurf(3,n) = ix(7,i)
!!                  ix_nsurf(4,n) = ix(6,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!            else if ( (x(2,ix(3,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(4,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(7,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(3,i)
!!                  ix_nsurf(2,n) = ix(4,i)
!!                  ix_nsurf(3,n) = ix(8,i)
!!                  ix_nsurf(4,n) = ix(7,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!            else if ( (x(2,ix(5,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(6,i)) .eq. 0.0d0) .and.
!!     &                (x(2,ix(7,i)) .eq. 0.0d0) ) then
!!               n = n + 1 
!!                  ix_nsurf(1,n) = ix(5,i)
!!                  ix_nsurf(2,n) = ix(6,i)
!!                  ix_nsurf(3,n) = ix(7,i)
!!                  ix_nsurf(4,n) = ix(8,i)
!!               ix_nsurf(5,n) = ix(16,i) ! mat'l index
!!               ix_nsurf(6,n) = -2 ! normal direc.
!!             end if
!!      end do
!!      nnbsurf = n ! number of nat.boundary surfaces/ele

      n = 0
      do i = 1,numel
            node = ix(1,i)
            if(x(2,node) .eq. 0.0d0) then
               n = n + 1 
                  ix_nsurf(1,n) = ix(1,i)
                  ix_nsurf(2,n) = ix(5,i)
                  ix_nsurf(3,n) = ix(6,i)
                  ix_nsurf(4,n) = ix(2,i)
               ix_nsurf(5,n) = ix(16,i) ! mat'l index
               ix_nsurf(6,n) = -2 ! normal direc.
            endif
      end do
      nnbsurf = n ! number of nat.boundary surfaces/ele

!=============================Neumann of A==================
      n = 0
!      do i = 1,numel
!            node = ix(1,i)
!            if(x(2,node) .eq. 0.0d0) then
!               n = n + 1 
!                  ix_nsurfa(1,n) = ix(1,i)
!                  ix_nsurfa(2,n) = ix(5,i)
!                  ix_nsurfa(3,n) = ix(6,i)
!                  ix_nsurfa(4,n) = ix(2,i)
!               ix_nsurfa(5,n) = 2 ! mat'l index
!               ix_nsurfa(6,n) = -2 ! normal direc.
!             else if(x(2,node) .eq. 0.25d0) then
!               n = n + 1 
!                  ix_nsurfa(1,n) = ix(3,i)
!                  ix_nsurfa(2,n) = ix(7,i)
!                  ix_nsurfa(3,n) = ix(8,i)
!                  ix_nsurfa(4,n) = ix(4,i)
!               ix_nsurfa(5,n) = 2 ! mat'l index
!               ix_nsurfa(6,n) = 2 ! normal direc.
!            endif
!      end do
      nnbsurfa = n ! number of nat.boundary surfaces/ele
!============Kuldeep Collabration=============================
!      n = 0
!      do i = 1,numnp
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) = 0
!      end do
!      nphi = n
!
!      n = 1
!      do i = 1,numnp
!        if(x(3,i) .eq. 0.0d0) then
!               sbc_a(1,n) = 4*(geqix(i)-1)+2
!               sbc_a(2,n) = 0.0d0
!               n = n + 1
!               sbc_a(1,n) = 4*(geqix(i)-1)+3
!               sbc_a(2,n) = 0.0d0
!               n = n + 1
!               sbc_a(1,n) = 4*(geqix(i)-1)+4
!               sbc_a(2,n) = 0.0d0
!               n = n + 1
!             else 
!               sbc_a(1,n) = 4*(geqix(i)-1)+2
!               sbc_a(2,n) = 0.0d0
!               n = n + 1
!               sbc_a(1,n) = 4*(geqix(i)-1)+3
!               sbc_a(2,n) = 0.0d0
!               n = n + 1
!         endif
!      end do
!      na=n-1
!      do i = 1,numnp
!        if((x(3,i) .eq. 0.06d0) .and.
!     & (x(1,i) .eq. 0.762d0) .and.
!     & (x(2,i) .eq. 0.762d0)) then
!               sbc_a(1,na+1) = 4*(geqix(i)-1)+4
!               sbc_a(2,na+1) = 1
!      na=na+1
!         endif
!      end do
! 
!
!
!
!=============================================================
!
!      n = 0
!      do i = 1,numnp
!        if(x(2,i) .eq. 0.25) then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) = 0
!         endif
!      end do
!      nphi = n
!
!      n = 1
!      do i = 1,numnp
!            if(
!     &           ( x(1,i) .eq. 0.0d0 ) 
!     &           .or.
!     &           ( x(1,i) .eq. 0.05d0 )
!     &           .or.
!     &           ( x(3,i) .eq. 0.0d0 )
!     &           .or.
!     &           ( x(3,i) .eq. 0.03d0 )
!     &           ) then
!                  sbc_a(1,n) = 4*(geqix(i)-1)+2
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+3
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+4
!                  sbc_a(2,n) = 0
!                  n = n + 1
!              endif
!      end do
!      na=n-1
!      end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!For PBC!!!!!!!!!!!!!!
!      n = 0
!      do i = 1,numnp
!        if((x(1,i) .eq. 0.0d0) .and. 
!     &       (x(2,i) .eq. 0.0d0) .and.
!     &       (x(3,i) .eq. 0.0d0))then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!              sbc_pi(2,n) = 1.0d0
!         else if ((x(1,i) .eq. 0.001d0) .and.
!     &           (x(2,i) .eq. 0.0d0) .and.
!     &           (x(3,i) .eq. 0.0d0))then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) = 0.0d0
!!         else if ((x(1,i) .eq. 0.0d0) .and.
!!!     &           (x(2,i) .eq. 0.001d0) .and.
!!     &           (x(3,i) .eq. 0.001d0))then
!!               n = n + 1
!!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!!               sbc_pi(2,n) = 0.0d0
!!         else if ((x(1,i) .eq. 0.0d0) .and.
!!!     &           (x(2,i) .eq. 0.001d0) .and.
!!     &           (x(3,i) .eq. 0.0d0))then
!!               n = n + 1
!!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!!               sbc_pi(2,n) = 0.0d0
!!         else if (x(1,i) .eq. 0.001d0) then
!!!     &           (x(2,i) .eq. 0.001d0) .and.
!!!     &           (x(2,i) .eq. 0.001d0))then
!!               n = n + 1
!!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!!               sbc_pi(2,n) = 1.0d0
!!!         else if ((x(1,i) .eq. 0.001d0) .and.
!!!!     &           (x(2,i) .eq. 0.001d0) .and.
!!!     &           (x(2,i) .eq. 0.0d0))then
!!!               n = n + 1
!!!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!!!               sbc_pi(2,n) = 1.0d0
!       end if
!      end do
!      nphi = n  
!!!!!!!!-------------z-dir flux BC  unit cell-----------!!
      n = 0
      do i = 1,numnp
        if (x(2,i) .eq. 0.25d0) then
               n = n + 1
               sbc_pi(1,n) = 4*(geqix(i)-1)+1
               sbc_pi(2,n) = 0.0d0
!        else if (x(1,i) .eq. 0.0d0) then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) = 0.0d0
         end if
      end do
      nphi = n

      n = 1
      do i = 1,numnp
!        if((x(2,i) .ne. 0.25d0) 
!     &   .and. (x(2,i) .ne. 0.0d0)) then

        if((x(1,i) .eq. 0.0d0) 
     &   .or. (x(1,i) .eq. 0.05d0)
     &   .or. (x(3,i) .eq. 0.0d0)
     &   .or. (x(3,i) .eq. 0.03d0)) then
              sbc_a(1,n) = 4*(geqix(i)-1)+2
              sbc_a(2,n) = 0
              n = n + 1
              sbc_a(1,n) = 4*(geqix(i)-1)+3
              sbc_a(2,n) = 0
              n = n + 1
              sbc_a(1,n) = 4*(geqix(i)-1)+4
              sbc_a(2,n) = 0
              n = n + 1
         end if
!        end if
      end do
      na = n-1
!!!!!!!!-------------x-dir potential static unit cell-----------!!
!      n = 0
!      do i = 1,numnp
!        if(x(1,i) .eq. 0.0d0) then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) =1.0e-3!         else if (x(1,i) .eq. 0.001d0) then
!               n = n + 1
!               sbc_pi(1,n) = 4*(geqix(i)-1)+1
!               sbc_pi(2,n) = 1.0d0
!         end if
!      end do
!      nphi = n
!
!      n = 1
!      do i = 1,numnp
!              sbc_a(1,n) = 4*(geqix(i)-1)+2
!              sbc_a(2,n) = 0
!              n = n + 1
!              sbc_a(1,n) = 4*(geqix(i)-1)+3
!              sbc_a(2,n) = 0
!              n = n + 1
!              sbc_a(1,n) = 4*(geqix(i)-1)+4
!              sbc_a(2,n) = 0
!              n = n + 1
!      end do
!      na = n-1
!!!!!!!!-------------VM167 BC-----------!!
!             if(
!     &           ( x(2,i) .eq. 0.0d0 )
!     &           ) then
!                  sbc_a(1,n) = 4*(geqix(i)-1)+2
!                  sbc_a(2,n) = 2.0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+3
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+4
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                else if (
!     &           ( x(2,i) .eq. 0.0d0 )
!     &             ) then
!                  sbc_a(1,n) = 4*(geqix(i)-1)+2
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+3
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+4
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                else
!                  sbc_a(1,n) = 4*(geqix(i)-1)+3
!                  sbc_a(2,n) = 0
!                  n = n + 1
!                  sbc_a(1,n) = 4*(geqix(i)-1)+4
!                  sbc_a(2,n) = 0
!                  n = n + 1
!              endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      end
