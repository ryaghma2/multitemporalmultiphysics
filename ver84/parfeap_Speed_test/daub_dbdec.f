c$Id:$
      subroutine daub_dbdec(coeff,f,nres,ncoeff,type)

      implicit  none

	 integer :: n,nres,ncoeff,nprev,norig
	 integer :: i,j

	 character :: type*20

	 real*8  :: coeff(ncoeff)
	 real*8  :: f(ncoeff),ftmp(ncoeff)
	 real*8  :: dbmat_dec(ncoeff,ncoeff)
	 real*8  :: ctmp(ncoeff)

	 n=ncoeff
	 norig=ncoeff
	 coeff=0.d0

	 if (nres .lt. 3) then
		nres=3
	 endif

	 ftmp=f
	 do while ((log(real(n))/log(2.)) .ge. real(nres))

		call init_dbdec(dbmat_dec,norig,n,type)
		ctmp=matmul(dbmat_dec,ftmp)
		nprev=n
		n=real(n)/2
		do i=1,n
		   ftmp(i)=ctmp(2*i-1)
		enddo
		coeff(1:n)=ftmp(1:n)
		do i=1,n
		   coeff(i+n)=ctmp(2*i)
		enddo

	 enddo





      end subroutine daub_dbdec
