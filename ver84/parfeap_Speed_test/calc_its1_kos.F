       subroutine calc_its1_kos(
     &		ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		yt0e,nc,dn,yt0,y0p,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)

      implicit  none

#     include   "finclude/petsc.h"
#     include   "pfeapb.h"
#     include   "time_flag.h"
#     include   "pointer.h"
#     include   "comfil.h"
#     include   "comblk.h"
#     include   "setups.h"

	integer :: ts
	integer :: nel,lint,ndm
	integer :: numnp,numel
	integer :: nsbc_pi,nsbc_a
	integer :: nnbsurf,nnbsurfa
	integer :: posp,posq! specific BC
 	integer :: ix(17,*)
	integer :: ix_nbsurf(6,nnbsurf),ix_nbsurfa(6,nnbsurfa)
	integer :: nphi,na
	integer :: i,j,k
	integer :: n,node,nb,mtlidx,ndirec,node1
	integer :: eqn,lnode
	integer :: ig(4),jg(4)
	integer :: grow,lrow
	real*8  :: dt,dtm
	real*8  :: mag_em,frq_em,w_em,mag_me,frq_me,w_me
	real*8  :: sbc_pi(2,nsbc_pi),sbc_a(2,nsbc_a)
	real*8  :: t
	real*8  :: ep,mu,sigma,g
	real*8  :: mat_prop(3,2)
	real*8  :: x(3,numnp)
	real*8  :: ans_a(3,numnp)   
	real*8  :: a_n_1(3,numnp),a_n(3,numnp)
	real*8  :: d_a_n(3,numnp),dd_a_n(3,numnp)
	real*8  :: ans_phi(numnp)
	real*8  :: phi_n(numnp),d_phi_n(numnp)
	real*8  :: fmat(32,32),fmatc(32,32),fmato(32,32)
	real*8  :: gmatc(32,32),gmato(32,32)
	real*8  :: f_vector(32)
	real*8  :: k_matrix(32,32),c_matrix(32,32),m_matrix(32,32)
	real*8  :: yn(32),ynf(32),yng(32)
	real*8  :: mechvel(3,numnp),mechacc(3,numnp)
	real*8  :: mechdisp(3,numnp),mechdisp_1(3,numnp)
	real*8  :: e_flux_input(ndm,4)
	real*8  :: e_flux_input_d(ndm,4),e_flux_input_h(ndm,4)
	real*8  :: sg(4,8),xl(ndm,8)
	real*8  :: shp(4,8,8),xsj(8)
	real*8  :: e_a_n_1(ndm,8),e_a_n(ndm,8)
	real*8  :: e_d_a_n(ndm,8),e_dd_a_n(ndm,8)
	real*8  :: e_phi_n(8),e_d_phi_n(8)
	real*8  :: e_mechvel(ndm,8),e_mechacc(ndm,8)
	real*8  :: e_mechdisp(ndm,8),e_mechdisp_1(ndm,8)
	real*8  :: nbc_vector_e(4)
	real*8  :: nbc_vector_e_d(4),nbc_vector_e_h(3*4)
	real*8  :: sg2(3,4),xl2d(2,4),shp2(3,4,4),xsj2(4)
	real*8  :: shp2d(4,4,4)
	real*8  :: f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
	real*8  :: detfi(8)
	real*8  :: f_1(3,3,8),fi_1(3,3,8),c_1(3,3,8),ci_1(3,3,8)
	real*8  :: detfi_1(8)
	real*8  :: d_f(3,3,8),d_fi(3,3,8),d_c(3,3,8),d_ci(3,3,8)
	real*8  :: d_detfi(8)
	real*8  :: f2(3,3,4),fi2(3,3,4),c2(3,3,4),ci2(3,3,4)
	real*8  :: f2d(3,3,4),fi2d(3,3,4),c2d(3,3,4),ci2d(3,3,4)
	real*8  :: detfi2(4)
	real*8  :: e_mechdisp2d(2,4)
	real*8  :: tmax
	integer :: gnodeix(8,numel), geqnix(8,numel)
	integer :: geqix(numnp)
	integer :: m,p,q
	PetscScalar :: valp,valq
	PetscScalar valnbc
	real*8  ::   val,vala
	PetscScalar :: p_n_t1(1)
	real*8  ::   p_n_t2(numnp)
	PetscScalar fvec_PS(1)
	real*8  ::   fvec_vPS(4*numtn) 
	real*8  ::   phia(4*numtn) 
	PetscScalar :: a_n_t1(1)
	PetscScalar :: a_n_t3(1)
	PetscScalar :: a_n_t5(1)
	real*8  :: a_n_t2(numnp) 
	real*8  :: a_n_t4(numnp) 
	real*8  :: a_n_t6(numnp) 
	integer :: ldem(32,5)
	real*8  :: ub(32) 
	real*8  :: disp_pre(3,numnp),disp_1_pre(3,numnp)
	real*8  :: vel_pre(3,numnp),acc_pre(3,numnp)
	real*8  :: disp_cur(3,numnp),disp_1_cur(3,numnp)
	real*8  :: vel_cur(3,numnp),acc_cur(3,numnp)
	real*8  :: dbdec(ndiv,ndiv),jind
	real*8  :: dbrec(ndiv,ndiv),dmatint(ndiv,ndiv)
	real*8  :: dbreci(ndiv,ndiv),dmatinti(ndiv,ndiv)
	real*8  :: dbrecr(ndiv,nwav),dmatintr(ndiv,nwav)
	real*8  :: dbdecr(nwav,ndiv)
	integer :: dbind(nwav),ndiv,icyc,nwav,tsj
	real*8  :: disp(3*ndiv,numnp),disp_1(3*ndiv,numnp)
	real*8  :: vel(3*ndiv,numnp),acc(3*ndiv,numnp)
	real*8  :: yt0e(8*numnp)
	real*8  :: y0p(8*numnp),y0c2(8*numnp)
	real*8  :: yt0(8*numnp),y0c1(8*numnp),ytmp(8*numnp)
	real*8  :: ym3i(8*numnp,4),ym2i(8*numnp,4)
	real*8  :: ym1i(8*numnp,4),ym0i(8*numnp,4)
	real*8  :: ym3t(8*numnp,4),ym2t(8*numnp,4)
	real*8  :: ym1t(8*numnp,4),ym0t(8*numnp,4)
	real*8  :: ytt(8*numnp)
	integer :: nc,dn,dni,nnp
	real*8  :: d_phi_n_1(numnp)
	real*8  :: tf,tpe,tpu
	logical :: setvar, palloc
	integer :: pmaxr, pmaxs, mi(4), nm, indm
	integer :: ndf
	PetscViewer  ::  K_view, B_view, K_viewcon
	PetscErrorCode  ::   ierr
	PetscOffset  :: i_x
	PetscOffset  :: i_y
	PC   :: pcem
	Mat  :: xmatgts
	Vec  :: grhs,gsol	  
	KSP  :: kspgsol
	PetscScalar  :: tol

!!!!!!!!!!!!    post processing        !!!!!!!!!!!!!!!!!!!
	real*8 ::  gradphi(3,numnp)
	real*8 ::  velb(3,numnp),etilda(3,numnp)
	real*8 ::  veld(3,numnp),h_tmp(3,numnp)
	real*8 ::  fivel(3,numnp)
	real*8 ::  df(3,numnp),bf(3,numnp),ef(3,numnp),hf(3,numnp)


	save

	data   ig/-1,1,1,-1/,jg/-1,-1,1,1/

	nel=8
	ndm=3
	lint=8
	nnp=numnp
	nphi=nsbc_pi
	na=nsbc_a


	do dni=1,dn-1


	call MatCreate(PETSC_COMM_WORLD,xmatgts,ierr)
	call MatSetSizes(xmatgts,4*numpn,4*numpn,PETSC_DETERMINE,
     &                        PETSC_DETERMINE,ierr)
	call MatSetBlockSize(xmatgts,4,ierr)
	call MatSetFromOptions(xmatgts, ierr)

	call MatSetType  (xmatgts,MATBAIJ,ierr)

	call MatMPIBAIJSetPreallocation(xmatgts,4,
     &   PETSC_NULL_INTEGER,mr(np(246)),
     &   PETSC_NULL_INTEGER,mr(np(247)),ierr)





	jind=int(1.d0*ndiv/2.d0)
	tsj=ts-(dn-dni+1)*ndiv+(jind-1)

	do i=1,nnp
	do k=1,3
		tf=((dni-1)*ndiv+(jind-1)+1)
     &		*1.d0/(dn*ndiv)
		mechdisp(k,i)=tf*(disp_cur(k,i) - 
     &		 disp_pre(k,i)) + disp_pre(k,i)
		mechdisp_1(k,i)=tf*(disp_1_cur(k,i) - 
     &		 disp_1_pre(k,i)) + disp_1_pre(k,i)
		mechvel(k,i)=tf*(vel_cur(k,i) - 
     &		 vel_pre(k,i)) + vel_pre(k,i)
		mechacc(k,i)=tf*(acc_cur(k,i) - 
     &		 acc_pre(k,i)) + acc_pre(k,i)
	end do
	end do

	do jind=1,nc

		tsj=ts-(dn-dni+1)*ndiv+(jind-1)*int(ndiv*1.d0/nc)

		if (tsj.eq.(ts-dn*ndiv)) then
		do i=1,numnp
			phi_n(i)=yt0e(8*i-7)
			d_phi_n(i)=yt0e(8*i-6)
			a_n(1,i)=yt0e(8*i-5)
			d_a_n(1,i)=yt0e(8*i-4)
			a_n(2,i)=yt0e(8*i-3)
			d_a_n(2,i)=yt0e(8*i-2)
			a_n(3,i)=yt0e(8*i-1)
			d_a_n(3,i)=yt0e(8*i)
			a_n_1(1,i)=yt0e(8*i-5)-dt*yt0e(8*i-4)
			a_n_1(2,i)=yt0e(8*i-3)-dt*yt0e(8*i-2)
			a_n_1(3,i)=yt0e(8*i-1)-dt*yt0e(8*i-0)
		enddo
		endif

		do i=1,numnp
			d_phi_n_1(i)=d_phi_n(i)
		enddo

		tsj=tsj+int(ndiv*1.d0/nc)
		t = dt*tsj
		tmax=2*3.14157/w_me*10.d0

	call VecCreate        (PETSC_COMM_WORLD,grhs,ierr)
	call VecSetSizes      (grhs, 4*numpn,PETSC_DECIDE,ierr)
	call VecSetFromOptions(grhs,ierr)
        
	call VecCreate        (PETSC_COMM_WORLD,gsol,ierr)
	call VecSetSizes      (gsol,4*numpn,PETSC_DECIDE,ierr)
	call VecSetFromOptions(gsol,ierr)

	do n = 1,numel
		ep = mat_prop(1,2)
		mu = mat_prop(2,2)
		sigma = mat_prop(3,2)

		fmatc=0.d0
		fmato=0.d0
		gmatc=0.d0
		gmato=0.d0
		ynf=0.d0
		yng=0.d0
		k_matrix=0.d0
		c_matrix=0.d0
		m_matrix=0.d0
		f_vector = 0.0d0
		ub = 0.0d0

		do i = 1,nel
			eqn = geqnix(i,n)
			node = gnodeix(i,n) !global nodes
			lnode = ix(i,n) !local nodes
			do j = 1,3
				xl(j,i) = x(j,lnode)
            
				e_a_n_1(j,i) = a_n_1(j,lnode)
				e_a_n(j,i) = a_n(j,lnode)
				e_d_a_n(j,i) = d_a_n(j,lnode)
				e_dd_a_n(j,i) = dd_a_n(j,lnode)
               
				e_mechvel(j,i) = mechvel(j,lnode)
				e_mechacc(j,i) = mechacc(j,lnode)
				e_mechdisp(j,i) = mechdisp(j,lnode)
				e_mechdisp_1(j,i) = mechdisp_1(j,lnode)
			enddo
			e_phi_n(i) = phi_n(lnode)
			e_d_phi_n(i) = d_phi_n(lnode)
		enddo
    
		g = 1.0d0/((3.0d0)**(0.5d0))
		do i = 1,4
			sg(1,i) = ig(i)*g
			sg(1,i+4) = sg(1,i)
			sg(2,i) = jg(i)*g
			sg(2,i+4) = sg(2,i)
			sg(3,i) = -g
			sg(3,i+4) = g
			sg(4,i) = 1.0d0
			sg(4,i+4) = 1.0d0
		enddo

		do i = 1,lint
			call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
			xsj(i) = xsj(i)*sg(4,i)
		enddo

		f  = 0.0d0
		fi = 0.0d0
		c  = 0.0d0
		ci = 0.0d0

		f_1  = 0.0d0
		fi_1 = 0.0d0
		c_1  = 0.0d0
		ci_1 = 0.0d0

		d_f  = 0.0d0
		d_fi = 0.0d0
		d_c  = 0.0d0
		d_ci = 0.0d0
         
		detfi   = 0.0d0
		detfi_1 = 0.0d0
		d_detfi = 0.0d0
         
		do i = 1,8
	call kine3df_lag(shp,e_mechdisp,f,fi,c,ci,detfi,i)
	call kine3df_lag(shp,e_mechdisp_1,f_1,fi_1,c_1,ci_1,
     &       detfi_1,i)
		enddo

		do i = 1,8
		do j = 1,3
		do k = 1,3
			d_f(k,j,i) = (f(k,j,i) - f_1(k,j,i))/dt
			d_fi(k,j,i) = (fi(k,j,i) - fi_1(k,j,i))/dt
			d_c(k,j,i) = (c(k,j,i) - c_1(k,j,i))/dt
			d_ci(k,j,i) = (ci(k,j,i) - ci_1(k,j,i))/dt
		enddo
		enddo
			d_detfi(i) = (detfi(i) - detfi_1(i))/dt
		enddo

	dtm=dt*int(ndiv*1.d0/nc)

		if ((jind.eq.1)) then

		call get_fmatc_rypa(
     &		fmatc,shp,xsj,nel,ndm,lint,
     &		dtm,t,ep,ci,detfi)

		call get_fmato_rypa(
     &		fmato,ynf,shp,xsj,nel,ndm,lint,
     &		dtm,t,ep,e_mechvel,fi,ci,detfi,e_a_n)

		call get_gmatc_rypa(
     &		gmatc,yng,shp,xsj,nel,ndm,lint,
     &		dtm,t,ep,mu,sigma,e_mechvel,e_mechacc,
     &		fi,c,ci,detfi,d_fi,d_ci,d_detfi,
     &		e_a_n_1,e_a_n)

		call get_gmato_rypa(
     &		gmato,yng,shp,xsj,nel,ndm,lint,
     &		dtm,t,ep,sigma,e_mechvel,
     &		fi,ci,detfi,d_ci,d_detfi,e_phi_n)

		do i=1,4*nel
		do j=1,4*nel
		if (i.eq.j) then
		fmat(i,j)=ep-dtm*(fmatc(i,j)+fmato(i,j)+
     &		gmatc(i,j)+gmato(i,j))
		else
		fmat(i,j)=-1.d0*dtm*(fmatc(i,j)+fmato(i,j)+
     &		gmatc(i,j)+gmato(i,j))
		endif
		enddo
		enddo
		yn=ynf+yng

		call assconne(n,ix(1,n),geqnix(1,n),ldem)

		do m = 1,4*nel
		do i = 1, nphi
		if (ldem(m,1) .eq. sbc_pi(1,i)) then
			ldem(m,5) = 1
			ub(m) = sbc_pi(2,i)
		endif
		enddo
		enddo

		do m = 1,4*nel
		do i = 1, na
		if (ldem(m,1) .eq. sbc_a(1,i)) then
			ldem(m,5) = 2
			ub(m) = sbc_a(2,i)
		endif
		enddo
		enddo

		do m = 1,4*nel
		if (ldem(m,5) .eq. 0) then
		do j = 1,4*nel
		if (ub(j) .ne. 0.0d0) then
			yn(m) = yn(m) - fmat(m,j)*ub(j)
		endif
		enddo
		endif
		enddo

		do m = 1,4*nel
		if (ldem(m,5) .ne. 0) then
			do j = 1,4*nel
				fmat(m,j)=0.0d0
				fmat(j,m)=0.0d0
			enddo
			fmat(m,m)=1.0d0
			yn(m) = ub(m)
		endif
		enddo

		do m = 1,4*nel
			i = ldem(m,1) - 1
			do k = ldem(m,3),ldem(m,4)
				j = ldem(k,2) - 1
				val = fmat(m,k)
	call MatSetValue( xmatgts, i, j, val, ADD_VALUES, ierr )
			enddo
		enddo


		do i = 1, 4*nel
			if(ldem(i,1).gt.0) then
				j = ldem(i,1)-1
				val = yn(i)
	call VecSetValue (grhs, j,val,ADD_VALUES,ierr)
			endif
		enddo 
         
		else

		call form_elem_fa_d_rypa(
     &        f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dtm,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

		call form_elem_fpi_h_rypa(
     &        f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dtm,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

		call form_elem_fa_h_rypa(
     &        f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dtm,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

		call assconne(n,ix(1,n),geqnix(1,n),ldem)

		do m = 1,4*nel
		do i = 1, nphi
		if (ldem(m,1) .eq. sbc_pi(1,i)) then
			ldem(m,5) = 1
			ub(m) = sbc_pi(2,i)
		endif
		enddo
		enddo

		do m = 1,4*nel
		do i = 1, na
		if (ldem(m,1) .eq. sbc_a(1,i)) then
			ldem(m,5) = 2
			ub(m) = sbc_a(2,i)
		endif
		enddo
		enddo

		do m = 1,4*nel
		if (ldem(m,5) .ne. 0) then
			f_vector(m) = ub(m)
		endif
		enddo

		do i = 1, 4*nel
			if(ldem(i,1).gt.0) then
				j = ldem(i,1)-1
				val = f_vector(i)
	call VecSetValue (grhs, j,val,ADD_VALUES,ierr)
			endif
		enddo 

		endif

	enddo ! end of element loop

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	call VecAssemblyBegin (grhs, ierr)
	call VecAssemblyEnd   (grhs, ierr)

	if ((jind.eq.1)) then
	call MatAssemblyBegin (xmatgts, MAT_FINAL_ASSEMBLY, ierr)
	call MatAssemblyEnd   (xmatgts, MAT_FINAL_ASSEMBLY, ierr)
 	endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if (nphi .gt. 0) then
		do p = 1,nphi
			posp = sbc_pi(1,p)-1
			valp = sbc_pi(2,p)
	call VecSetValue (grhs, posp, valp,INSERT_VALUES,ierr)
		enddo
	endif
	if (na .gt. 0) then
		do q = 1,na
			posq = sbc_a(1,q)-1
			valq = sbc_a(2,q)
	call VecSetValue (grhs, posq,valq,INSERT_VALUES,ierr)
		enddo
	endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	call VecAssemblyBegin (grhs, ierr)
	call VecAssemblyEnd   (grhs, ierr)
		  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vector!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	do nb = 1,nnbsurf
		mtlidx = ix_nbsurf(5,nb)

		do i = 1,4
			nbc_vector_e(i) = 0.0d0
			do j = 1,3
				e_flux_input(j,i) = 0.0d0
			enddo
			do j = 1,3
				sg2(j,i) = 0.0d0
			enddo
			xsj2(i) = 0.0d0
			do j = 1,4
			do k = 1,3
				shp2(k,j,i) = 0.0d0
			enddo
			enddo
		enddo

		do i = 1,4
			do j = 1,ndm
				e_flux_input(j,i) = mag_em*sin(w_em*t)
C				e_flux_input(j,i) = 
C     &			((t-tmax)/tmax)**2.d0+
C     &			exp(-t/tmax)*sin(w_me*t)+0.8*sin(w_em*t)
			enddo
		enddo

		ndirec = ix_nbsurf(6,nb)
		do i = 1,4
		node1 = ix_nbsurf(i,nb)
		if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
			xl2d(1,i) = x(2,node1)
			xl2d(2,i) = x(3,node1)
			e_mechdisp2d(1,i) = mechdisp(2,node1)
			e_mechdisp2d(2,i) = mechdisp(3,node1)
		elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
			xl2d(1,i) = x(3,node1)
			xl2d(2,i) = x(1,node1)
			e_mechdisp2d(1,i) = mechdisp(3,node1)
			e_mechdisp2d(2,i) = mechdisp(1,node1)
		elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
			xl2d(1,i) = x(1,node1)
			xl2d(2,i) = x(2,node1)
			e_mechdisp2d(1,i) = mechdisp(1,node1)
			e_mechdisp2d(2,i) = mechdisp(2,node1)
		endif
		enddo

		g = 1.0d0/((3.0d0)**(0.5d0))
		do i = 1,4
			sg2(1,i) = ig(i)*g
			sg2(2,i) = jg(i)*g
			sg2(3,i) = 1.0d0
		enddo

		do i = 1,4 ! lint
			call shp2d_4(shp2,xsj2,xl2d,sg2,2,4,i)
			xsj2(i) = xsj2(i)*sg2(3,i)
		enddo

		f2 = 0.0d0
		fi2 = 0.0d0
		c2 = 0.0d0
		ci2 = 0.0d0
		f2d = 0.0d0
		fi2d = 0.0d0
		c2d = 0.0d0
		ci2d = 0.0d0
		detfi2 = 0.0d0

		do i = 1,4
	call kine2d_lag(shp2,e_mechdisp2d,f2,fi2,c2,ci2,detfi2,i)
		enddo

		ndirec = ix_nbsurf(6,nb)
		do i = 1,4
		do j = 1,4
		if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
			shp2d(1,j,i) = 0.0d0
			shp2d(2,j,i) = shp2(1,j,i)
			shp2d(3,j,i) = shp2(2,j,i)
			shp2d(4,j,i) = shp2(3,j,i)
                  
		elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
			shp2d(1,j,i) = shp2(2,j,i)
			shp2d(2,j,i) = 0.0d0
			shp2d(3,j,i) = shp2(1,j,i)
			shp2d(4,j,i) = shp2(3,j,i)
                  
		elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
			shp2d(1,j,i) = shp2(1,j,i)
			shp2d(2,j,i) = shp2(2,j,i)
			shp2d(3,j,i) = 0.0d0
			shp2d(4,j,i) = shp2(3,j,i)                  
                  
		endif
		enddo
		enddo

		ndirec = ix_nbsurf(6,nb)
		do i = 1,4
		if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
			f2d(1,1,i) = 1.0d0
			f2d(2,2,i) = f2(1,1,i)
			f2d(2,3,i) = f2(1,2,i)
			f2d(3,2,i) = f2(2,1,i)
			f2d(3,3,i) = f2(2,2,i)
               
		elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
			f2d(1,1,i) = f2(2,2,i)
			f2d(1,3,i) = f2(2,1,i)
			f2d(2,2,i) = 1.0d0
			f2d(3,1,i) = f2(1,2,i)
			f2d(3,3,i) = f2(1,1,i)
               
		elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
			f2d(1,1,i) = f2(1,1,i)
			f2d(1,2,i) = f2(1,2,i)
			f2d(2,1,i) = f2(2,1,i)
			f2d(2,2,i) = f2(2,2,i)
			f2d(3,3,i) = 1.0d0
               
		endif
		enddo

		do i = 1,4 ! lint
	call kine2d_lag_1(shp2,e_mechdisp2d,f2d,fi2d,c2d,ci2d,detfi2,i)
		enddo

		call natural_bc_d(
     &          nbc_vector_e,nb,
     &          ix_nbsurf,
     &          shp2d,xsj2,
     &          4,2,nnbsurf,4,
     &          ep,mu,sigma,
     &          f2d,fi2d,c2d,ci2d,detfi2,
     &          e_flux_input)
         
		do i = 1,4
			grow = 4*geqix(ix_nbsurf(i,nb))-3
			lrow = i
			valnbc=nbc_vector_e(lrow)
	call VecSetValue(grhs,grow-1,valnbc,ADD_VALUES,ierr) 

		enddo
	enddo

	call VecAssemblyBegin (grhs, ierr)
	call VecAssemblyEnd   (grhs, ierr)
		  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!--------------KSP solver for the system (Jacobian preconditioner)----------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	if ((jind.eq.1)) then

	call KSPCreate        (PETSC_COMM_WORLD, kspgsol,ierr)
	call KSPSetOperators  (kspgsol, xmatgts, xmatgts,
     &        DIFFERENT_NONZERO_PATTERN,ierr)

	call KSPGetPC(kspgsol,pcem,ierr)

	call PCSetType(pcem,PCBJACOBI,ierr)
	tol = 1.e-20
	call KSPSetTolerances(kspgsol,tol,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_INTEGER,ierr) 

	endif

	call KSPSolve (kspgsol, grhs, gsol, ierr)

!!!!!!!!!!!!!!!!!!!!!!!!!!!! Restore xc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	Phi
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
	call VecGetArray (gsol, p_n_t1, i_x, ierr)
	do i = 1, numpn 
		j = (i-1)*4+1
		val = p_n_t1(i_x + j)
		p_n_t2 (i) = val
	enddo
	call VecRestoreArray (gsol,p_n_t2,i_x,ierr)
         
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr*1
	pmaxs = pmaxs*1
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (p_n_t2,mr(np(248)),mr(np(249)),
     &          mr(np(250)),mr(np(251)),mr(np( 31)),
     &          ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)

	do i = 1, numnp
		ans_phi(i)=p_n_t2(i)
		d_phi_n(i)  = (1.0d0/dt)*(ans_phi(i)-phi_n(i))
		phi_n(i) = ans_phi(i)
	enddo
         
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_x
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         
	call VecGetArray (gsol, a_n_t1, i_y, ierr)
	do i =1, numpn
		j = (i-1)*4+2
		vala = a_n_t1(i_y + j)
		a_n_t2 (i) = vala
	enddo
	call VecRestoreArray (gsol,a_n_t2,i_y,ierr)
         
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr*1
	pmaxs = pmaxs*1
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (a_n_t2,mr(np(248)),mr(np(249)),
     &       mr(np(250)),mr(np(251)),mr(np( 31)),
     &       ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)

	do i = 1, numnp
		ans_a(1,i)=a_n_t2(i)
		dd_a_n(1,i) = (1.0d0/(dt**2.0d0))*
     &           (ans_a(1,i)-2*a_n(1,i)+a_n_1(1,i))
		d_a_n(1,i)  = (1.0d0/dt)*(ans_a(1,i)-a_n(1,i))
		a_n_1(1,i)  = a_n(1,i)
		a_n(1,i)  = ans_a(1,i)
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_y
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	call VecGetArray (gsol, a_n_t3, i_y, ierr)
	do i =1, numpn
		j = (i-1)*4+3
		vala = a_n_t3(i_y + j)
		a_n_t4 (i) = vala
	enddo
	call VecRestoreArray (gsol,a_n_t4,i_y,ierr)
         
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr*1
	pmaxs = pmaxs*1
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (a_n_t4,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)

	do i = 1, numnp
		ans_a(2,i)=a_n_t4(i)
		dd_a_n(2,i) = (1.0d0/(dt**2.0d0))*
     &           (ans_a(2,i)-2*a_n(2,i)+a_n_1(2,i))
		d_a_n(2,i)  = (1.0d0/dt)*(ans_a(2,i)-a_n(2,i))
		a_n_1(2,i)  = a_n(2,i)
		a_n(2,i)  = ans_a(2,i)
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_z
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
	call VecGetArray (gsol, a_n_t5, i_y, ierr)
	do i =1, numpn
		j = (i-1)*4+4
		vala = a_n_t5(i_y + j)
		a_n_t6 (i) = vala
	enddo
	call VecRestoreArray (gsol,a_n_t6,i_y,ierr)
         
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr*1
	pmaxs = pmaxs*1
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (a_n_t6,mr(np(248)),mr(np(249)),
     &        mr(np(250)),mr(np(251)),mr(np( 31)),
     &        ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)

	do i = 1, numnp
		ans_a(3,i)=a_n_t6(i)
		dd_a_n(3,i) = (1.0d0/(dt**2.0d0))*
     &           (ans_a(3,i)-2*a_n(3,i)+a_n_1(3,i))
		d_a_n(3,i)  = (1.0d0/dt)*(ans_a(3,i)-a_n(3,i))
		a_n_1(3,i)  = a_n(3,i)
		a_n(3,i)  = ans_a(3,i)
	enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call VecDestroy(gsol,ierr)
      call VecDestroy(grhs,ierr)

	if (jind.eq.nc) then
	do i=1,nnp
		ytt(8*i-7)=phi_n(i)
		ytt(8*i-6)=d_phi_n(i)
		ytt(8*i-5)=a_n(1,i)
		ytt(8*i-4)=d_a_n(1,i)
		ytt(8*i-3)=a_n(2,i)
		ytt(8*i-2)=d_a_n(2,i)
		ytt(8*i-1)=a_n(3,i)
		ytt(8*i-0)=d_a_n(3,i)
	enddo
	endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!       Find Gradient of Phi        !!!!!!!!!!!!!!!
      call grad_scalar(gradphi,phi_n,x,ix,numnp,numel)

!!!!!!!!!!!!!!!    Find Curl of A (B field)     !!!!!!!!!!!!!!!
      call curl_vector(bf,a_n,x,ix,numnp,numel)

!!!!!!!!!!!!!!!       Find E field     !!!!!!!!!!!!!!!
	 do i = 1, numnp
		do j = 1, 3
		   ef(j,i)=-1*gradphi(j,i)-d_a_n(j,i)
		end do
	 end do
!!!!!!!!!!!!!!!       Find Finv X Vector        !!!!!!!!!!!!!!!
	 call fidotvector(fivel,mechvel,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,
     &     mechdisp)
!!!!!!!!!!!!!!!       Find velocity cross B     !!!!!!!!!!!!!!!
	 call cross_product(velb,fivel,bf,numnp)

!!!!!!!!!!!!!!!       Find D field     !!!!!!!!!!!!!!!
c     e_tilda field 
	 call get_efield_tilda(etilda,ef,velb,numnp)

c     d field
	 call vectordotci(df,etilda,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp)


!!!!!!!!!!!!!!!       Find H field     !!!!!!!!!!!!!!!
c     vel x d
	 call cross_product(veld,fivel,df,numnp)

c     h field_tmp
	 call vectordotc(h_tmp,bf,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp)

c     h field
	 call get_hfield(hf,h_tmp,veld,numnp,ep,mu)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	enddo

	call KSPDestroy(kspgsol,ierr)
	call MatDestroy(xmatgts,ierr)


	enddo

	do i=1,nnp
		yt0(8*i-7)=phi_n(i)
		yt0(8*i-6)=d_phi_n(i)
		yt0(8*i-5)=a_n(1,i)
		yt0(8*i-4)=d_a_n(1,i)
		yt0(8*i-3)=a_n(2,i)
		yt0(8*i-2)=d_a_n(2,i)
		yt0(8*i-1)=a_n(3,i)
		yt0(8*i)=d_a_n(3,i)
	enddo

	end subroutine calc_its1_kos






