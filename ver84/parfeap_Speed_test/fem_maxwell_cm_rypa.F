
	subroutine fem_maxwell_cm_rypa(
     &           hfield,dfield,bfield,efield,
     &           phi_n,d_phi_n,
     &           a_n_1,a_n,d_a_n,dd_a_n,
     &           ts,dt,w_me,mag_me,w_em,mag_em,
     &           x,ix,mat_prop,numnp,numel,
     &           mechvel,mechacc,
     &           mechdisp,mechdisp_1,
     &           ix_nbsurf,nnbsurf,
     &           ix_nbsurfa,nnbsurfa,
     &           sbc_pi,nsbc_pi,sbc_a,nsbc_a)

      implicit  none

#     include   "finclude/petsc.h"
#     include   "pointer.h"
#     include   "comblk.h"
#     include   "pfeapb.h"
#     include   "sdata.h"

C	integer   ndm
      integer   ts
      integer   nel,lint

      integer   numnp,numel,nsbc_pi,nsbc_a
      integer   nnbsurf,nnbsurfa

      integer   numnp2

      integer   ix(17,*)
	 real*8  :: sbc_pi(2,nsbc_pi),sbc_a(2,nsbc_a)
	 integer ::   ix_nbsurf(6,nnbsurf)
	 integer ::   ix_nbsurfa(6,nnbsurfa)

      integer   m1,m2,i,j

      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    hfield(3,numnp),dfield(3,numnp)
      real*8    bfield(3,numnp),efield(3,numnp)

      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      real*8    phi_n(numnp),d_phi_n(numnp)

      real*8    gradphi(3,numnp)

	real*8 :: k_matrix(32,32)
	real*8 :: f_vector(32)
	real*8 :: c_matrix(32,32)
	real*8 :: m_matrix(32,32)
	real*8 :: F_vec(32)

      real*8    mechvel(3,numnp),mechacc(3,numnp)
      real*8    mechdisp(3,numnp),mechdisp_1(3,numnp)

      real*8    x_ans_s(4*numnp)
      real*8    x_ans_pi(numnp),x_ans_a(3,numnp)

      integer nphi, na

      save

      ndm = 3
      nel = 8
      lint = 8

	 nphi=nsbc_pi
 	 na=nsbc_a

c$    starting parallel program
c     initialize s and b


c     construct stiffness matrix
      


           call form_pi_a_cm_rypa(
     &     k_matrix,c_matrix,m_matrix,f_vector,F_vec,
     &     x,ix,mat_prop,nel,ndm,numnp,numel,lint,
     &     ts,dt,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     w_me,mag_me,w_em,mag_em,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     nsbc_pi,sbc_pi,nsbc_a,sbc_a,
     &     ix_nbsurf,nnbsurf,
     &     ix_nbsurfa,nnbsurfa,
     &     mr(up(1)),mr(up(2)),mr(up(22)) 
     &     ) 


           end

