
      subroutine fdotvector( b,u,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp )

c     SUBROUTINE FOR  J^-1 ( F ).( vector )

c     b : output vector
c     u : input vector

      implicit   none

      integer    numnp,numel,ndm,nel,lint

      integer    i,j,k,n,nix,nix1,l

      integer    matlidx

      integer    count(numnp),ix(17,numel)

      integer    ig(4),jg(4)
      
      real*8     x(3,numnp),u(3,numnp),mat_prop(3,2)

      real*8     shp(4,8,8),sg(4,8),xsj(8),g

      real*8     xl(3,8),eb(3,8),disp(3,8)

      real*8     f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8),detfi(8)

      real*8     cmtr(24),bmtr(24),amtr(24,24)

      real*8     b(3,numnp),mechdisp(3,numnp)

      real*8     temp,eb2(3,8)

      real*8     ep,mu

      save

      data       ig/-1,1,1,-1/,jg/-1,-1,1,1/

c     initialize b and count
      do i = 1,numnp
         do j = 1,ndm
            b(j,i) = 0.0d0
         end do
      end do
            
      do i = 1,numnp
         count(i) = 0
      end do

      do n = 1,numel
c     get mat'l properties
!         matlidx = ix(9,n)

         ep = mat_prop(1,2)
         mu = mat_prop(2,2)
         
c     initialize
         do i = 1,lint
            do j = 1,nel
               do k = 1,4
                  shp(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,8
            do j = 1,4
               sg(j,i) = 0.0d0
            end do
         end do
         
         do i = 1,lint
            xsj(i) = 0.0d0
         end do

         do i = 1,8
            do j = 1,3
               xl(j,i) = 0.0d0
               disp(j,i) = 0.0d0
            end do
         end do

c     collect element coord.
         do i = 1,nel
            do j = 1,ndm
               xl(j,i) = x(j,ix(i,n))
               disp(j,i) = mechdisp(j,ix(i,n))
            end do
         end do

         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg(1,i) = ig(i)*g
            sg(1,i+4) = sg(1,i)
            sg(2,i) = jg(i)*g
            sg(2,i+4) = sg(2,i)
            sg(3,i) = g
            sg(3,i+4) = -g 
            sg(4,i) = 1.0d0
            sg(4,i+4) = 1.0d0
         end do
         do i = 1,lint
            call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
            xsj(i) = xsj(i)*sg(4,i)
         end do

c     F, Fi, C, Ci
         do i = 1,lint
            do j = 1,3
               do k = 1,3
                  f(k,j,i) = 0.0d0
                  fi(k,j,i) = 0.0d0
                  c(k,j,i) = 0.0d0
                  ci(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,lint
            detfi(i) = 0.0d0
         end do
         
         do i = 1,8 ! lint
            call kine3df_lag(shp,disp,f,fi,c,ci,detfi,i)
         end do

c    evaluate u at integration points
         do i = 1,8
            do j = 1,3
               eb(j,i) = 0.0d0
            end do
         end do

         do j = 1,lint
            nix = ix(j,n)
            do i = 1,nel
               nix1 = ix(i,n)
               eb(1,j) = eb(1,j) + shp(4,i,j)*u(1,nix1)
               eb(2,j) = eb(2,j) + shp(4,i,j)*u(2,nix1)
               eb(3,j) = eb(3,j) + shp(4,i,j)*u(3,nix1)
            end do
            count(nix) = count(nix) + 1
         end do

         do i = 1,lint
            do j = 1,3
               eb2(j,i) = 0.0d0
               temp = 0.0d0
               do k = 1,3
                  temp = temp + f(j,k,i)*eb(k,i)/(detfi(i))
               end do
               eb2(j,i) = temp
            end do
         end do

         do i = 1,24
            bmtr(i) = 0.0d0
            cmtr(i) = 0.0d0
         end do

         i = 1
         do j = 1,nel
            bmtr(3*j-2) = eb2(1,i)
            bmtr(3*j-1) = eb2(2,i)
            bmtr(3*j) = eb2(3,i)
            i = i + 1
         end do

         do i = 1,(3*nel)
            do j = 1,(3*nel)
               amtr(j,i) = 0.0d0
            end do
         end do

         i = 1
         do j = 1,nel
            l = 1
            do k = 1,nel
               amtr(3*j-2,3*k-2) = shp(4,l,i)
               amtr(3*j-1,3*k-1) = shp(4,l,i)
               amtr(3*j,3*k) = shp(4,l,i)
               l = l + 1
            end do
            i = i + 1
         end do

         call solgau(amtr,cmtr,bmtr,24)

         do i = 1,nel
            nix = ix(i,n)
            b(1,nix) = b(1,nix) + cmtr(3*i-2)
            b(2,nix) = b(2,nix) + cmtr(3*i-1)
            b(3,nix) = b(3,nix) + cmtr(3*i)
         end do
      end do

      do i = 1,numnp
         do j = 1,ndm
            b(j,i) = b(j,i)/count(i)
         end do
      end do

      end

