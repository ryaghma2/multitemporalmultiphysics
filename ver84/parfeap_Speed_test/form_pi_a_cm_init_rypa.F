
	subroutine form_pi_a_cm_init_rypa(
     &     k_matrix,c_matrix,m_matrix,f_vector,F_vec,
     &     x,ix,mat_prop,nel,ndm,numnp,numel,lint,
     &     ts,dt,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     w_me,mag_me,w_em,mag_em,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     nsbc_pi,sbc_pi,nsbc_a,sbc_a,      
     &     ix_nbsurf,nnbsurf,
     &     gnodeix,geqnix,geqix
     &     )

c     k_matrix : global tangent matrix
c     f_vector : global force vector
c     x        : global node coord.
c     ix       : nodal connectivity
c     ndm      : dimension
c     nel      : nodes per element
c     numel    : number of elements
c     dt       : time step
c     geqix    : when reduce results to each processor, local nodes

      implicit  none

#     include   "finclude/petsc.h"
#     include   "pfeapb.h"
#     include   "time_flag.h"
#     include   "pointer.h"
#     include   "comfil.h"
#     include   "comblk.h"
#     include   "setups.h"





      integer   ts
      integer   nel,lint,ndm

      integer   numnp,numel
	integer:: nsbc_pi,nsbc_a
C	integer:: numpn
C	integer:: numtn
      integer   nnbsurf

      integer   posp,posq		! specific BC
      integer  ix(17,*)
	 integer :: nphi,na
	 real*8  :: sbc_pi(2,nsbc_pi),sbc_a(2,nsbc_a)
      integer   ix_nbsurf(6,nnbsurf)

      integer   i,j,k
      integer   n,node,nb,mtlidx,ndirec,node1
      integer   eqn,lnode
      integer   ig(4),jg(4)

      integer   grow,lrow

      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    t
      real*8    ep,mu,sigma,g

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    ans_a(3,numnp)        ! update values
      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      real*8    ans_phi(numnp)        ! update values
      real*8    phi_n(numnp),d_phi_n(numnp)

      real*8 :: k_matrix(4*nel,4*nel)
	 real*8 :: f_vector(4*nel)
	 real*8 :: c_matrix(4*nel,4*nel)
	 real*8 :: m_matrix(4*nel,4*nel)
	 real*8 :: F_vec(4*nel)

      real*8    mechvel(3,numnp),mechacc(3,numnp)
      real*8    mechdisp(3,numnp),mechdisp_1(3,numnp)

      real*8    e_flux_input(ndm,4)
      real*8    e_flux_input_d(ndm,4),e_flux_input_h(ndm,4)

      real*8    sg(4,lint),xl(ndm,nel)
      real*8    shp(4,nel,lint),xsj(lint)

      real*8    e_a_n_1(ndm,nel),e_a_n(ndm,nel)
      real*8    e_d_a_n(ndm,nel),e_dd_a_n(ndm,nel)
      real*8    e_phi_n(nel),e_d_phi_n(nel)

      real*8    e_mechvel(ndm,nel),e_mechacc(ndm,nel)
      real*8    e_mechdisp(ndm,nel),e_mechdisp_1(ndm,nel)

      real*8    nbc_vector_e(4)
      real*8    nbc_vector_e_d(4),nbc_vector_e_h(3*4)

      real*8    sg2(3,4),xl2d(2,4),shp2(3,4,4),xsj2(4)
      real*8    shp2d(4,4,4)

      real*8    f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8    detfi(8)

      real*8    f_1(3,3,8),fi_1(3,3,8),c_1(3,3,8),ci_1(3,3,8)
      real*8    detfi_1(8)

      real*8    d_f(3,3,8),d_fi(3,3,8),d_c(3,3,8),d_ci(3,3,8)
      real*8    d_detfi(8)

      real*8    f2(3,3,4),fi2(3,3,4),c2(3,3,4),ci2(3,3,4)
      real*8    f2d(3,3,4),fi2d(3,3,4),c2d(3,3,4),ci2d(3,3,4)
      real*8    detfi2(4)

      real*8    e_mechdisp2d(2,4)

	 real*8  :: tmax

!!!!!!!!!!! data transformation between processors!!!
      logical    setvar, palloc
      integer    pmaxr, pmaxs
      integer    ndf


!!!!!!!!!!!! current configuration info!!!!!!!!!!!!!!!!!!!
      real*8    phicur(numnp),phicurv(numnp)

!!!!!!!!!!!!    post processing        !!!!!!!!!!!!!!!!!!!
      real*8    gradphi(3,numnp)
      real*8    velb(3,numnp),etilda(3,numnp)
      real*8    fivel(3,numnp)
      real*8    d(3,numnp),b(3,numnp),e(3,numnp)
      real*8     ans_d1(numnp) 
      real*8     ans_d2(numnp) 
      real*8     ans_d3(numnp) 



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      integer    gnodeix(nel,numel), geqnix(nel,numel) 	!connectivity of nodes and equations
      integer    geqix(numnp)
      integer    m,p,q
      PetscScalar valp,valq
      PetscScalar valnbc
      real*8     val,vala
      PetscScalar p_n_t1(1)
      real*8     p_n_t2(numnp) 
      PetscScalar a_n_t1(1)
      PetscScalar a_n_t3(1)
      PetscScalar a_n_t5(1)
      real*8     a_n_t2(numnp) 
      real*8     a_n_t4(numnp) 
      real*8     a_n_t6(numnp) 
      integer    ldem(4*nel,5)
      real*8     ub(4*nel) 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      PetscViewer    K_view, B_view, K_viewcon
!      PetscTruth     chk
      PetscBool     chk
      PetscErrorCode   ierr
      PetscScalar oneS
      PetscScalar moneS
      PetscInt oneI
      PetscOffset i_x
      PetscOffset i_y
      PC             pcem
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Mat Kmatem     !global assembly matrix
      common /pfeapem/ Kmatem

      PetscInt ncol
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      Vec rhsem
      common /pfeapem/ rhsem
      Vec solem
      common /pfeapem/ solem
!!!!!!!!!get sub vector!!!!!!!!!!!!!!!!!!!!!

      KSP kspsolem
      common /pfeapem/ kspsolem

      real*8      tol


      save

      data       ig/-1,1,1,-1/,jg/-1,-1,1,1/

c$    form matrices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      t = dt*ts

      tmax=2*3.14157/w_me*10.d0
	 nphi=nsbc_pi
	 na=nsbc_a

      oneI = 1.0
      oneS = 1.0
c$    construct stiffness matrix

          call MatCreate(PETSC_COMM_WORLD,Kmatem,ierr)
          call MatSetSizes(Kmatem,
     &		4*numpn,4*numpn,PETSC_DETERMINE,
     &          PETSC_DETERMINE,ierr)
          call MatSetBlockSize(Kmatem,4,ierr)
          call MatSetFromOptions(Kmatem, ierr)

          call MatSetType(Kmatem,MATBAIJ,ierr)
          call MatMPIBAIJSetPreallocation(Kmatem,4,
     &          PETSC_NULL_INTEGER,mr(np(246)),
     &          PETSC_NULL_INTEGER,mr(np(247)),
     &          ierr)


          call VecCreate        (PETSC_COMM_WORLD, rhsem, ierr)
          call VecSetSizes      (rhsem, 4*numpn, PETSC_DECIDE, ierr) !parallel rhs 32-->local processor
          call VecSetFromOptions(rhsem, ierr)
        
          call VecCreate        (PETSC_COMM_WORLD, solem, ierr)
          call VecSetSizes      (solem, 4*numpn, PETSC_DECIDE, ierr) !parallel rhs 32-->local processor
          call VecSetFromOptions(solem, ierr)

      do n = 1,numel
c     get material constant and form inverse matrix
         ep = mat_prop(1,2)
         mu = mat_prop(2,2)
         sigma = mat_prop(3,2)
            do p = 1,4*nel
              do q = 1,4*nel
                   k_matrix(p,q) = 0.0d0
                   c_matrix(p,q) = 0.0d0
                   m_matrix(p,q) = 0.0d0
              end do
                   f_vector(p) = 0.0d0
                   F_vec(p) = 0.0d0
                   ub(p) = 0.0d0
            end do

c$    get element coords. and element mech vel.
         do i = 1,nel

            eqn = geqnix(i,n)
            node = gnodeix(i,n) !global nodes
            lnode = ix(i,n) !local nodes
            do j = 1,3
               xl(j,i) = x(j,lnode)
            
               e_a_n_1(j,i) = a_n_1(j,lnode)
               e_a_n(j,i) = a_n(j,lnode)
               e_d_a_n(j,i) = d_a_n(j,lnode)
               e_dd_a_n(j,i) = dd_a_n(j,lnode)
               
               e_mechvel(j,i) = mechvel(j,lnode)
               e_mechacc(j,i) = mechacc(j,lnode)
               e_mechdisp(j,i) = mechdisp(j,lnode)
               e_mechdisp_1(j,i) = mechdisp_1(j,lnode)
            end do
            e_phi_n(i) = phi_n(lnode)
            e_d_phi_n(i) = d_phi_n(lnode)
        
         end do
    
c$    shpfunc. values @ integration points
         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg(1,i) = ig(i)*g
            sg(1,i+4) = sg(1,i)
            sg(2,i) = jg(i)*g
            sg(2,i+4) = sg(2,i)
            sg(3,i) = -g
            sg(3,i+4) = g
            sg(4,i) = 1.0d0
            sg(4,i+4) = 1.0d0
         end do

         do i = 1,lint ! loop to integration poits
            call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
            xsj(i) = xsj(i)*sg(4,i)
         end do
         
c     F, Fi, C, Ci
         do i = 1,lint
            do j = 1,3
               do k = 1,3
                  f(k,j,i) = 0.0d0
                  fi(k,j,i) = 0.0d0
                  c(k,j,i) = 0.0d0
                  ci(k,j,i) = 0.0d0

                  f_1(k,j,i) = 0.0d0
                  fi_1(k,j,i) = 0.0d0
                  c_1(k,j,i) = 0.0d0
                  ci_1(k,j,i) = 0.0d0

                  d_f(k,j,i) = 0.0d0
                  d_fi(k,j,i) = 0.0d0
                  d_c(k,j,i) = 0.0d0
                  d_ci(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,lint
            detfi(i) = 0.0d0
            detfi_1(i) = 0.0d0
            d_detfi(i) = 0.0d0
         end do
         
c     n+1 : current step
         do i = 1,8 ! lint
            call kine3df_lag(shp,e_mechdisp,f,fi,c,ci,detfi,i)
         end do
c     n : previous step
         do i = 1,8 ! lint
            call kine3df_lag(shp,e_mechdisp_1,f_1,fi_1,c_1,ci_1,
     &           detfi_1,i)
         end do
c     rate of f, fi, c, ci
         do i = 1,8
            do j = 1,3
               do k = 1,3
                  d_f(k,j,i) = (f(k,j,i) - f_1(k,j,i))/dt
                  d_fi(k,j,i) = (fi(k,j,i) - fi_1(k,j,i))/dt
                  d_c(k,j,i) = (c(k,j,i) - c_1(k,j,i))/dt
                  d_ci(k,j,i) = (ci(k,j,i) - ci_1(k,j,i))/dt
               end do
            end do
            d_detfi(i) = (detfi(i) - detfi_1(i))/dt
         end do

c$    form elem. matrix of d eqn
c     form k & m elem. matrix of pi in d
         call form_elem_pi_d_rypa(
     &        k_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

         call form_elem_pi_h_rypa(
     &        k_matrix,c_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

         call form_elem_a_h_init_rypa(
     &        k_matrix,c_matrix,m_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        ts,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)


	    k_matrix=k_matrix+1.d0/dt*c_matrix+1.d0/(dt**2.d0)*m_matrix
	    f_vector=f_vector

         
         call assconne(n,ix(1,n),geqnix(1,n),ldem)


           do m = 1,4*nel
               do i = 1, nphi
               if (ldem(m,1) .eq. sbc_pi(1,i)) then
                  ldem(m,5) = 1
                  ub(m) = sbc_pi(2,i)
               end if
               end do
           end do

            do m = 1,4*nel
               do i = 1, na
               if (ldem(m,1) .eq. sbc_a(1,i)) then
                  ldem(m,5) = 2
                  ub(m) = sbc_a(2,i)
               end if
               end do
            end do

            do m = 1,4*nel
              if (ldem(m,5) .eq. 0) then
               do j = 1,4*nel
                if (ub(j) .ne. 0.0d0) then
                   f_vector(m) = f_vector(m) - k_matrix(m,j)*ub(j)
                end if
               end do
              end if
            end do

            do m = 1,4*nel
               if (ldem(m,5) .ne. 0) then
                do j = 1,4*nel
                	k_matrix(m,j)=0.0d0
                	k_matrix(j,m)=0.0d0
                end do
                	k_matrix(m,m)=1.0d0
                	f_vector(m) = ub(m)
               end if
             end do

     
            do m = 1,4*nel
              i = ldem(m,1) - 1
              do k = ldem(m,3),ldem(m,4)
              	j = ldem(k,2) - 1
              	val = k_matrix(m,k)
              	call MatSetValue( Kmatem, i, j, val, ADD_VALUES, ierr )
              end do
            end do



       do i = 1, 4*nel
          if(ldem(i,1).gt.0) then
              call VecSetValue (rhsem, ldem(i,1)-1, f_vector(i)
     &                             ,ADD_VALUES,ierr)
          endif
        end do 
        


      end do ! end of element loop

          
          call VecAssemblyBegin (rhsem, ierr)
          call VecAssemblyEnd   (rhsem, ierr)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

          call MatAssemblyBegin (Kmatem, MAT_FLUSH_ASSEMBLY, ierr)
          call MatAssemblyEnd   (Kmatem, MAT_FLUSH_ASSEMBLY, ierr)


         do p = 1,nphi
            posp = sbc_pi(1,p)-1
            val = 1.0d0
            call MatSetValue(Kmatem, posp, posp,val,INSERT_VALUES, ierr)
         end do

         do q = 1,na
            posq = sbc_a(1,q)-1
            val = 1.0d0
            call MatSetValue(Kmatem, posq, posq,val,
     &                    INSERT_VALUES, ierr)  
         end do
 
          call MatAssemblyBegin (Kmatem, MAT_FINAL_ASSEMBLY, ierr)
          call MatAssemblyEnd   (Kmatem, MAT_FINAL_ASSEMBLY, ierr)
 
!          call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"Kmatem.m",K_view,
!     &                                ierr)
!          call PetscViewerSetFormat(K_view,PETSC_VIEWER_ASCII_MATLAB,
!     &                                ierr)
!          call MatView             (Kmatem,K_view,ierr)
!          call PetscViewerDestroy  (K_view,ierr)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         if (nphi .gt. 0) then
          do p = 1,nphi
            posp = sbc_pi(1,p)-1
            valp = sbc_pi(2,p)
            call VecSetValue (rhsem, posp, valp,INSERT_VALUES,ierr)
          end do
         end if
         if (na .gt. 0) then
          do q = 1,na
            posq = sbc_a(1,q)-1
            valq = sbc_a(2,q)
            call VecSetValue (rhsem, posq,valq,INSERT_VALUES,ierr)
          end do
         end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          call VecAssemblyBegin (rhsem, ierr)
          call VecAssemblyEnd   (rhsem, ierr)
          

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vector!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      do nb = 1,nnbsurf
         mtlidx = ix_nbsurf(5,nb)

        do i = 1,4
            nbc_vector_e(i) = 0.0d0
            do j = 1,3
               e_flux_input(j,i) = 0.0d0
            end do
            do j = 1,3
               sg2(j,i) = 0.0d0
            end do
            xsj2(i) = 0.0d0
            do j = 1,4
               do k = 1,3
                  shp2(k,j,i) = 0.0d0
               end do
            end do
         end do

         do i = 1,4
               do j = 1,ndm
C                  e_flux_input(j,i) = mag_em*sin(w_em*t)
C                  e_flux_input(j,i) = mag_em
                   e_flux_input(j,i) = 
     &			((t-tmax)/tmax)**2.d0+
     &			exp(-t/tmax)*sin(w_me*t)+0.8*sin(w_em*t)
               end do
         end do

         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            node1 = ix_nbsurf(i,nb)
            if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
               xl2d(1,i) = x(2,node1)
               xl2d(2,i) = x(3,node1)
               e_mechdisp2d(1,i) = mechdisp(2,node1)
               e_mechdisp2d(2,i) = mechdisp(3,node1)
            elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
               xl2d(1,i) = x(3,node1)
               xl2d(2,i) = x(1,node1)
               e_mechdisp2d(1,i) = mechdisp(3,node1)
               e_mechdisp2d(2,i) = mechdisp(1,node1)
            elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
               xl2d(1,i) = x(1,node1)
               xl2d(2,i) = x(2,node1)
               e_mechdisp2d(1,i) = mechdisp(1,node1)
               e_mechdisp2d(2,i) = mechdisp(2,node1)
            endif
         end do

         g = 1.0d0/((3.0d0)**(0.5d0))
         do i = 1,4
            sg2(1,i) = ig(i)*g
            sg2(2,i) = jg(i)*g
            sg2(3,i) = 1.0d0
         end do

         do i = 1,4 ! lint
            call shp2d_4(shp2,xsj2,xl2d,sg2,2,4,i)
            xsj2(i) = xsj2(i)*sg2(3,i)
         end do

         do i = 1,4
            do j = 1,3
               do k = 1,3
                  f2(k,j,i) = 0.0d0
                  fi2(k,j,i) = 0.0d0
                  c2(k,j,i) = 0.0d0
                  ci2(k,j,i) = 0.0d0
                  f2d(k,j,i) = 0.0d0
                  fi2d(k,j,i) = 0.0d0
                  c2d(k,j,i) = 0.0d0
                  ci2d(k,j,i) = 0.0d0
               end do
            end do
         end do
         
         do i = 1,4 !lint
            detfi2(i) = 0.0d0
         end do

         do i = 1,4 ! lint
            call kine2d_lag(shp2,e_mechdisp2d,f2,fi2,c2,ci2,detfi2,i)
         end do

         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            do j = 1,4
               if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
                  shp2d(1,j,i) = 0.0d0
                  shp2d(2,j,i) = shp2(1,j,i)
                  shp2d(3,j,i) = shp2(2,j,i)
                  shp2d(4,j,i) = shp2(3,j,i)
                  
               elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
                  shp2d(1,j,i) = shp2(2,j,i)
                  shp2d(2,j,i) = 0.0d0
                  shp2d(3,j,i) = shp2(1,j,i)
                  shp2d(4,j,i) = shp2(3,j,i)
                  
               elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
                  shp2d(1,j,i) = shp2(1,j,i)
                  shp2d(2,j,i) = shp2(2,j,i)
                  shp2d(3,j,i) = 0.0d0
                  shp2d(4,j,i) = shp2(3,j,i)                  
                  
               endif
            end do
         end do

c$    rearrange F
         ndirec = ix_nbsurf(6,nb)
         do i = 1,4
            if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
                  f2d(1,1,i) = 1.0d0
                  f2d(2,2,i) = f2(1,1,i)
                  f2d(2,3,i) = f2(1,2,i)
                  f2d(3,2,i) = f2(2,1,i)
                  f2d(3,3,i) = f2(2,2,i)
               
            elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
                  f2d(1,1,i) = f2(2,2,i)
                  f2d(1,3,i) = f2(2,1,i)
                  f2d(2,2,i) = 1.0d0
                  f2d(3,1,i) = f2(1,2,i)
                  f2d(3,3,i) = f2(1,1,i)
               
            elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
                  f2d(1,1,i) = f2(1,1,i)
                  f2d(1,2,i) = f2(1,2,i)
                  f2d(2,1,i) = f2(2,1,i)
                  f2d(2,2,i) = f2(2,2,i)
                  f2d(3,3,i) = 1.0d0
               
            endif
         end do

c     get : detfi2,Fi, C, Ci
         do i = 1,4 ! lint
            call kine2d_lag_1(shp2,e_mechdisp2d,
     &           f2d,fi2d,c2d,ci2d,detfi2,i)
         end do

         call natural_bc_d(
     &        nbc_vector_e,nb,
     &        ix_nbsurf,
     &        shp2d,xsj2,
     &        4,2,nnbsurf,4,
     &        ep,mu,sigma,
     &        f2d,fi2d,c2d,ci2d,detfi2,
     &        e_flux_input)
         
         do i = 1,4
            grow = geqix(ix_nbsurf(i,nb))*4-3
            lrow = i
            valnbc=nbc_vector_e(lrow)!*2.5e16

      	 call VecSetValue(rhsem,grow-1,valnbc,ADD_VALUES,ierr) 

         end do
      end do

          call VecAssemblyBegin (rhsem, ierr)
          call VecAssemblyEnd   (rhsem, ierr)


c         RHS Vector viewer

!            call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"rhs.m",B_view,
!     &                                ierr)
!            call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
!     &                                ierr)
!            call VecView             (rhsem,B_view,ierr)
!            call PetscViewerDestroy  (B_view,ierr)


!-----KSP solver for the system (Jacobian preconditioner)-----

         call KSPCreate        (PETSC_COMM_WORLD, kspsolem,ierr)
         call KSPSetOperators  (kspsolem, Kmatem, Kmatem,
     &                          DIFFERENT_NONZERO_PATTERN,ierr)
         call KSPGetPC(kspsolem,pcem,ierr)
         call PCSetType(pcem,PCBJACOBI,ierr)
         tol = 1.e-20
         call KSPSetTolerances(kspsolem,tol,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_INTEGER,ierr) 
         call KSPSetFromOptions(kspsolem,ierr)

         call KSPSolve (kspsolem, rhsem, solem, ierr)
         
!            call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"solem.m",B_view,
!     &                                ierr)
!            call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
!     &                                ierr)
!            call VecView             (solem,B_view,ierr)
!            call PetscViewerDestroy  (B_view,ierr)
!!!!!!!!!!!!!!! Restore Phi and update for the next time step!!!!!!!!!!!!!!!!!!!
         call VecGetArray (solem, p_n_t1, i_x, ierr)
         do i = 1, numpn 
          j = (i-1)*4+1
          val = p_n_t1(i_x + j)
          p_n_t2 (i) = val
         end do
         call VecRestoreArray (solem,p_n_t2,i_x,ierr)
         
          pmaxr = 0
          pmaxs = 0
          do i = 0, ntasks-1
            pmaxr = max( pmaxr, mr(np(248)+i) )
            pmaxs = pmaxs + mr(np(250)+i)
          end do
          pmaxr = pmaxr*1
          pmaxs = pmaxs*1
          ndf = 1
          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
          call psetb (p_n_t2,mr(np(248)),mr(np(249)),
     &                mr(np(250)),mr(np(251)),mr(np( 31)),
     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
          setvar = palloc( 120, 'TEMP0',    0, 2)



         do i = 1, numnp
         ans_phi(i)=p_n_t2(i)
         end do
         
         


         do i = 1, numnp
!         d_phi_n(i)  = (1.0d0/dt)*(ans_phi(i)-phi_n(i))
         phi_n(i) = 0.0d0
         end do

!!!!!!!!!!!!!!!! Restore A1 and update for the next time step!!!!!!!!!!!!!!!!!!!
         
         call VecGetArray (solem, a_n_t1, i_y, ierr)
         do i =1, numpn
          j = (i-1)*4+2
          vala = a_n_t1(i_y + j)
          a_n_t2 (i) = vala
         end do
         call VecRestoreArray (solem,a_n_t2,i_y,ierr)
         
          pmaxr = 0
          pmaxs = 0
          do i = 0, ntasks-1
            pmaxr = max( pmaxr, mr(np(248)+i) )
            pmaxs = pmaxs + mr(np(250)+i)
          end do
          pmaxr = pmaxr*1
          pmaxs = pmaxs*1
          ndf = 1
          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
          call psetb (a_n_t2,mr(np(248)),mr(np(249)),
     &                mr(np(250)),mr(np(251)),mr(np( 31)),
     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
          setvar = palloc( 120, 'TEMP0',    0, 2)

         do i = 1, numnp
         ans_a(1,i)=a_n_t2(i)!*10000.0
         end do

!!!!!!!!!!!!!!!! Restore A2 and update for the next time step!!!!!!!!!!!!!!!!!!!

         call VecGetArray (solem, a_n_t3, i_y, ierr)
         do i =1, numpn
          j = (i-1)*4+3
          vala = a_n_t3(i_y + j)
          a_n_t4 (i) = vala
         end do
         call VecRestoreArray (solem,a_n_t4,i_y,ierr)
         
          pmaxr = 0
          pmaxs = 0
          do i = 0, ntasks-1
            pmaxr = max( pmaxr, mr(np(248)+i) )
            pmaxs = pmaxs + mr(np(250)+i)
          end do
          pmaxr = pmaxr*1
          pmaxs = pmaxs*1
          ndf = 1
          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
          call psetb (a_n_t4,mr(np(248)),mr(np(249)),
     &                mr(np(250)),mr(np(251)),mr(np( 31)),
     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
          setvar = palloc( 120, 'TEMP0',    0, 2)

         do i = 1, numnp
         ans_a(2,i)=a_n_t4(i)!*10000.0
         end do

!!!!!!!!!!!!!!!! Restore A3 and update for the next time step!!!!!!!!!!!!!!!!!!!
 
         call VecGetArray (solem, a_n_t5, i_y, ierr)
         do i =1, numpn
          j = (i-1)*4+4
          vala = a_n_t5(i_y + j)
          a_n_t6 (i) = vala
         end do
         call VecRestoreArray (solem,a_n_t6,i_y,ierr)
         
          pmaxr = 0
          pmaxs = 0
          do i = 0, ntasks-1
            pmaxr = max( pmaxr, mr(np(248)+i) )
            pmaxs = pmaxs + mr(np(250)+i)
          end do
          pmaxr = pmaxr*1
          pmaxs = pmaxs*1
          ndf = 1
          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
          call psetb (a_n_t6,mr(np(248)),mr(np(249)),
     &                mr(np(250)),mr(np(251)),mr(np( 31)),
     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
          setvar = palloc( 120, 'TEMP0',    0, 2)

         do i = 1, numnp
         ans_a(3,i)=a_n_t6(i)!*10000.0
         end do

!!!!!!!!!!!!!!! Find phi in current configuration !!!!!!!!!!!!!!!
!         do i =1, numnp
!          phicurv(i) = 0.0d0
!          phicur(i) = 0.0d0
!         end do
!
!      call fidotvector(phicur,mechvel,
!     &     numnp,numel,ndm,nel,lint,
!     &     x,ix,ans_a,ans_phi,
!     &     mechdisp)
!         do i =1, numpn
!          phicurv(i) = phicur(i)
!         end do
!
!          pmaxr = 0
!          pmaxs = 0
!          do i = 0, ntasks-1
!            pmaxr = max( pmaxr, mr(np(248)+i) )
!            pmaxs = pmaxs + mr(np(250)+i)
!          end do
!          pmaxr = pmaxr*1
!          pmaxs = pmaxs*1
!          ndf = 1
!          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
!          call psetb (phicurv,mr(np(248)),mr(np(249)),
!     &                mr(np(250)),mr(np(251)),mr(np( 31)),
!     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
!          setvar = palloc( 120, 'TEMP0',    0, 2)
!
!!!!!!!!!!!!!!! Restore A and Derivatives of A!!!!!!!!!!!!!!!!!!!
         
         do i = 1, numnp
           do j = 1, 3
!              dd_a_n(j,i) = (1.0d0/(dt**2.0d0))*
!     &                      (ans_a(j,i)-2*a_n(j,i)+a_n_1(j,i))
!              d_a_n(j,i)  = (1.0d0/dt)*(ans_a(j,i)-a_n(j,i))
              a_n_1(j,i)  = a_n(j,i)
!              a_n(j,i)  = ans_a(j,i)
           end do
         end do
         
!!!!!!!!!!!!!!!       Find Gradient of Phi        !!!!!!!!!!!!!!!
!      call grad_scalar(gradphi,phi_n,x,ix,numnp,numel)
!!!!!!!!!!!!!!!       Find Curl of A        !!!!!!!!!!!!!!!
!      call curl_vector(b,a_n,x,ix,numnp,numel)
!         do i = 1, numpn
!         ans_d1(i)=b(1,i)
!         end do
!         do i = 1, numpn
!         ans_d2(i)=b(2,i)
!         end do
!         do i = 1, numpn
!         ans_d3(i)=b(3,i)
!         end do
!!!!!!!!!!!!!!!!       Find Finv X Vector        !!!!!!!!!!!!!!!
!      call fidotvector(fivel,mechvel,
!     &     numnp,numel,ndm,nel,lint,
!     &     x,ix,
!     &     mechdisp)
!!!!!!!!!!!!!!!!       Find velocity cross B     !!!!!!!!!!!!!!!
!      call cross_product(velb,fivel,b,numnp)
!
!c     e field tmp
!      call get_efield(e,gradphi,d_a_n,numnp)
!
!c     e_tilda field
!      call get_efield_tilda(etilda,e,velb,numnp)
!
!c     e field in current
!      call vectordotfi(d,etilda,
!     &     numnp,numel,ndm,nel,lint,
!     &     x,ix,mat_prop,
!     &     mechdisp)
!
!!         do i = 1, numpn
!!         ans_d1(i)=d(1,i)
!!         end do
!!         do i = 1, numpn
!!         ans_d2(i)=e(2,i)
!!         end do
!!         do i = 1, numpn
!!         ans_d3(i)=d(3,i)
!!         end do
!
!          pmaxr = 0
!          pmaxs = 0
!          do i = 0, ntasks-1
!            pmaxr = max( pmaxr, mr(np(248)+i) )
!            pmaxs = pmaxs + mr(np(250)+i)
!          end do
!          pmaxr = pmaxr*1
!          pmaxs = pmaxs*1
!          ndf = 1
!          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
!          call psetb (ans_d1,mr(np(248)),mr(np(249)),
!     &                mr(np(250)),mr(np(251)),mr(np( 31)),
!     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
!          setvar = palloc( 120, 'TEMP0',    0, 2)
!
!          pmaxr = 0
!          pmaxs = 0
!          do i = 0, ntasks-1
!            pmaxr = max( pmaxr, mr(np(248)+i) )
!            pmaxs = pmaxs + mr(np(250)+i)
!          end do
!          pmaxr = pmaxr*1
!          pmaxs = pmaxs*1
!          ndf = 1
!          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
!          call psetb (ans_d2,mr(np(248)),mr(np(249)),
!     &                mr(np(250)),mr(np(251)),mr(np( 31)),
!     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
!          setvar = palloc( 120, 'TEMP0',    0, 2)
!
!          pmaxr = 0
!          pmaxs = 0
!          do i = 0, ntasks-1
!            pmaxr = max( pmaxr, mr(np(248)+i) )
!            pmaxs = pmaxs + mr(np(250)+i)
!          end do
!          pmaxr = pmaxr*1
!          pmaxs = pmaxs*1
!          ndf = 1
!          setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
!          call psetb (ans_d3,mr(np(248)),mr(np(249)),
!     &                mr(np(250)),mr(np(251)),mr(np( 31)),
!     &                ndf, hr(np(120)),hr(np(120)+pmaxr))
!          setvar = palloc( 120, 'TEMP0',    0, 2)
!!      open (unit = 27, file = conname, status = 'unknown'
!!     &      ,position='rewind')
!!          do i =1,numnp
!!          write(27,*) phicurv(i)
!!          end do
!!!!!!!!!!!!!!!! Write tecplot files!!!!!!!!!!!!!!!!!!!
!
!      open (unit = 28, file = bcname, status = 'unknown'
!     &      ,position='rewind')
!!      write(28,*) 'VARIABLES = "X\",\"Y\",\"Z\",\"Phi\"'
!      write(28,*) 'VARIABLES = "X\",\"Y\",\"Z\",\"E\",\"A1\"
!     &,"A2\","A3\"'
!      write(28,*) 'ZONE T=','"',2,'"',',N=',numnp, ', E=',numel,
!     &             'StrandID=',rank+1, 'SolutionTime=',ts, 
!     &             ', DATAPACKING=POINT, ZONETYPE=FEBRICK'
!            do i =1,numnp
!        write(28,"7e25.15") ((x(j,i)+mechdisp(j,i)),j=1,3),
!     &                       ans_d1(i),ans_d2(i),ans_d3(i),a_n(2,i)
!!        write(28,"6e25.15") ((x(j,i)+mechdisp(j,i)),j=1,3),
!!     &                        ans_d1(i),ans_d2(i),ans_d3(i)
!!          write(28,"4e25.15") x(1,i),x(2,i),x(3,i),ans_phi(i)
!!        write(28,"4e25.15") ((x(j,i)+mechdisp(j,i)),j=1,3),ans_d2(i)
!        end do
!          do i =1,numel
!            write(28,"8i7") (ix(j,i), j=1,8)
!          end do
!!!!!!!!!!!!!!! Write tecplot files!!!!!!!!!!!!!!!!!!!
      call KSPDestroy(kspsolem,ierr)
      call VecDestroy(solem,ierr)
      call VecDestroy(rhsem,ierr)
      call MatDestroy(Kmatem,ierr)

      end

