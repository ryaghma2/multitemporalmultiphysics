
       subroutine calc_quadratic_reduced_eqn6(
     &		pi_rec_tmp,dpi_rec_tmp,
     &		a_rec_tmp,da_rec_tmp,
     &		ytp_tmp,dy_tmp,yc_tmp,
     &		niter,nitermax,dn,tol_y0,tolc,
     &		rr,ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		y02,y01,yc,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)

	implicit   none

#     include   "finclude/petsc.h"
#     include   "pfeapb.h"
#     include   "time_flag.h"
#     include   "pointer.h"
#     include   "comfil.h"
#     include   "comblk.h"
#     include   "setups.h"

	integer :: nphi,na
	integer :: nb,node1
	integer :: eqn,lnode,gnode
	integer :: ig(4),jg(4)
	integer :: grow,lrow,kjj,iloc,jloc

	real*8  :: t
	real*8  :: ep,mu,sigma,g

	real*8  :: e_y0(64)
	real*8  :: disp_pre(3,numnp),disp_1_pre(3,numnp)
	real*8  :: vel_pre(3,numnp),acc_pre(3,numnp)
	real*8  :: disp_cur(3,numnp),disp_1_cur(3,numnp)
	real*8  :: vel_cur(3,numnp),acc_cur(3,numnp)
	real*8  :: tmax
	real*8  :: maxy0,maxr0,sumy0,sumr0

!!!!!!!!!!! data transformation between processors !!!!!!!!!!!
	logical :: setvar, palloc
	integer :: pmaxr, pmaxs
	integer :: ndf

!!!!!!!!!!!! current configuration info!!!!!!!!!!!!!!!!!!!
	real*8 ::  phicur(numnp),phicurv(numnp)

!!!!!!!!!!!!    post processing        !!!!!!!!!!!!!!!!!!!
	real*8 ::  gradphi(3,numnp)
	real*8 ::  velb(3,numnp),etilda(3,numnp)
	real*8 ::  veld(3,numnp),h_tmp(3,numnp)
	real*8 ::  fivel(3,numnp)
	real*8 ::  df(3,numnp),bf(3,numnp),ef(3,numnp),hf(3,numnp)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	integer :: gnodeix(8,numel), geqnix(8,numel) !connectivity of nodes and equations
	integer :: geqix(numnp)
	integer :: p,q
	PetscScalar :: valp,valq
	PetscScalar :: valnbc
	real*8  :: val,vala
	PetscScalar :: xc_PS(1)
	real*8  :: xc_pfv(numnp)	
	real*8  :: xc(8*numnp*ndiv)
	
	real*8  :: ans_xc_p(numnp*ndiv)	
	real*8  :: ans_xc_dp(numnp*ndiv)	
	real*8  :: ans_xc_a1(numnp*ndiv)	
	real*8  :: ans_xc_da1(numnp*ndiv)	
	real*8  :: ans_xc_a2(numnp*ndiv)	
	real*8  :: ans_xc_da2(numnp*ndiv)	
	real*8  :: ans_xc_a3(numnp*ndiv)	
	real*8  :: ans_xc_da3(numnp*ndiv)	

	real*8  :: ans_xcw_p(numnp*nwav)	
	real*8  :: ans_xcw_dp(numnp*nwav)	
	real*8  :: ans_xcw_a1(numnp*nwav)	
	real*8  :: ans_xcw_da1(numnp*nwav)	
	real*8  :: ans_xcw_a2(numnp*nwav)	
	real*8  :: ans_xcw_da2(numnp*nwav)	
	real*8  :: ans_xcw_a3(numnp*nwav)	
	real*8  :: ans_xcw_da3(numnp*nwav)	
	
	PetscScalar :: dy_PS(1)
	real*8  :: dy_pfv(numnp)
	
	integer :: ldem(4*8,5)
	real*8  :: ub(4*8)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	PetscViewer    K_view, B_view
	PetscErrorCode   ierr
	PetscOffset i_x

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Vec rG,dy0G,drG,dy0Gm,rGm,mrG,dy0Gr,dy0Gs
	Vec invGmatl,invGmatr,inv0lG,inv0rG
	Mat invGmat,BGmat

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	integer :: ix(17,*),jind,ic,jc,ip,nc,dni
	real*8  :: sbc_pi(2,nsbc_pi),sbc_a(2,nsbc_a)
	integer :: ix_nbsurf(6,nnbsurf),ix_nbsurfa(6,nnbsurfa)
	integer :: ierror,icyc,i,j,k,l,m,n,nx
	integer :: ii,i1,ii1,jj
	integer :: iflag,niter
	integer :: nitermax,dn,dn1
	integer :: numnp,nnp,numel,ndm,nsbc_pi,nsbc_a
	integer :: nel,lint
	integer :: nnbsurf,nnbsurfa,ndiv,nwav
	integer :: ts,tsj,ngp,iflaginv,node
	integer :: counter2,counter3,row,col,row1,col1

	real*8  :: x(3,numnp)
	real*8  :: y02(8*numnp),y01(8*numnp)
	real*8  :: yc(8*numnp*ndiv)
	real*8  :: dbdec(ndiv,ndiv)
	real*8  :: dbrec(ndiv,ndiv),dmatint(ndiv,ndiv)
	real*8  :: dbreci(ndiv,ndiv),dmatinti(ndiv,ndiv)
	real*8  :: dbrecr(ndiv,nwav),dmatintr(ndiv,nwav)
	integer :: dbind(nwav)

	real*8  :: den,fr1,fr2,fr3,rr,tpe,tpu,dt
	real*8  :: mag_em,frq_em,w_em,mag_me,frq_me,w_me
	real*8  :: mat_prop(3,2)
	real*8  :: n_y0(1,1)
	real*8  :: y0_p(1,1),y0_dp(1,1)
	real*8  :: y0_a1(1,1),y0_da1(1,1)
	real*8  :: y0_a2(1,1),y0_da2(1,1)
	real*8  :: y0_a3(1,1),y0_da3(1,1)
	real*8  :: tol_y0,v1(1,1),ds,di,dj,tolc,ds2
	real*8  :: disp(3*ndiv,numnp),disp_1(3*ndiv,numnp)
	real*8  :: vel(3*ndiv,numnp),acc(3*ndiv,numnp)
	real*8  :: r0_err

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real*8  :: mechvel(3,numnp),mechacc(3,numnp)
	real*8  :: mechdisp(3,numnp),mechdisp_1(3,numnp)
	real*8  :: yc_0(8*numnp)
	real*8  :: yt0(8*numnp)
	real*8  :: ytp(8*numnp)
	
	real*8  :: r0(8*numnp),r0prev(8*numnp)
	real*8  :: dyt0(8*numnp)
	real*8  :: r0tmp(8*numnp)
	PetscScalar :: r0tmp_PS(1)
	real*8  :: r0tmp_pfv(numnp)
	real*8  :: rgmp(8*numnp)
	PetscScalar :: rgm_PS(1)
	real*8  :: rgm_pfv(numnp)
	real*8  :: dy0gmp(8*numnp)
	PetscScalar :: dy0gm_PS(1)
	real*8  :: dy0gm_pfv(numnp)
	real*8  :: dy0grp(8*numnp)
	PetscScalar :: dy0gr_PS(1)
	real*8  :: dy0gr_pfv(numnp)
	real*8  :: inv0l(8*numnp)
	PetscScalar :: inv0l_PS(1)
	real*8  :: inv0l_pfv(numnp)
	real*8  :: inv0r(8*numnp)
	PetscScalar :: inv0r_PS(1)
	real*8  :: inv0r_pfv(numnp)

	real*8  :: rtl(8*numnp)
		
	PetscScalar :: dot
	PetscReal :: idot,sdot,rdot,sdot2
	
	real*8  :: tc(ndiv)
	real*8  :: pi_rec_tmp(numnp,ndiv+1)
	real*8  :: dpi_rec_tmp(numnp,ndiv+1)
	real*8  :: a_rec_tmp(3*ndiv+1,numnp)
	real*8  :: da_rec_tmp(3*ndiv+1,numnp)
	real*8  :: ytp_tmp(8*numnp)
	real*8  :: y0_tmp(8*numnp)
	real*8  :: dy_tmp(8*numnp)
	real*8  :: yc_tmp(8*numnp*ndiv)
	real*8  :: vrec(1,ndiv)
	real*8  :: n_xc_p(ndiv,1)
	real*8  :: n_xc_dp(ndiv,1)
	real*8  :: n_xc_a1(ndiv,1),n_xc_a2(ndiv,1),n_xc_a3(ndiv,1)
	real*8  :: n_xc_da1(ndiv,1),n_xc_da2(ndiv,1),n_xc_da3(ndiv,1)
	real*8  :: a_n_1(ndm,numnp),a_n(ndm,numnp)
	real*8  :: d_a_n(ndm,numnp),dd_a_n(ndm,numnp)
	real*8  :: phi_n(numnp),d_phi_n(numnp)

	integer :: idiv
	integer :: lext,exced_m,exced_k
	integer :: mloc,kloc
	integer :: mnode,knode
	
	real*8  :: t1,t2,t3,t4,dts
	integer ::	xi,xp,il,ir,kn
	real*8  ::	fni,fnp,tf
	real*8  :: yt0e(8*numnp)

	real*8  :: pmat(numnp,ndiv)
	real*8  :: dpmat(numnp,ndiv)
	real*8  :: amat(3*numnp,ndiv)
	real*8  :: damat(3*numnp,ndiv)
	real*8  :: d_phi_n_1(numnp)
	real*8  :: n_pt(ndiv),n_pc(ndiv)
	real*8  :: n_dpt(ndiv),n_dpc(ndiv)
	real*8  :: n_a1t(ndiv),n_a1c(ndiv)
	real*8  :: n_da1t(ndiv),n_da1c(ndiv)
	real*8  :: n_a2t(ndiv),n_a2c(ndiv)
	real*8  :: n_da2t(ndiv),n_da2c(ndiv)
	real*8  :: n_a3t(ndiv),n_a3c(ndiv)
	real*8  :: n_da3t(ndiv),n_da3c(ndiv)

	real*8  :: ltilde1(8*numnp),ltilde2(8*numnp)

	save


	data       ig/-1,1,1,-1/,jg/-1,-1,1,1/
	

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	START

c	INITIALIZE

	nel=8
	ndm=3

	lint=8
	nnp=numnp
	nphi=nsbc_pi
	na=nsbc_a

	ierror=0
	iflag=0
	niter=0

	yt0=y01

	den=(rr+1)**(2.0d0)-1
	fr1=((rr+1)**(2.0d0))/den
	fr2=1.0d0/den
	fr3=((rr+1)**(2.0d0)-(rr+1))/den

	dbreci=0.d0
	do i=1,ndiv
		dbreci(i,i)=1.d0
	enddo

	dmatinti=0.d0
	do i=1,ndiv
		dmatinti(i,i)=1.d0
	enddo

	il=8*numpn*nitermax
	call VecCreate        (PETSC_COMM_WORLD, invGmatl, ierr)
	call VecSetSizes      (invGmatl, il, PETSC_DECIDE, ierr)
	call VecSetFromOptions(invGmatl, ierr)
	ir=8*numpn*nitermax
	call VecCreate        (PETSC_COMM_WORLD, invGmatr, ierr)
	call VecSetSizes      (invGmatr, ir, PETSC_DECIDE, ierr)
	call VecSetFromOptions(invGmatr, ierr)

	do while ( iflag.eq.0 .and. niter.lt.nitermax )

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	XC

	do i=1,8*nnp
		yt0e(i)=yt0(i)
	enddo

!	if (niter.lt.3) then
	if (numtn.lt.225) then

	call calc_reduced_mts1(
     &		ans_xcw_p,ans_xcw_dp,
     &		ans_xcw_a1,ans_xcw_da1,
     &		ans_xcw_a2,ans_xcw_da2,
     &		ans_xcw_a3,ans_xcw_da3,
     &		ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		yt0e,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)

!	call calc_reduced_mts2(
!     &		ans_xcw_p,ans_xcw_dp,
!     &		ans_xcw_a1,ans_xcw_da1,
!     &		ans_xcw_a2,ans_xcw_da2,
!     &		ans_xcw_a3,ans_xcw_da3,
!     &		ndiv,icyc,ts,dt,
!     &		dbdec,dbrec,dmatint,
!     &		dbrecr,dmatintr,nwav,dbind,
!     &		yt0e,
!     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
!     &		x,ix,mat_prop,
!     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
!     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
!     &		disp,disp_1,vel,acc,
!     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
!     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
!     &		numnp,numel,ndm,
!     &		gnodeix,geqnix,geqix)

	else

	call calc_reduced_gts1(
     &		ans_xcw_p,ans_xcw_dp,
     &		ans_xcw_a1,ans_xcw_da1,
     &		ans_xcw_a2,ans_xcw_da2,
     &		ans_xcw_a3,ans_xcw_da3,
     &		ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		yt0e,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)

!	call calc_reduced_gts2(
!     &		ans_xcw_p,ans_xcw_dp,
!     &		ans_xcw_a1,ans_xcw_da1,
!     &		ans_xcw_a2,ans_xcw_da2,
!     &		ans_xcw_a3,ans_xcw_da3,
!     &		ndiv,icyc,ts,dt,
!     &		dbdec,dbrec,dmatint,
!     &		dbrecr,dmatintr,nwav,dbind,
!     &		yt0e,
!     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
!     &		x,ix,mat_prop,
!     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
!     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
!     &		disp,disp_1,vel,acc,
!     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
!     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
!     &		numnp,numel,ndm,
!     &		gnodeix,geqnix,geqix)

	endif
!	endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	ans_xc_p=0.d0
	ans_xc_dp=0.d0
	ans_xc_a1=0.d0
	ans_xc_da1=0.d0
	ans_xc_a2=0.d0
	ans_xc_da2=0.d0
	ans_xc_a3=0.d0
	ans_xc_da3=0.d0
	do ic=1,nwav
	do i=1,numnp
	ip=dbind(ic)
	ans_xc_p((i-1)*ndiv+ip)=ans_xcw_p((i-1)*nwav+ic)
	ans_xc_dp((i-1)*ndiv+ip)=ans_xcw_dp((i-1)*nwav+ic)
	ans_xc_a1((i-1)*ndiv+ip)=ans_xcw_a1((i-1)*nwav+ic)
	ans_xc_da1((i-1)*ndiv+ip)=ans_xcw_da1((i-1)*nwav+ic)
	ans_xc_a2((i-1)*ndiv+ip)=ans_xcw_a2((i-1)*nwav+ic)
	ans_xc_da2((i-1)*ndiv+ip)=ans_xcw_da2((i-1)*nwav+ic)
	ans_xc_a3((i-1)*ndiv+ip)=ans_xcw_a3((i-1)*nwav+ic)
	ans_xc_da3((i-1)*ndiv+ip)=ans_xcw_da3((i-1)*nwav+ic)
	enddo
	enddo

	do i = 1,numnp
	do kjj=1,ndiv	
	xc(8*ndiv*(i-1)+kjj)=ans_xc_p(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+ndiv+kjj)=ans_xc_dp(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+2*ndiv+kjj)=ans_xc_a1(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+3*ndiv+kjj)=ans_xc_da1(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+4*ndiv+kjj)=ans_xc_a2(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+5*ndiv+kjj)=ans_xc_da2(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+6*ndiv+kjj)=ans_xc_a3(ndiv*(i-1)+kjj)
	xc(8*ndiv*(i-1)+7*ndiv+kjj)=ans_xc_da3(ndiv*(i-1)+kjj)
	enddo
	enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	ytp

	vrec(1,1:ndiv)=dmatint(ndiv,1:ndiv)
	yc_0=0.0d0

	do i=1,nnp
	n_xc_p(1:ndiv,1)=ans_xc_p((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_p)
	yc_0(8*i-7)=n_y0(1,1)
	n_xc_dp(1:ndiv,1)=ans_xc_dp((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_dp)
	yc_0(8*i-6)=n_y0(1,1)
	n_xc_a1(1:ndiv,1)=ans_xc_a1((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a1)
	yc_0(8*i-5)=n_y0(1,1)
	n_xc_da1(1:ndiv,1)=ans_xc_da1((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da1)
	yc_0(8*i-4)=n_y0(1,1)
	n_xc_a2(1:ndiv,1)=ans_xc_a2((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a2)
	yc_0(8*i-3)=n_y0(1,1)
	n_xc_da2(1:ndiv,1)=ans_xc_da2((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da2)
	yc_0(8*i-2)=n_y0(1,1)
	n_xc_a3(1:ndiv,1)=ans_xc_a3((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a3)
	yc_0(8*i-1)=n_y0(1,1)
	n_xc_da3(1:ndiv,1)=ans_xc_da3((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da3)
	yc_0(8*i)=n_y0(1,1)
	enddo

	ytp=yt0+yc_0

	dn1=dn
	r0=(1+fr3*dn1)*yt0-fr3*dn1*ytp-fr1*y01+fr2*y02

	call VecCreate        (PETSC_COMM_WORLD, rG, ierr)
	call VecSetSizes      (rG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(rG, ierr)

	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (rG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (rG, i, val,INSERT_VALUES,ierr)
	endif
	enddo
	enddo

	call VecAssemblyBegin (rG, ierr)
	call VecAssemblyEnd   (rG, ierr)
	call VecDot   (rG,rG,dot, ierr)
	rdot=PetscRealPart(dot)	
	r0_err=1.d0/dn*(rdot)
	write(*,*) 'r0_err=', r0_err

	if ( (r0_err.lt.tol_y0) .and. (niter.ge.4)) then
	iflag=1
	else

	if ( niter .eq. 0 ) then

	dj=1.d0/(1+fr3*dn1)

	else

	call VecCreate   (PETSC_COMM_WORLD, drG, ierr)
	call VecSetSizes(drG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(drG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0(8*(lnode-1)+(2*lext-1))-r0prev(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (drG, i, val,INSERT_VALUES,ierr)				
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0(8*(lnode-1)+(2*lext  ))-r0prev(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (drG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (drG, ierr)
	call VecAssemblyEnd   (drG, ierr)

	do i=1,8*numnp
	r0tmp(i)=dj*(r0(i)-r0prev(i))
	enddo

	do kn=1,niter-1
	do lext=1,8
	call VecGetArray (invGmatr, inv0r_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0r_PS(i_x + j)
	inv0r_pfv (i) = val
	end do
	call VecRestoreArray (invGmatr,inv0r_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0r_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0r ((i-1)*8+lext)= inv0r_pfv(i)
	enddo
	enddo
	call VecCreate        (PETSC_COMM_WORLD, inv0rG, ierr)
	call VecSetSizes      (inv0rG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(inv0rG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (inv0rG, ierr)
	call VecAssemblyEnd   (inv0rG, ierr)
	call VecDot   (inv0rG,drG,dot, ierr)
	idot=PetscRealPart(dot)
	di=idot
	call VecDestroy(inv0rG, ierr)
	do lext=1,8
	call VecGetArray (invGmatl, inv0l_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0l_PS(i_x + j)
	inv0l_pfv (i) = val
	end do
	call VecRestoreArray (invGmatl,inv0l_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0l_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0l ((i-1)*8+lext)= inv0l_pfv(i)
	enddo
	enddo
	do i=1,8*numnp
	r0tmp(i)=r0tmp(i)+di*inv0l(i)
	enddo
	enddo



	call VecCreate        (PETSC_COMM_WORLD, rGm, ierr)
	call VecSetSizes      (rGm, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(rGm, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0tmp(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (rGm, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = r0tmp(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (rGm, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (rGm, ierr)
	call VecAssemblyEnd   (rGm, ierr)
	call VecCreate        (PETSC_COMM_WORLD, dy0Gm, ierr)
	call VecSetSizes      (dy0Gm, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(dy0Gm, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n)
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dyt0(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (dy0Gm, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dyt0(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (dy0Gm, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (dy0Gm, ierr)
	call VecAssemblyEnd   (dy0Gm, ierr)	
	call VecDot   (rGm,dy0Gm,dot, ierr)
	sdot=PetscRealPart(dot)	
	ds=sdot
	call VecCreate        (PETSC_COMM_WORLD, dy0Gr, ierr)
	call VecSetSizes      (dy0Gr, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(dy0Gr, ierr)
	call VecAssemblyBegin (dy0Gr, ierr)
	call VecAssemblyEnd   (dy0Gr, ierr)
	Call VecAXPY(dy0Gr,dj,dy0Gm,ierr)
	do lext=1,8
	call VecGetArray (rGm, rgm_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8 + lext
	val = rgm_PS(i_x + j)
	rgm_pfv (i) = val
	enddo
	call VecRestoreArray (rGm,rgm_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (rgm_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	rgmp ((i-1)*8+lext)= rgm_pfv(i)
	enddo
	enddo
	do lext=1,8
	call VecGetArray (dy0Gm, dy0gm_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8 + lext
	val = dy0gm_PS(i_x + j)
	dy0gm_pfv (i) = val
	enddo
	call VecRestoreArray (dy0Gm,dy0gm_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (dy0gm_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	dy0gmp ((i-1)*8+lext)= dy0gm_pfv(i)
	enddo
	enddo
	do lext=1,8
	call VecGetArray (dy0Gr, dy0gr_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8 + lext
	val = dy0gr_PS(i_x + j)
	dy0gr_pfv (i) = val
	enddo
	call VecRestoreArray (dy0Gr,dy0gr_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (dy0gr_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	dy0grp ((i-1)*8+lext)= dy0gr_pfv(i)
	enddo
	enddo







	do kn=1,niter-1
	do lext=1,8
	call VecGetArray (invGmatl, inv0l_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0l_PS(i_x + j)
	inv0l_pfv (i) = val
	end do
	call VecRestoreArray (invGmatl,inv0l_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0l_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0l ((i-1)*8+lext)= inv0l_pfv(i)
	enddo
	enddo
	call VecCreate        (PETSC_COMM_WORLD, inv0lG, ierr)
	call VecSetSizes      (inv0lG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(inv0lG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0l(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (inv0lG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0l(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (inv0lG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (inv0lG, ierr)
	call VecAssemblyEnd   (inv0lG, ierr)
	call VecDot   (inv0lG,dy0Gm,dot, ierr)
	idot=PetscRealPart(dot)
	di=idot
	call VecDestroy(inv0lG, ierr)
	do lext=1,8
	call VecGetArray (invGmatr, inv0r_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0r_PS(i_x + j)
	inv0r_pfv (i) = val
	enddo
	call VecRestoreArray (invGmatr,inv0r_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0r_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0r ((i-1)*8+lext)= inv0r_pfv(i)
	enddo
	enddo
	if (niter.ge.2) then
	call VecCreate        (PETSC_COMM_WORLD, inv0rG, ierr)
	call VecSetSizes      (inv0rG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(inv0rG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (inv0rG, ierr)
	call VecAssemblyEnd   (inv0rG, ierr)
	call VecDot   (rG,inv0rG,dot,ierr)
	sdot2=PetscRealPart(dot)
	ds2=-1.d0*sdot2
	call VecDestroy(inv0rG,ierr)
	endif
	do i=1,8*numnp
	dy0grp(i)=dy0grp(i)+di*inv0r(i)
	enddo
	enddo


	if (niter.lt.2) then
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	gnode=1+int(1.d0*(ldem(m,1)-1)/4.d0)
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = 1.d0/ds*(dy0gmp(8*(lnode-1)+(2*lext-1))-
     &		rgmp(8*(lnode-1)+(2*lext-1)))
	endif
	call VecSetValue (invGmatl, i, val,INSERT_VALUES,ierr)
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = 1.d0/ds*(dy0gmp(8*(lnode-1)+(2*lext  ))-
     &		rgmp(8*(lnode-1)+(2*lext  )))
	endif
	call VecSetValue (invGmatl, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	else

	nc=16
	call calc_its1(
     &		ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		y01,nc,dn,rtl,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)


!	call calc_its2(
!     &		ndiv,icyc,ts,dt,
!     &		dbdec,dbrec,dmatint,
!     &		dbrecr,dmatintr,nwav,dbind,
!     &		y01,nc,dn,rtl,
!     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
!     &		x,ix,mat_prop,
!     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
!     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
!     &		disp,disp_1,vel,acc,
!     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
!     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
!     &		numnp,numel,ndm,
!     &		gnodeix,geqnix,geqix)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

	do i=1,8*numnp
	ltilde1(i)=-1.d0*dj*r0(i)
	enddo

	do kn=1,niter-1
	do lext=1,8
	call VecGetArray (invGmatr, inv0r_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0r_PS(i_x + j)
	inv0r_pfv (i) = val
	end do
	call VecRestoreArray (invGmatr,inv0r_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0r_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0r ((i-1)*8+lext)= inv0r_pfv(i)
	enddo
	enddo
	call VecCreate        (PETSC_COMM_WORLD, inv0rG, ierr)
	call VecSetSizes      (inv0rG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(inv0rG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (inv0rG, ierr)
	call VecAssemblyEnd   (inv0rG, ierr)
	call VecDot   (inv0rG,rG,dot, ierr)
	idot=PetscRealPart(dot)
	di=idot
	call VecDestroy(inv0rG, ierr)
	do lext=1,8
	call VecGetArray (invGmatl, inv0l_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0l_PS(i_x + j)
	inv0l_pfv (i) = val
	end do
	call VecRestoreArray (invGmatl,inv0l_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0l_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0l ((i-1)*8+lext)= inv0l_pfv(i)
	enddo
	enddo
	do i=1,8*numnp
	ltilde1(i)=ltilde1(i)-di*inv0l(i)
	enddo
	enddo
	do i=1,8*numnp
	ltilde2(i)=rtl(i)-yt0(i)
	enddo
	call VecAssemblyBegin (dy0Gr, ierr)
	call VecAssemblyEnd   (dy0Gr, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dy0grp(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (dy0Gr, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dy0grp(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (dy0Gr, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecDot   (dy0Gr,rG,dot, ierr)
	idot=PetscRealPart(dot)
	di=idot


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11


	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n)
	if(ldem(m,1).gt.0) then
	gnode=1+int(1.d0*(ldem(m,1)-1)/4.d0)
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
!	val = 1.d0/ds2*(rtl(8*(lnode-1)+(2*lext-1))-
!     &		yt0(8*(lnode-1)+(2*lext-1)))
!!	val = (rtl(8*(lnode-1)+(2*lext-1))-
!!     &		yt0(8*(lnode-1)+(2*lext-1)))
	val = 1.d0/di*(ltilde1(8*(lnode-1)+(2*lext-1))-
     &		ltilde2(8*(lnode-1)+(2*lext-1)))
	endif
	call VecSetValue (invGmatl, i, val,INSERT_VALUES,ierr)				
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
!	val = 1.d0/ds2*(rtl(8*(lnode-1)+(2*lext  ))-
!     &		yt0(8*(lnode-1)+(2*lext  )))
!!	val = (rtl(8*(lnode-1)+(2*lext  ))-
!!     &		yt0(8*(lnode-1)+(2*lext  )))
	val = 1.d0/di*(ltilde1(8*(lnode-1)+(2*lext  ))-
     &		ltilde2(8*(lnode-1)+(2*lext  )))
	endif
	call VecSetValue (invGmatl, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	endif
	call VecAssemblyBegin (invGmatl, ierr)
	call VecAssemblyEnd   (invGmatl, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	gnode=1+int(1.d0*(ldem(m,1)-1)/4.d0)
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dy0grp(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (invGmatr, i, val,INSERT_VALUES,ierr)				
	i=8*nitermax*(gnode-1) + 2*(lext-1)*nitermax + nitermax + niter -1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dy0grp(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (invGmatr, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (invGmatr, ierr)
	call VecAssemblyEnd   (invGmatr, ierr)



	call VecDestroy(rGm,ierr)
	call VecDestroy(dy0Gm,ierr)
	call VecDestroy(dy0Gr,ierr)
	call VecDestroy(drG,ierr)


	endif		! end of iter#0


	call VecCreate (PETSC_COMM_WORLD, mrG, ierr)
	call VecSetSizes (mrG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(mrG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1				
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = -1.d0*r0(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (mrG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1				
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = -1.d0*r0(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (mrG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (mrG, ierr)
	call VecAssemblyEnd   (mrG, ierr)
	r0prev=r0
	call VecCreate (PETSC_COMM_WORLD, dy0Gs, ierr)
	call VecSetSizes (dy0Gs, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(dy0Gs, ierr)
	call VecAssemblyBegin (dy0Gs, ierr)
	call VecAssemblyEnd   (dy0Gs, ierr)



	do i=1,8*numnp
	dyt0(i)=-1.d0*dj*r0(i)
	enddo
	do kn=1,niter
	do lext=1,8
	call VecGetArray (invGmatr, inv0r_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0r_PS(i_x + j)
	inv0r_pfv (i) = val
	end do
	call VecRestoreArray (invGmatr,inv0r_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0r_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0r ((i-1)*8+lext)= inv0r_pfv(i)
	enddo
	enddo
	call VecCreate        (PETSC_COMM_WORLD, inv0rG, ierr)
	call VecSetSizes      (inv0rG, 8*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(inv0rG, ierr)
	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = inv0r(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (inv0rG, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (inv0rG, ierr)
	call VecAssemblyEnd   (inv0rG, ierr)
	call VecDot   (inv0rG,mrG,dot, ierr)
	idot=PetscRealPart(dot)
	di=idot
	call VecDestroy(inv0rG, ierr)
	do lext=1,8
	call VecGetArray (invGmatl, inv0l_PS, i_x, ierr)
	do i = 1,numpn
	j = (i-1)*8*nitermax + (lext-1)*nitermax + kn
	val = inv0l_PS(i_x + j)
	inv0l_pfv (i) = val
	end do
	call VecRestoreArray (invGmatl,inv0l_pfv,i_x,ierr)
	pmaxr = 0
	pmaxs = 0
	do i = 0, ntasks-1
	pmaxr = max( pmaxr, mr(np(248)+i) )
	pmaxs = pmaxs + mr(np(250)+i)
	enddo
	pmaxr = pmaxr
	pmaxs = pmaxs
	ndf = 1
	setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	call psetb (inv0l_pfv,mr(np(248)),mr(np(249)),
     &         mr(np(250)),mr(np(251)),mr(np( 31)),
     &         ndf, hr(np(120)),hr(np(120)+pmaxr))
	setvar = palloc( 120, 'TEMP0',    0, 2)
	do i = 1,numnp
	inv0l ((i-1)*8+lext)= inv0l_pfv(i)
	enddo
	enddo
	do i=1,8*numnp
	dyt0(i)=dyt0(i)+di*inv0l(i)
	enddo
!!	if (niter.lt.2) then
!!	do i=1,8*numnp
!!	dyt0(i)=dyt0(i)+di*inv0l(i)
!!	enddo
!!	else
!!	do i=1,8*numnp
!!	dyt0(i)=inv0l(i)
!!	enddo
!!	endif
	enddo


	do n = 1,numel
	call assconne(n,ix(1,n),geqnix(1,n),ldem)
	do m = 1, 4*nel
	k=int((m-1)/4.d0)+1
	lext=m-4*(k-1)
	lnode = ix(k,n) 
	if(ldem(m,1).gt.0) then
	i = (2*ldem(m,1)-1) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dyt0(8*(lnode-1)+(2*lext-1))
	endif
	call VecSetValue (dy0Gs, i, val,INSERT_VALUES,ierr)
	i = (2*ldem(m,1)  ) - 1
	if(ldem(m,5).ne.0) then
	val = 0.d0
	else
	val = dyt0(8*(lnode-1)+(2*lext  ))
	endif
	call VecSetValue (dy0Gs, i, val,INSERT_VALUES,ierr)
	endif
	enddo 
	enddo
	call VecAssemblyBegin (dy0Gs, ierr)
	call VecAssemblyEnd   (dy0Gs, ierr)


	call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"mrG.m",B_view,
     &                                ierr)
	call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
     &                                ierr)
	call VecView             (mrG,B_view,ierr)
	call PetscViewerDestroy  (B_view,ierr)

	call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"dy0Gs.m",B_view,
     &                                ierr)
	call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
     &                                ierr)
	call VecView             (dy0Gs,B_view,ierr)
	call PetscViewerDestroy  (B_view,ierr)

	yt0=yt0+dyt0
	call VecDestroy(dy0Gs,ierr)
	call VecDestroy(mrG,ierr)

	endif
	call VecDestroy(rG,ierr)


	niter=niter+1
	write(*,*) 'niter =',niter

	enddo		!end while loop

	ytp_tmp=ytp
!	ytp_tmp=yt0
	yc_tmp=xc
	dy_tmp=ytp-yt0

	do k=1,ndiv
	vrec(1,1:ndiv)=dmatint(k,1:ndiv)
	do i=1,nnp
	n_xc_p(1:ndiv,1)=ans_xc_p((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_p)
	pi_rec_tmp(i,k)=yt0(8*i-7) + n_y0(1,1)
	n_xc_dp(1:ndiv,1)=ans_xc_dp((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_dp)
	dpi_rec_tmp(i,k)=yt0(8*i-6) + n_y0(1,1)
	n_xc_a1(1:ndiv,1)=ans_xc_a1((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a1)
	a_rec_tmp(3*(k-1)+1,i)=yt0(8*i-5) + n_y0(1,1)
	n_xc_da1(1:ndiv,1)=ans_xc_da1((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da1)
	da_rec_tmp(3*(k-1)+1,i)=yt0(8*i-4) + n_y0(1,1)
	n_xc_a2(1:ndiv,1)=ans_xc_a2((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a2)
	a_rec_tmp(3*(k-1)+2,i)=yt0(8*i-3) + n_y0(1,1)
	n_xc_da2(1:ndiv,1)=ans_xc_da2((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da2)
	da_rec_tmp(3*(k-1)+2,i)=yt0(8*i-2) + n_y0(1,1)
	n_xc_a3(1:ndiv,1)=ans_xc_a3((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_a3)
	a_rec_tmp(3*(k-1)+3,i)=yt0(8*i-1) + n_y0(1,1)
	n_xc_da3(1:ndiv,1)=ans_xc_da3((i-1)*ndiv+1:i*ndiv)
	n_y0=matmul(vrec,n_xc_da3)
	da_rec_tmp(3*(k-1)+3,i)=yt0(8*i) + n_y0(1,1)
	enddo
	enddo

	do i=1,nnp
	pi_rec_tmp(i,ndiv+1)=icyc*1.d0	
	dpi_rec_tmp(i,ndiv+1)=icyc*1.d0	
	a_rec_tmp(3*ndiv+1,i)=icyc*1.d0
	da_rec_tmp(3*ndiv+1,i)=icyc*1.d0
	enddo

!	open (unit = 28, file = bcname, status = 'unknown'
!     &      ,position='append')
!	do i =17,17
!	write(28,'(1e25.15)') 
!     &		(ans_xc_p(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_dp(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_a1(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_da1(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_a2(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_da2(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_a3(j),j=(i-1)*ndiv+1,i*ndiv),
!     &		(ans_xc_da3(j),j=(i-1)*ndiv+1,i*ndiv)
!	enddo

	do jind =1,ndiv
	do i=1,numnp
	phi_n(i)=pi_rec_tmp(i,jind)
	a_n(1,i)=a_rec_tmp(3*jind-2,i)
	a_n(2,i)=a_rec_tmp(3*jind-1,i)
	a_n(3,i)=a_rec_tmp(3*jind-0,i)
	d_a_n(1,i)=da_rec_tmp(3*jind-2,i)
	d_a_n(2,i)=da_rec_tmp(3*jind-1,i)
	d_a_n(3,i)=da_rec_tmp(3*jind-0,i)
	enddo

	tsj=ts-ndiv+jind
	do i=1,nnp
	do k=1,3
	mechdisp(k,i)=disp((jind-1)*3+k,i)
	mechdisp_1(k,i)=disp_1((jind-1)*3+k,i)
	mechvel(k,i)=vel((jind-1)*3+k,i)
	mechacc(k,i)=acc((jind-1)*3+k,i)
	enddo
	enddo

	call grad_scalar(gradphi,phi_n,x,ix,numnp,numel)
	call curl_vector(bf,a_n,x,ix,numnp,numel)
	do i = 1, numnp
	do j = 1, 3
	ef(j,i)=-1*gradphi(j,i)-d_a_n(j,i)
	enddo
	enddo
	call fidotvector(fivel,mechvel,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,
     &     mechdisp)
	call cross_product(velb,fivel,bf,numnp)
	call get_efield_tilda(etilda,ef,velb,numnp)
	call vectordotci(df,etilda,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp)
	call cross_product(veld,fivel,df,numnp)
	call vectordotc(h_tmp,bf,
     &     numnp,numel,ndm,nel,lint,
     &     x,ix,mat_prop,
     &     mechdisp)
	call get_hfield(hf,h_tmp,veld,numnp,ep,mu)

	open (unit = 28, file = bcname, status = 'unknown'
     &      ,position='append')
	do i =75,75		!1,nnp
	write(28,'(1e25.15)') (ef(j,i),j=1,3),(bf(j,i),j=1,3)
     &	,(mechdisp(j,i),j=1,3),(mechvel(j,i),j=1,3)
	enddo


	enddo







	end subroutine calc_quadratic_reduced_eqn6







