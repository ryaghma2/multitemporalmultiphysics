c$Id:$
      subroutine kine3df_lag(shp,ul,f,fi,c,ci,detfi,lint)

c     Inputs:
c        shp(4,nel,lint)  - Shape functions
c        ul(ndm,nel) - Mechnical nodal displacement values
c        ndm       - Degrees of freedom / node
c        nel       - Number of element nodes

c     Outputs:
c        f(3,3,lint)  - Deformation gradients
c        fi(3,3,lint)   - Inverse deformation gradient
c        detfi(lint)  - Determinant of deformation gradient
c        c(3,3,lint)  - Right Cauchy Green Tensor

      implicit   none

      integer    i,j,k,lint
      
      real*8     shp(4,8,8)
      real*8     f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
      real*8     detfi(8),deti,detfic(8),detic
      real*8     ul(3,8)

      save

c     Compute compatible deformation gradient at t-n+1: F = I + GRAD u
      do i = 1,3
         do j = 1,3
            f(i,j,lint)  = 0.0d0
            fi(i,j,lint) = 0.0d0
            c(i,j,lint)  = 0.0d0
            ci(i,j,lint) = 0.0d0
            do k = 1,8 ! nel
               f(i,j,lint) = f(i,j,lint) + ul(i,k)*shp(j,k,lint)
            end do
         end do
         f(i,i,lint) = f(i,i,lint) + 1.0d0
      end do

c     Invert F
      detfi(lint) = f(1,1,lint)*f(2,2,lint)*f(3,3,lint)
     &            + f(1,2,lint)*f(2,3,lint)*f(3,1,lint)
     &            + f(1,3,lint)*f(2,1,lint)*f(3,2,lint)
     &            - f(3,1,lint)*f(2,2,lint)*f(1,3,lint)
     &            - f(3,2,lint)*f(2,3,lint)*f(1,1,lint)
     &            - f(3,3,lint)*f(2,1,lint)*f(1,2,lint)

      deti    = 1.0d0/detfi(lint)
      
      fi(1,1,lint) = (f(2,2,lint)*f(3,3,lint)
     &             - f(3,2,lint)*f(2,3,lint))*deti
      fi(1,2,lint) =-(f(1,2,lint)*f(3,3,lint)
     &             - f(3,2,lint)*f(1,3,lint))*deti
      fi(1,3,lint) = (f(1,2,lint)*f(2,3,lint)
     &             - f(2,2,lint)*f(1,3,lint))*deti
      fi(2,1,lint) =-(f(2,1,lint)*f(3,3,lint)
     &             - f(3,1,lint)*f(2,3,lint))*deti
      fi(2,2,lint) = (f(1,1,lint)*f(3,3,lint)
     &             - f(3,1,lint)*f(1,3,lint))*deti
      fi(2,3,lint) =-(f(1,1,lint)*f(2,3,lint)
     &             - f(2,1,lint)*f(1,3,lint))*deti
      fi(3,1,lint) = (f(2,1,lint)*f(3,2,lint)
     &             - f(3,1,lint)*f(2,2,lint))*deti
      fi(3,2,lint) =-(f(1,1,lint)*f(3,2,lint)
     &             - f(3,1,lint)*f(1,2,lint))*deti
      fi(3,3,lint) = (f(1,1,lint)*f(2,2,lint)
     &             - f(2,1,lint)*f(1,2,lint))*deti

c     C : Right Cauchy Green Tensor
      c(1,1,lint) = f(1,1,lint)*f(1,1,lint) + f(1,2,lint)*f(1,2,lint)
     &            + f(1,3,lint)*f(1,3,lint)
      c(2,1,lint) = f(1,1,lint)*f(2,1,lint) + f(1,2,lint)*f(2,2,lint)
     &            + f(1,3,lint)*f(2,3,lint)
      c(3,1,lint) = f(1,1,lint)*f(3,1,lint) + f(1,2,lint)*f(3,2,lint)
     &            + f(1,3,lint)*f(3,3,lint)
      c(1,2,lint) = f(2,1,lint)*f(1,1,lint) + f(2,2,lint)*f(1,2,lint)
     &            + f(2,3,lint)*f(1,3,lint)
      c(2,2,lint) = f(2,1,lint)*f(2,1,lint) + f(2,2,lint)*f(2,2,lint)
     &            + f(2,3,lint)*f(2,3,lint)
      c(3,2,lint) = f(2,1,lint)*f(3,1,lint) + f(2,2,lint)*f(3,2,lint)
     &            + f(2,3,lint)*f(3,3,lint)
      c(1,3,lint) = f(3,1,lint)*f(1,1,lint) + f(3,2,lint)*f(1,2,lint)
     &            + f(3,3,lint)*f(1,3,lint)
      c(2,3,lint) = f(3,1,lint)*f(2,1,lint) + f(3,2,lint)*f(2,2,lint)
     &            + f(3,3,lint)*f(2,3,lint)
      c(3,3,lint) = f(3,1,lint)*f(3,1,lint) + f(3,2,lint)*f(3,2,lint)
     &            + f(3,3,lint)*f(3,3,lint)

c     Invert C
      detfic(lint) = c(1,1,lint)*c(2,2,lint)*c(3,3,lint)
     &              + c(1,2,lint)*c(2,3,lint)*c(3,1,lint)
     &              + c(1,3,lint)*c(2,1,lint)*c(3,2,lint)
     &              - c(3,1,lint)*c(2,2,lint)*c(1,3,lint)
     &              - c(3,2,lint)*c(2,3,lint)*c(1,1,lint)
     &              - c(3,3,lint)*c(2,1,lint)*c(1,2,lint)

      detic    = 1.0d0/detfic(lint)

      ci(1,1,lint) = (c(2,2,lint)*c(3,3,lint) - c(3,2,lint)*c(2,3,lint))
     &             *detic
      ci(1,2,lint) =-(c(1,2,lint)*c(3,3,lint) - c(3,2,lint)*c(1,3,lint))
     &             *detic
      ci(1,3,lint) = (c(1,2,lint)*c(2,3,lint) - c(2,2,lint)*c(1,3,lint))
     &             *detic
      ci(2,1,lint) =-(c(2,1,lint)*c(3,3,lint) - c(3,1,lint)*c(2,3,lint))
     &             *detic
      ci(2,2,lint) = (c(1,1,lint)*c(3,3,lint) - c(3,1,lint)*c(1,3,lint))
     &             *detic
      ci(2,3,lint) =-(c(1,1,lint)*c(2,3,lint) - c(2,1,lint)*c(1,3,lint))
     &             *detic
      ci(3,1,lint) = (c(2,1,lint)*c(3,2,lint) - c(3,1,lint)*c(2,2,lint))
     &             *detic
      ci(3,2,lint) =-(c(1,1,lint)*c(3,2,lint) - c(3,1,lint)*c(1,2,lint))
     &             *detic
      ci(3,3,lint) = (c(1,1,lint)*c(2,2,lint) - c(2,1,lint)*c(1,2,lint))
     &             *detic

      end

