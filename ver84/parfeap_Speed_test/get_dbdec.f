c$Id:$
      subroutine get_dbdec(
     &     dbdec,nres,nresmin,
     &     adim,type)

      implicit none


	 integer :: n,nres,nresmin,i,adim

	 character :: type*20

	 real*8  :: v(adim),c(adim)
	 real*8  :: dbdec(adim,adim)

	 n=adim
	 do i=1,n

	    v=0.d0
	    v(i)=1.d0
	    call daub_dbdec(c,v,nresmin,n,type)
	    dbdec(1:n,i)=c(1:n)

	 enddo




      end subroutine get_dbdec





