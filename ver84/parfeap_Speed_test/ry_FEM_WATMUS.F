
	subroutine ry_FEM_WATMUS(
     &     mat_prop,
     &     ts,dt,
     &     w_em,mag_em,
     &     idiv,ndiv,nwav,
     &     nres,dn,dnprev,ind,
     &     cyc_save,tcyc,niters, 	 

     &     gnodeix,geqnix,geqix,
     &     ggn,

     &     x_cm,ix_cm,
     &     mechvel_cm,mechacc_cm,
     &     mechdisp_cm,mechdisp_1_cm,
     &     VEL,ACC,
     &     DISP,DISP_1,
     &     nsbc_pi_cm,nsbc_a_cm,
     &     sbc_pi_cm,sbc_a_cm,
     &     nnbsurf_cm,
     &     nnbsurfa,
     &     ix_nbsurf_cm,
     &     ix_nbsurfa,
     &     a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &     phi_n_cm,d_phi_n_cm,
     &     hfield_cm,dfield_cm,
     &     bfield_cm,efield_cm,
     &     y02,y01)



      implicit  none
#     include   "finclude/petsc.h"
#     include   "pointer.h"
#     include   "comblk.h"
#     include   "pfeapb.h"
#     include   "sdata.h"
#     include   "cdata.h"
#     include   "time_flag.h"
#     include   "comfil.h"


      integer   ts,timesteps
      integer   i,j,k,m,n
      integer   nsbc_pi_cm,nsbc_a_cm,nnbsurf_cm,nnbsurfa
      integer   ix_cm(nen1,*),gnodeix(8,numel)
      integer   geqix(numnp)
      integer   geqnix(8,numel), ggn(numnp-numpn)
      integer   plotnode

      real*8    sbc_pi_cm(2,*),sbc_a_cm(2,*)
      integer   ix_nbsurf_cm(6,*),ix_nbsurfa(6,*)
      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me
      real*8    mat_prop(3,2)
      real*8    x_cm(ndm,*)
      real*8    hfield_cm(3,numnp),dfield_cm(3,numnp)
      real*8    bfield_cm(3,numnp),efield_cm(3,numnp)
      real*8    a_n_1_cm(3,numnp),a_n_cm(3,numnp)
      real*8    d_a_n_cm(3,numnp),dd_a_n_cm(3,numnp)
      real*8    phi_n_cm(numnp),d_phi_n_cm(numnp)
      real*8    mechvel_cm(3,numnp),mechacc_cm(3,numnp)
      real*8    mechdisp_cm(3,numnp),mechdisp_1_cm(3,numnp)

	 real*8  :: vel_i(3,numnp),acc_i(3,numnp)
	 real*8  :: disp_i(3,numnp),disp_1_i(3,numnp)
	 real*8  :: vel_pre(3,numnp),acc_pre(3,numnp)
	 real*8  :: disp_pre(3,numnp),disp_1_pre(3,numnp)
	 real*8  :: vel_cur(3,numnp),acc_cur(3,numnp)
	 real*8  :: disp_cur(3,numnp),disp_1_cur(3,numnp)

	 integer :: nresmin,nitermax,icyc
	 integer :: ierror,dnprev,iflag1
	 integer :: counter
	 integer :: currentcyc,nwav
	 integer :: nres,idiv,ndiv,ncycst,ncyce,nnp,nel
	 integer :: dnmin,dnmax,dn,ts1,ts2,ind,indcyc
	 integer :: cyc_save(10000),niters(10000)
	 integer :: niter_output,niter_min,niter_tmp
	 integer :: i1,ii,ii1,row,row1,col,col1 
	 integer :: cont1,cont2
	 integer :: dbind(nwav)

	 real*8  :: pi
	 real*8  :: tpe,tpu
	 real*8  :: rr
	 real*8  :: tol_y0,tol_dn,tolc
	 real*8  :: sumr0,sumy0,dn_error
	 real*8  :: tcyc(10000)
	 real*8  :: y01d(8*numnp),y01(8*numnp),y02(8*numnp)
	 real*8  :: y0(8*numnp)
	 real*8  :: dbdec(ndiv,ndiv),dbrec(ndiv,ndiv),integmat(ndiv,ndiv)
	 real*8  :: dmatint(ndiv,ndiv)
	 real*8  :: dbrecr(ndiv,nwav),dmatintr(ndiv,nwav)
	 real*8  :: y0_tmp(8*numnp)
	 real*8  :: ytp_tmp(8*numnp)
	 real*8  :: dy_tmp(8*numnp),yc_tmp(8*numnp*ndiv)
	 real*8  :: y0_min(8*numnp)
	 real*8  :: ytp_min(8*numnp)
	 real*8  :: dy_min(8*numnp),yc_min(8*numnp*ndiv)
	 real*8  :: yc(8*numnp*ndiv)
	 real*8  :: y(8*numnp*ndiv)
	 
	 
	 real*8  :: DISP(3*ndiv,numnp),DISP_1(3*ndiv,numnp)
	 real*8  :: VEL(3*ndiv,numnp),ACC(3*ndiv,numnp)
	 real*8  :: DISPt(3*ndiv,numnp),DISP_1t(3*ndiv,numnp)
	 real*8  :: VELt(3*ndiv,numnp),ACCt(3*ndiv,numnp)
	 real*8  :: t1,t2,t3,t4
	 real*8  :: pi_rec_tmp(numnp,ndiv+1)
	 real*8  :: dpi_rec_tmp(numnp,ndiv+1)
	 real*8  :: a_rec_tmp(3*ndiv+1,numnp)
	 real*8  :: da_rec_tmp(3*ndiv+1,numnp)
	 real*8  :: pi_rec_min(numnp,ndiv+1)
	 real*8  :: dpi_rec_min(numnp,ndiv+1)
	 real*8  :: a_rec_min(3*ndiv+1,numnp)
	 real*8  :: da_rec_min(3*ndiv+1,numnp)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 PetscErrorCode   ierr
	 PetscOffset i_x
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 Vec VecDy,VecDym,VecDdy,VecDdym
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	PetscScalar :: dot
	PetscReal :: Dydot,Dymdot,Ddydot,Ddymdot
	PetscReal :: Dyymdot,Ddydymdot
	
	PetscScalar :: y_PS(1),ymin_PS(1),dy_PS(1),dymin_PS(1)
	real*8  :: y_pfv(4*numtn),ymin_pfv(4*numtn)
	real*8  :: dy_pfv(4*numtn),dymin_pfv(4*numtn)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 integer :: ldem(4*8,5)
	 integer :: lext,lnode
	 real*8  :: ydytol(2)

	 real*8  :: val
	 integer :: ts_tmp,icyc_min
	 
	 character :: wname*20
	 integer :: icyc_next

	 integer :: ti,tind,kjj
	 real*8  :: dts
	 integer :: ti_tmp,ic,dni
	 real*8  :: tf

	 save

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	INPUTS

c	MISCELLANEOUS
	ndm=3
	nnp=numnp; nel=numel;
		
	mag_me = 0.001d0              
	w_me = 628.318           

	nresmin=3
	wname='db8'		!'bior31'

	dnmin=floor(w_em*1.d0/w_me/50.d0)
	dnmax=floor(w_em*1.d0/w_me/25.d0)
  	dts=(dt/(dnmax*ndiv))
	tpe=dt/dnmax
	ts1=dnmax*ndiv+1
	ts2=2*dnmax*ndiv+1
	idiv=1
	
	tol_y0=10.0d0**(-5.0d0)
	tol_dn=10.0d0**(-1.0d0)
	tolc=10.0d0**(-4.0d0)
	nitermax=10

	DISP=0.d0
	DISP_1=0.d0
	VEL=0.d0
	ACC=0.d0
	DISPt=0.d0
	DISP_1t=0.d0
	VELt=0.d0
	ACCt=0.d0

	iflag1=0

!	icyc=ts
!	write(*,*) 'icyc =',icyc

	if (ts.le.2) then

		do ti=(ts-1)*ndiv*dnmax+ts,ts*ndiv*dnmax+1
		  write(*,*) 'ti = ',ti
		  if (ti.eq.1) then

			do i=1,nnp
			do k=1,3
				disp_i(k,i)=0.d0
				disp_1_i(k,i)=0.d0
				vel_i(k,i)=0.d0
				acc_i(k,i)=0.d0
				disp_pre(k,i)=0.d0
				disp_1_pre(k,i)=0.d0
				vel_pre(k,i)=0.d0
				acc_pre(k,i)=0.d0
			enddo
			enddo

			call fem_maxwell_cm_init_rypa(
     &           hfield_cm,dfield_cm,bfield_cm,efield_cm,
     &           phi_n_cm,d_phi_n_cm,
     &           a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &           ti,dts,w_me,mag_me,w_em,mag_em,
     &           x_cm,ix_cm,mat_prop,numnp,numel,
     &           vel_i,acc_i,
     &           disp_i,disp_1_i,
     &           ix_nbsurf_cm,nnbsurf_cm,
     &           sbc_pi_cm,nsbc_pi_cm,sbc_a_cm,nsbc_a_cm
     &           )

			startflag = .false.
				
		  else

			tf=(ti-(ts-1)*ndiv*dnmax-1)*1.d0/(ndiv*dnmax)
			do i=1,nnp
			do k=1,3
				disp_i(k,i)=
     &			  tf*(mechdisp_cm(k,i)-
     &					disp_pre(k,i)) + disp_pre(k,i)
				disp_1_i(k,i)=
     &			  tf*(mechdisp_1_cm(k,i)-
     &					disp_1_pre(k,i)) + disp_1_pre(k,i)
				vel_i(k,i)=
     &			  tf*(mechvel_cm(k,i)-
     &					vel_pre(k,i)) + vel_pre(k,i)
				acc_i(k,i)=
     &			  tf*(mechacc_cm(k,i)-
     &					acc_pre(k,i)) + acc_pre(k,i)
			enddo
			enddo
		 
			call fem_maxwell_cm_rypa(
     &        hfield_cm,dfield_cm,bfield_cm,efield_cm,
     &        phi_n_cm,d_phi_n_cm,
     &        a_n_1_cm,a_n_cm,d_a_n_cm,dd_a_n_cm,
     &        ti,dts,w_me,mag_me,w_em,mag_em,
     &        x_cm,ix_cm,mat_prop,numnp,numel,
     &        vel_i,acc_i,
     &        disp_i,disp_1_i,
     &        ix_nbsurf_cm,nnbsurf_cm,
     &        ix_nbsurfa,nnbsurfa,
     &        sbc_pi_cm,nsbc_pi_cm,sbc_a_cm,nsbc_a_cm
     &        )
		
			if (ti.eq.ts1) then
			do i=1,nnp
				y02(8*i-7)=phi_n_cm(i)
				y02(8*i-6)=d_phi_n_cm(i)
				y02(8*i-5)=a_n_cm(1,i)
				y02(8*i-4)=d_a_n_cm(1,i)
				y02(8*i-3)=a_n_cm(2,i)
				y02(8*i-2)=d_a_n_cm(2,i)
				y02(8*i-1)=a_n_cm(3,i)
				y02(8*i)=d_a_n_cm(3,i)
			enddo

			ind=1
			indcyc=dnmax
			tcyc(ind)=indcyc*tpe
			niters(ind)=1
			cyc_save(ind)=indcyc
			
			elseif (ti.eq.ts2) then
			do i=1,nnp
				y01(8*i-7)=phi_n_cm(i)
				y01(8*i-6)=d_phi_n_cm(i)
				y01(8*i-5)=a_n_cm(1,i)
				y01(8*i-4)=d_a_n_cm(1,i)
				y01(8*i-3)=a_n_cm(2,i)
				y01(8*i-2)=d_a_n_cm(2,i)
				y01(8*i-1)=a_n_cm(3,i)
				y01(8*i)=d_a_n_cm(3,i)
			enddo

			ind=2
			indcyc=2*dnmax	
			tcyc(ind)=indcyc*tpe
			niters(ind)=1
			cyc_save(ind)=indcyc
!		 	dn=dnmin
!		 	dnprev=1
			
			endif
		  endif
		enddo
				
		do i=1,nnp
		do k=1,3
			disp_pre(k,i)=mechdisp_cm(k,i)
			disp_1_pre(k,i)=mechdisp_1_cm(k,i)
			vel_pre(k,i)=mechvel_cm(k,i)
			acc_pre(k,i)=mechacc_cm(k,i)
		enddo
		enddo
	
	else
		
		call get_dbdec(
     &        dbdec,nres,nresmin,
     &        ndiv,wname) 

		do i=1,ndiv
		do j=1,ndiv
			dbrec(i,j)=dbdec(j,i)
		enddo
		enddo

		integmat=0.0d0
		do i=1,ndiv
		do j=1,i
			integmat(i,j)=dts
		enddo
		enddo
		dmatint=matmul(integmat,dbrec)

!		do i=1,nwav
!			dbind(i)=i
!		enddo

		do i=1,16
			dbind(i)=i
		enddo
		dbind(17)=17
		dbind(18)=18
		dbind(19)=59
		dbind(20)=60
		dbind(21)=61
		dbind(22)=62
		dbind(23)=63
		dbind(24)=64

		do i=1,ndiv
		do j=1,nwav
			k=dbind(j)
			dbrecr(i,j)=dbrec(i,k)
			dmatintr(i,j)=dmatint(i,k)
		enddo
		enddo

			if (ts.eq.3) then
				dnprev=dnmax
			endif
		
			dn=dnmin
			rr=dnprev*1.d0/dn
			currentcyc=(ts-1)*dnmax+dn
			niter_output=1
			ti=currentcyc*ndiv+1
			write(*,*) 'icyc dn_min=', currentcyc

			ic=dn
			do i=1,nnp
			do k=1,3
				tf=(ic*1.d0/dnmax)
				disp_cur(k,i)=
     &		 	   tf*(mechdisp_cm(k,i) - 
     &		  	   disp_pre(k,i)) +
     &		  	   disp_pre(k,i)
				disp_1_cur(k,i)=
     &		        tf*(mechdisp_1_cm(k,i) - 
     &		        disp_1_pre(k,i)) +
     &		        disp_1_pre(k,i)
				vel_cur(k,i)=
     &		        tf*(mechvel_cm(k,i) - 
     &		        vel_pre(k,i)) +
     &		        vel_pre(k,i)
				acc_cur(k,i)=
     &		        tf*(mechacc_cm(k,i) - 
     &		        acc_pre(k,i)) + 
     &		        acc_pre(k,i)

			do idiv=1,ndiv
				tf=((ic-1)*ndiv+idiv)*1.d0/(dnmax*ndiv)
				DISP((idiv-1)*3+k,i)=
     &		 	   tf*(mechdisp_cm(k,i) - 
     &		  	   disp_pre(k,i)) +
     &		  	   disp_pre(k,i)
				DISP_1((idiv-1)*3+k,i)=
     &		        tf*(mechdisp_1_cm(k,i) - 
     &		        disp_1_pre(k,i)) +
     &		        disp_1_pre(k,i)
				VEL((idiv-1)*3+k,i)=
     &		        tf*(mechvel_cm(k,i) - 
     &		        vel_pre(k,i)) +
     &		        vel_pre(k,i)
				ACC((idiv-1)*3+k,i)=
     &		        tf*(mechacc_cm(k,i) - 
     &		        acc_pre(k,i)) + 
     &		        acc_pre(k,i)
			enddo
			enddo
			enddo

			tol_y0=10.d0**(-10.d0)

		call calc_quadratic_reduced_eqn2(
     &		pi_rec_min,dpi_rec_min,
     &		a_rec_min,da_rec_min,
     &		ytp_min,dy_min,yc_min,
     &		niter_output,nitermax,dn,tol_y0,tolc,
     &		rr,ndiv,currentcyc,ti,dts,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		y02,y01,yc,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x_cm,ix_cm,mat_prop,
     &		sbc_pi_cm,sbc_a_cm,ix_nbsurf_cm,ix_nbsurfa,
     &		nsbc_pi_cm,nsbc_a_cm,nnbsurf_cm,nnbsurfa,
     &		DISP,DISP_1,VEL,ACC,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		nnp,nel,ndm,
     &		gnodeix,geqnix,geqix)
	 	 
	 	 
			dn=dnmax
			rr=dnprev*1.d0/dn
			currentcyc=ts*dnmax
			niter_output=1
			ti=currentcyc*ndiv+1

			do while (iflag1 .eq. 0)

				ic=dn
				tf=1.d0*ic/dnmax
				do i=1,nnp
				do k=1,3
				do idiv=1,ndiv
					DISP((idiv-1)*3+k,i)=
     &		 	  		tf*(mechdisp_cm(k,i) - 
     &		  	  		disp_pre(k,i)) +
     &		  	 	 	disp_pre(k,i)
					DISP_1((idiv-1)*3+k,i)=
     &		      		tf*(mechdisp_1_cm(k,i) - 
     &		      		disp_1_pre(k,i)) +
     &		      		disp_1_pre(k,i)
					VEL((idiv-1)*3+k,i)=
     &		      		tf*(mechvel_cm(k,i) - 
     &		      		vel_pre(k,i)) +
     &		      		vel_pre(k,i)
					ACC((idiv-1)*3+k,i)=
     &		      		tf*(mechacc_cm(k,i) - 
     &		      		acc_pre(k,i)) + 
     &		      		acc_pre(k,i)
				enddo
				enddo
				enddo

				write(*,*) 'icyc dn_min=', currentcyc

				call calc_quadratic_reduced_eqn2(
     &		pi_rec_tmp,dpi_rec_tmp,
     &		a_rec_tmp,da_rec_tmp,
     &		ytp_tmp,dy_tmp,yc_tmp,
     &		niter_output,nitermax,dn,tol_y0,tolc,
     &		rr,ndiv,currentcyc,ti,dts,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		y02,y01,yc,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x_cm,ix_cm,mat_prop,
     &		sbc_pi_cm,sbc_a_cm,ix_nbsurf_cm,ix_nbsurfa,
     &		nsbc_pi_cm,nsbc_a_cm,nnbsurf_cm,nnbsurfa,
     &		DISP,DISP_1,VEL,ACC,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		mechdisp_cm,mechdisp_1_cm,mechvel_cm,mechacc_cm,
     &		nnp,nel,ndm,
     &		gnodeix,geqnix,geqix)


	call VecCreate        (PETSC_COMM_WORLD, VecDy, ierr)
	call VecSetSizes      (VecDy, 4*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(VecDy, ierr)
	call VecCreate        (PETSC_COMM_WORLD, VecDym, ierr)
	call VecSetSizes      (VecDym, 4*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(VecDym, ierr)
	call VecCreate        (PETSC_COMM_WORLD, VecDdy, ierr)
	call VecSetSizes      (VecDdy, 4*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(VecDdy, ierr)
	call VecCreate        (PETSC_COMM_WORLD, VecDdym, ierr)
	call VecSetSizes      (VecDdym, 4*numpn, PETSC_DECIDE, ierr)
	call VecSetFromOptions(VecDdym, ierr)
				
	do n = 1,numel
		call assconne(n,ix_cm(1,n),geqnix(1,n),ldem)

		do m = 1, 4*8
			k=int((m-1)/4.d0)+1
			lext=m-4*(k-1)
			lnode = ix_cm(k,n)

			if(ldem(m,1).gt.0) then
				i = ldem(m,1) - 1
				if(ldem(m,5).ne.0) then
					val = 0.d0
				else
					val = 1.d0/dnmax*(ytp_tmp(8*(lnode-1)+(2*lext-1))-
     &						y01(8*(lnode-1)+(2*lext-1)))
				endif
				call VecSetValue (VecDy, i, val,INSERT_VALUES,ierr)

				if(ldem(m,5).ne.0) then
					val = 0.d0
				else
					val = 1.d0/dnmin*(ytp_min(8*(lnode-1)+(2*lext-1))-
     &						y01(8*(lnode-1)+(2*lext-1)))
				endif
				call VecSetValue (VecDym, i, val,INSERT_VALUES,ierr)
				
				i = ldem(m,1) - 1
				if(ldem(m,5).ne.0) then
					val = 0.d0
				else
					val = 1.d0/dnmax*(ytp_tmp(8*(lnode-1)+(2*lext))-
     &						y01(8*(lnode-1)+(2*lext)))
				endif
				call VecSetValue (VecDdy, i, val,INSERT_VALUES,ierr)
				
				if(ldem(m,5).ne.0) then
					val = 0.d0
				else
					val = 1.d0/dnmin*(ytp_min(8*(lnode-1)+(2*lext))-
     &						y01(8*(lnode-1)+(2*lext)))
				endif
				call VecSetValue (VecDdym, i, val,INSERT_VALUES,ierr)
			   endif
			enddo 
			
		enddo

		call VecAssemblyBegin (VecDy, ierr)
		call VecAssemblyEnd   (VecDy, ierr)
		call VecAssemblyBegin (VecDym, ierr)
		call VecAssemblyEnd   (VecDym, ierr)
		call VecAssemblyBegin (VecDdy, ierr)
		call VecAssemblyEnd   (VecDdy, ierr)
		call VecAssemblyBegin (VecDdym, ierr)
		call VecAssemblyEnd   (VecDdym, ierr)

		Dydot=0.d0
		Dymdot=0.d0
		Dyymdot=0.d0
		Ddydot=0.d0
		Ddymdot=0.d0
		Ddydymdot=0.d0

		call VecDot   (VecDy,VecDy,dot, ierr)
		Dydot=PetscRealPart(dot)
		call VecDot   (VecDym,VecDym,dot, ierr)
		Dymdot=PetscRealPart(dot)
		call VecDot   (VecDy,VecDym,dot, ierr)
		Dyymdot=PetscRealPart(dot)
		call VecDot   (VecDdy,VecDdy,dot, ierr)
		Ddydot=PetscRealPart(dot)			
		call VecDot   (VecDdym,VecDdym,dot, ierr)
		Ddymdot=PetscRealPart(dot)
		call VecDot   (VecDdy,VecDdym,dot, ierr)
		Ddydymdot=PetscRealPart(dot)			

		ydytol(1) = abs(1.d0-Dyymdot/sqrt(Dydot)/sqrt(Dymdot))

		ydytol(2) = abs(1.d0-Ddydymdot/sqrt(Ddydot)/sqrt(Ddymdot))

		call VecDestroy(VecDy,ierr)
		call VecDestroy(VecDym,ierr)
		call VecDestroy(VecDdy,ierr)
		call VecDestroy(VecDdym,ierr)
		
		dn_error=maxval(ydytol)
		write(*,*) 'dn_error =', dn_error

 		if ( dn_error .lt. tol_dn ) then
			iflag1=1
		else
	 
			icyc=icyc-dn
			dn=floor(dn/2.d0)

			if (dn .le. dnmin) then

				dn=dnmax-dnmin
				dnprev=dnmin
				rr=dnprev*1.d0/dn
				currentcyc=ts*dnmax
				niter_output=1
				ti=currentcyc*ndiv+1
				ic=dnmax
				tf=1.d0*ic/dnmax
				do i=1,nnp
				do k=1,3
				do idiv=1,ndiv
					DISP((idiv-1)*3+k,i)=
     &		 	  		tf*(mechdisp_cm(k,i) - 
     &		  	  		disp_pre(k,i)) +
     &		  	 	 	disp_pre(k,i)
					DISP_1((idiv-1)*3+k,i)=
     &		      		tf*(mechdisp_1_cm(k,i) - 
     &		      		disp_1_pre(k,i)) +
     &		      		disp_1_pre(k,i)
					VEL((idiv-1)*3+k,i)=
     &		      		tf*(mechvel_cm(k,i) - 
     &		      		vel_pre(k,i)) +
     &		      		vel_pre(k,i)
					ACC((idiv-1)*3+k,i)=
     &		      		tf*(mechacc_cm(k,i) - 
     &		      		acc_pre(k,i)) + 
     &		      		acc_pre(k,i)
				enddo
				enddo
				enddo

				y02=y01
				y01=ytp_min

				call calc_quadratic_reduced_eqn2(
     &		pi_rec_tmp,dpi_rec_tmp,
     &		a_rec_tmp,da_rec_tmp,
     &		ytp_tmp,dy_tmp,yc_tmp,
     &		niter_output,nitermax,dn,tol_y0,tolc,
     &		rr,ndiv,currentcyc,ti,dts,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		y02,y01,yc,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x_cm,ix_cm,mat_prop,
     &		sbc_pi_cm,sbc_a_cm,ix_nbsurf_cm,ix_nbsurfa,
     &		nsbc_pi_cm,nsbc_a_cm,nnbsurf_cm,nnbsurfa,
     &		DISP,DISP_1,VEL,ACC,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		mechdisp_cm,mechdisp_1_cm,mechvel_cm,mechacc_cm,
     &		nnp,nel,ndm,
     &		gnodeix,geqnix,geqix)

				iflag1=1

			endif

 		endif

		enddo


		y02=y01
		y01=ytp_tmp
		yc=yc_tmp
		dnprev=dn
		ind=ind+1
		cyc_save(ind)=currentcyc
		tcyc(ind)=currentcyc*tpe
		niters(ind)=niter_output
		
		do i=1,nnp
		do k=1,3
			disp_pre(k,i)=mechdisp_cm(k,i)
			disp_1_pre(k,i)=mechdisp_1_cm(k,i)
			vel_pre(k,i)=mechvel_cm(k,i)
			acc_pre(k,i)=mechacc_cm(k,i)
		enddo
		enddo
		
		
	endif
	
	ts = ts + 1
  
	end

