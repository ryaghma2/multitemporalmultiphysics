
       subroutine calc_reduced_mts2(
     &		ans_xcw_p,ans_xcw_dp,
     &		ans_xcw_a1,ans_xcw_da1,
     &		ans_xcw_a2,ans_xcw_da2,
     &		ans_xcw_a3,ans_xcw_da3,
     &		ndiv,icyc,ts,dt,
     &		dbdec,dbrec,dmatint,
     &		dbrecr,dmatintr,nwav,dbind,
     &		yt0e,
     &		w_me,mag_me,w_em,mag_em,tpe,tpu,
     &		x,ix,mat_prop,
     &		sbc_pi,sbc_a,ix_nbsurf,ix_nbsurfa,
     &		nsbc_pi,nsbc_a,nnbsurf,nnbsurfa,
     &		disp,disp_1,vel,acc,
     &		disp_pre,disp_1_pre,vel_pre,acc_pre,
     &		disp_cur,disp_1_cur,vel_cur,acc_cur,
     &		numnp,numel,ndm,
     &		gnodeix,geqnix,geqix)

	implicit   none

#     include   "finclude/petsc.h"
#     include   "pfeapb.h"
#     include   "time_flag.h"
#     include   "pointer.h"
#     include   "comfil.h"
#     include   "comblk.h"
#     include   "setups.h"

C	integer :: numpn
C	integer :: numtn
	integer :: posp,posq          ! specific BC
	integer :: nphi,na
	integer :: nb,mtlidx,ndirec,node1
	integer :: eqn,lnode
	integer :: ig(4),jg(4)
	integer :: grow,lrow,kjj,iloc,jloc

	real*8  :: t
	real*8  :: ep,mu,sigma,g
	real*8  :: k_matrix(32,32)
	real*8  :: f_vector(32)
	real*8  :: c_matrix(32,32)
	real*8  :: m_matrix(32,32)
	real*8  :: F_vec(32)
	real*8  :: aseq1(64,64)
	real*8  :: aseq2(64,64)
	real*8  :: gf1(64)
	real*8  :: Amat1(64,64,64)				!64,64,ndiv
	real*8  :: Amat2(64,64,64)				!!64,64,ndiv
	real*8  :: Fmat1(64,64)				!ndiv
	real*8  :: Fmat2(32,2,numnp,64)		!32,2,nnbsurf,ndiv

	PetscScalar :: xc_PS(1)
	real*8  :: xc_pfv(numnp)	

	real*8  :: e_y0(64)
	real*8  :: e_flux_input(3,4)
	real*8  :: e_flux_input_d(3,4),e_flux_input_h(3,4)
	real*8  :: sg(4,8),xl(3,8)
	real*8  :: shp(4,8,8),xsj(8)
	real*8  :: e_a_n_1(3,8),e_a_n(3,8)
	real*8  :: e_d_a_n(3,8),e_dd_a_n(3,8)
	real*8  :: e_phi_n(8),e_d_phi_n(8)
	real*8  :: e_mechvel(3,8),e_mechacc(3,8)
	real*8  :: e_mechdisp(3,8),e_mechdisp_1(3,8)
	real*8  :: disp_pre(3,numnp),disp_1_pre(3,numnp)
	real*8  :: vel_pre(3,numnp),acc_pre(3,numnp)
	real*8  :: disp_cur(3,numnp),disp_1_cur(3,numnp)
	real*8  :: vel_cur(3,numnp),acc_cur(3,numnp)
	real*8  :: nbc_vector_e(4)
	real*8  :: nbc_vector_e_d(4),nbc_vector_e_h(3*4)
	real*8  :: sg2(3,4),xl2d(2,4),shp2(3,4,4),xsj2(4)
	real*8  :: shp2d(4,4,4)
	real*8  :: f(3,3,8),fi(3,3,8),c(3,3,8),ci(3,3,8)
	real*8  :: detfi(8)
	real*8  :: f_1(3,3,8),fi_1(3,3,8),c_1(3,3,8),ci_1(3,3,8)
	real*8  :: detfi_1(8)
	real*8  :: d_f(3,3,8),d_fi(3,3,8),d_c(3,3,8),d_ci(3,3,8)
	real*8  :: d_detfi(8)
	real*8  :: f2(3,3,4),fi2(3,3,4),c2(3,3,4),ci2(3,3,4)
	real*8  :: f2d(3,3,4),fi2d(3,3,4),c2d(3,3,4),ci2d(3,3,4)
	real*8  :: detfi2(4)
	real*8  :: e_mechdisp2d(2,4)
	real*8  :: tmax
	real*8  :: maxy0,maxr0,sumy0,sumr0

!!!!!!!!!!! data transformation between processors !!!!!!!!!!!
	logical :: setvar, palloc
	integer :: pmaxr, pmaxs
	integer :: ndf

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	integer :: gnodeix(8,numel), geqnix(8,numel) !connectivity of nodes and equations
	integer :: geqix(numnp)
	integer :: p,q
	PetscScalar :: valp,valq
	PetscScalar :: valnbc
	real*8  :: val,vala
	
	real*8  :: ans_xcw_p(numnp*nwav)	
	real*8  :: ans_xcw_dp(numnp*nwav)	
	real*8  :: ans_xcw_a1(numnp*nwav)	
	real*8  :: ans_xcw_da1(numnp*nwav)	
	real*8  :: ans_xcw_a2(numnp*nwav)	
	real*8  :: ans_xcw_da2(numnp*nwav)	
	real*8  :: ans_xcw_a3(numnp*nwav)	
	real*8  :: ans_xcw_da3(numnp*nwav)	
	
	integer :: ldem(4*8,5)
	real*8  :: ub(4*8)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	PetscViewer    K_view, B_view, K_viewcon
!      PetscTruth     chk
      PetscBool     chk
	PetscErrorCode   ierr
	PetscScalar oneS
	PetscScalar moneS
	PetscInt oneI
	PetscOffset i_x
      PC             pcem
      PC             pcemp
      IS     isrowpp !phi-phi index stride
      IS     iscolap !phi-a index stride
      IS     irperm  !index permutation of LU factorization--row
      IS     icperm  !index permutation of LU factorization--column
      MatFactorInfo info(11)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Mat xmatloc     !global assembly matrix

	PetscInt ncol

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Vec xcrhs,xcsol,vecy0

!!!!!!!!!!!!!!!!!!!!!!!!get sub vector!!!!!!!!!!!!!!!!!!!!!!!!!
	PetscScalar valrhs1
	VecScatter toall

	KSP kspxcsol

	PCType      ptype
	PetscScalar      tol

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	integer :: ix(17,*),jind,ic,jc,ip,nc,dni
	real*8  :: sbc_pi(2,nsbc_pi),sbc_a(2,nsbc_a)
	integer :: ix_nbsurf(6,nnbsurf),ix_nbsurfa(6,nnbsurfa)
	integer :: ierror,icyc,i,j,k,l,m,n,nx
	integer :: ii,i1,ii1,jj
	integer :: iflag,niter
	integer :: nitermax,dn,dn1
	integer :: numnp,nnp,numel,ndm,nsbc_pi,nsbc_a
	integer :: nel,lint
	integer :: nnbsurf,nnbsurfa,ndiv,nwav
	integer :: ts,tsj,ngp,iflaginv,node
	integer :: counter2,counter3,row,col,row1,col1

	real*8  :: x(3,numnp)
	real*8  :: dbdec(ndiv,ndiv)
	real*8  :: dbrec(ndiv,ndiv),dmatint(ndiv,ndiv)
	real*8  :: dbreci(ndiv,ndiv),dmatinti(ndiv,ndiv)
	real*8  :: dbrecr(ndiv,nwav),dmatintr(ndiv,nwav)
	integer :: dbind(nwav)

	real*8  :: tpe,tpu,dt
	real*8  :: mag_em,frq_em,w_em,mag_me,frq_me,w_me
	real*8  :: mat_prop(3,2)
	real*8  :: disp(3*ndiv,numnp),disp_1(3*ndiv,numnp)
	real*8  :: vel(3*ndiv,numnp),acc(3*ndiv,numnp)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!ALLOCATABLE REAL

	real*8  :: mechvel(3,numnp),mechacc(3,numnp)
	real*8  :: mechdisp(3,numnp),mechdisp_1(3,numnp)
	real*8  :: tc(ndiv)
	real*8  :: a_n_1(ndm,numnp),a_n(ndm,numnp)
	real*8  :: d_a_n(ndm,numnp),dd_a_n(ndm,numnp)
	real*8  :: phi_n(numnp),d_phi_n(numnp)

	integer :: idiv
	integer :: lext,exced_m,exced_k
	integer :: mloc,kloc
	integer :: mnode,knode
	
	real*8  :: t1,t2,t3,t4,dts
	integer ::	xi,xp
	real*8  ::	fni,fnp,tf
	real*8  :: yt0e(8*numnp)
	real*8  :: yt0(8*numnp)

	save


	data       ig/-1,1,1,-1/,jg/-1,-1,1,1/
	

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	START

c	INITIALIZE

	nel=8
	ndm=3

	lint=8
	nnp=numnp
	nphi=nsbc_pi
	na=nsbc_a

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	XC
		
		i=8*nwav*numpn
		j=8*nwav*numtn


!	call MatCreateMPIBAIJ  (PETSC_COMM_WORLD,8*nwav,i,i,
!     &          PETSC_DETERMINE, PETSC_DETERMINE,
!     &          PETSC_NULL_INTEGER, mr(np(246)),
!     &          PETSC_NULL_INTEGER, mr(np(247)),
!     &          xmatloc, ierr)


          call MatCreate(PETSC_COMM_WORLD,xmatloc,ierr)
          call MatSetSizes(xmatloc,
     &		8*nwav*numpn,8*nwav*numpn,PETSC_DETERMINE,
     &          PETSC_DETERMINE,ierr)
          call MatSetBlockSize(xmatloc,8*nwav,ierr)
          call MatSetFromOptions(xmatloc, ierr)


!		call MatCreate  (PETSC_COMM_WORLD,xmatloc,ierr)
!		call MatSetSizes  (xmatloc,i,i,j,j,ierr)
!		call MatSetType  (xmatloc,MATSUPERLU_DIST,ierr)
		call MatSetType  (xmatloc,MATBAIJ,ierr)
	
		call VecCreate        (PETSC_COMM_WORLD, xcrhs, ierr)
		call VecSetSizes      (xcrhs, i, PETSC_DECIDE, ierr)
		call VecSetFromOptions(xcrhs, ierr)
        
		call VecCreate        (PETSC_COMM_WORLD, xcsol, ierr)
		call VecSetSizes      (xcsol, i, PETSC_DECIDE, ierr)
		call VecSetFromOptions(xcsol, ierr)
	
		oneI = 1.0
		oneS = 1.0

	do n = 1,numel

		Amat1 = 0.0d0
		Amat2 = 0.0d0
		Fmat1 = 0.0d0

c     get material constant and form inverse matrix
		ep = mat_prop(1,2)
		mu = mat_prop(2,2)
		sigma = mat_prop(3,2)

c$    get element coords.
			do i = 1,nel

				eqn = geqnix(i,n)
				node = gnodeix(i,n) !global nodes
				lnode = ix(i,n) !local nodes
				do j = 1,3
					xl(j,i) = x(j,lnode)
            
					e_a_n_1(j,i) = a_n_1(j,lnode)
					e_a_n(j,i) = a_n(j,lnode)
					e_d_a_n(j,i) = d_a_n(j,lnode)
					e_dd_a_n(j,i) = dd_a_n(j,lnode)
               
				end do
				e_phi_n(i) = phi_n(lnode)
				e_d_phi_n(i) = d_phi_n(lnode)

				do j = 1,8
				 e_y0(8*(i-1)+j)=yt0(8*(lnode-1)+j)
				end do
			enddo

c$    shpfunc. values @ integration points
				g = 1.0d0/((3.0d0)**(0.5d0))
				do i = 1,4
					sg(1,i) = ig(i)*g
					sg(1,i+4) = sg(1,i)
					sg(2,i) = jg(i)*g
					sg(2,i+4) = sg(2,i)
					sg(3,i) = -g
					sg(3,i+4) = g
					sg(4,i) = 1.0d0
					sg(4,i+4) = 1.0d0
				end do

				do i = 1,lint
					call shp3d_8(shp,xsj,xl,sg,ndm,nel,i)
					xsj(i) = xsj(i)*sg(4,i)
				end do
         
	   	do jind=1,ndiv

           	tsj=ts-ndiv+jind

           	do i=1,nnp
             	   do k=1,3
                 	mechdisp(k,i)=disp((jind-1)*3+k,i)
                 	mechdisp_1(k,i)=disp_1((jind-1)*3+k,i)
                 	mechvel(k,i)=vel((jind-1)*3+k,i)
                 	mechacc(k,i)=acc((jind-1)*3+k,i)
              	   end do
          		end do

			t = dt*tsj
			tmax=2*3.14157/w_me*10.d0

c     F, Fi, C, Ci
				do i = 1,lint
					do j = 1,3
						do k = 1,3
							f(k,j,i) = 0.0d0
							fi(k,j,i) = 0.0d0
							c(k,j,i) = 0.0d0
							ci(k,j,i) = 0.0d0

							f_1(k,j,i) = 0.0d0
							fi_1(k,j,i) = 0.0d0
							c_1(k,j,i) = 0.0d0
							ci_1(k,j,i) = 0.0d0

							d_f(k,j,i) = 0.0d0
							d_fi(k,j,i) = 0.0d0
							d_c(k,j,i) = 0.0d0
							d_ci(k,j,i) = 0.0d0
						end do
					end do
				end do
         
				do i = 1,lint
					detfi(i) = 0.0d0
					detfi_1(i) = 0.0d0
					d_detfi(i) = 0.0d0
				end do
         
c     n+1 : current step
				do i = 1,8 ! lint
					call kine3df_lag(shp,e_mechdisp,f,fi,c,ci,detfi,i)
				end do
c     n : previous step
				do i = 1,8 ! lint
					call kine3df_lag(shp,e_mechdisp_1,f_1,fi_1,c_1,ci_1,
     &           				detfi_1,i)
				end do
c     rate of f, fi, c, ci
				do i = 1,8
					do j = 1,3
						do k = 1,3
							d_f(k,j,i) = (f(k,j,i) - f_1(k,j,i))/dt
							d_fi(k,j,i) = (fi(k,j,i) - fi_1(k,j,i))/dt
							d_c(k,j,i) = (c(k,j,i) - c_1(k,j,i))/dt
							d_ci(k,j,i) = (ci(k,j,i) - ci_1(k,j,i))/dt
						end do
					end do
					d_detfi(i) = (detfi(i) - detfi_1(i))/dt
				end do


c$    form matrices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c$    get element mech vel.
			do i = 1,nel
				do j = 1,3               
					e_mechvel(j,i) = mechvel(j,lnode)
					e_mechacc(j,i) = mechacc(j,lnode)
					e_mechdisp(j,i) = mechdisp(j,lnode)
					e_mechdisp_1(j,i) = mechdisp_1(j,lnode)
				end do
			enddo

			k_matrix = 0.0d0
			c_matrix = 0.0d0
			m_matrix = 0.0d0
			f_vector = 0.0d0
			F_vec = 0.0d0
			aseq1 = 0.d0
			aseq2 = 0.d0

c$    form elem. matrix of d eqn
c     form k & m elem. matrix of pi in d
         call form_elem_pi_d_rypa(
     &        k_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)
     
c$     form k & c & m elem. matrix of a in d
         call form_elem_a_d_rypa(
     &        k_matrix,c_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

         call form_elem_pi_h_rypa(
     &        k_matrix,c_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)

c     form k & c & m elem. matrix of a in h
         call form_elem_a_h_rypa(
     &        k_matrix,c_matrix,m_matrix,f_vector,n,
     &        x,ix,shp,xsj,nel,ndm,numnp,numel,lint,
     &        tsj,dt,t,ep,mu,sigma,
     &        e_mechvel,e_mechacc,
     &        f,fi,c,ci,detfi,
     &        f_1,fi_1,c_1,ci_1,detfi_1,
     &        d_f,d_fi,d_c,d_ci,d_detfi,
     &        e_phi_n,e_d_phi_n,
     &        e_a_n_1,e_a_n,e_d_a_n,e_dd_a_n)


		call assconne(n,ix(1,n),geqnix(1,n),ldem)


            do m = 1,4*nel
               do i = 1, nphi
               if (ldem(m,1) .eq. sbc_pi(1,i)) then
                  ldem(m,5) = 1
                  ub(m) = sbc_pi(2,i)
               end if
               end do
            end do

            do m = 1,4*nel
               do i = 1, na
               if (ldem(m,1) .eq. sbc_a(1,i)) then
                  ldem(m,5) = 2
                  ub(m) = sbc_a(2,i)
               end if
               end do
            end do


		do i=1,32
		   do j=1,32
			 aseq1(2*i-1,2*i-1)=1.d0
			 aseq1(2*i,2*j)=m_matrix(i,j)
		   enddo
		enddo

		do i=1,32
		   do j=1,32
			 aseq2(2*i-1,2*i)=1.d0
			 aseq2(2*i,2*j-1)=-1.d0*k_matrix(i,j)
			 aseq2(2*i,2*j)=-1.d0*c_matrix(i,j)
		   enddo
		enddo

		do m = 1,4*nel
			if (ldem(m,5) .ne. 0) then
                do j = 1,64
                	aseq1(2*m-1,j)=0.0d0
                	aseq1(j,2*m-1)=0.0d0
                	aseq1(2*m,j)=0.0d0
                	aseq1(j,2*m)=0.0d0

                	aseq2(2*m-1,j)=0.0d0
                	aseq2(j,2*m-1)=0.0d0
                	aseq2(2*m,j)=0.0d0
                	aseq2(j,2*m)=0.0d0
                end do
                aseq1(2*m-1,2*m-1)=1.0d0
                aseq1(2*m,2*m)=1.0d0

                aseq2(2*m-1,2*m-1)=1.0d0
                aseq2(2*m,2*m)=1.0d0
			end if
		end do

		gf1=matmul(aseq2,e_y0)

		do i=1,64
			Fmat1(i,jind)=gf1(i)
			do j=1,64
				Amat1(i,j,jind)=aseq1(i,j)
				Amat2(i,j,jind)=aseq2(i,j)
			enddo
		enddo

	   enddo   	!jind


CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	Assembly of xmatloc
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
	do m = 1,4*nel
		do k = ldem(m,3),ldem(m,4)

		   lext=k-4*int((k-1)/4.d0)
		   knode=int((ldem(k,2)-1)/4.d0)+1

		   do ic=1,nwav
			 do jc=1,nwav
			    do ip=1,ndiv

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	First Quadrant	2i-1,2j-1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)-1) - 1
		j=(knode-1)*8*nwav + (lext-1)*2*nwav + jc - 1
		val=Amat1(2*m-1,2*k-1,ip)*dbrecr(ip,ic)*dbrecr(ip,jc)-
     &	  Amat2(2*m-1,2*k-1,ip)*dbrecr(ip,ic)*dmatintr(ip,jc)
		if (abs(val) .gt. 10.d0**(-50.d0)) then
	call MatSetValue( xmatloc, i, j, val, ADD_VALUES, ierr )
		 endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	Second Quadrant	2i-1,2j
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)-1) - 1
		j=(knode-1)*8*nwav + (lext-1)*2*nwav + nwav + jc - 1
		val=Amat1(2*m-1,2*k,ip)*dbrecr(ip,ic)*dbrecr(ip,jc)-
     &	  Amat2(2*m-1,2*k,ip)*dbrecr(ip,ic)*dmatintr(ip,jc)
		if (abs(val) .gt. 10.d0**(-50.d0)) then
	call MatSetValue( xmatloc, i, j, val, ADD_VALUES, ierr )
		endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	Third Quadrant	2i,2j-1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)  ) - 1
		j=(knode-1)*8*nwav + (lext-1)*2*nwav + jc - 1
		val=Amat1(2*m  ,2*k-1,ip)*dbrecr(ip,ic)*dbrecr(ip,jc)-
     &	  Amat2(2*m  ,2*k-1,ip)*dbrecr(ip,ic)*dmatintr(ip,jc)
		if (abs(val) .gt. 10.d0**(-50.d0)) then
	call MatSetValue( xmatloc, i, j, val, ADD_VALUES, ierr )
		endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	Forth Quadrant	2i,2j
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)  ) - 1
		j=(knode-1)*8*nwav + (lext-1)*2*nwav + nwav + jc - 1
		val=Amat1(2*m  ,2*k  ,ip)*dbrecr(ip,ic)*dbrecr(ip,jc)-
     &	  Amat2(2*m  ,2*k  ,ip)*dbrecr(ip,ic)*dmatintr(ip,jc)
		if (abs(val) .gt. 10.d0**(-50.d0)) then
	call MatSetValue( xmatloc, i, j, val, ADD_VALUES, ierr )
		endif

			    enddo
			 enddo
		   enddo

		enddo
	enddo


CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	Assembly of xcrhs
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
	do m = 1, 4*nel

		if(ldem(m,1).gt.0) then

		   do ic=1,nwav
			 do ip=1,ndiv

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	First half	2i-1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)-1) - 1
		if(ldem(m,5).ne.0) then
			val=0.d0
		else
			val=Fmat1(2*m-1,ip)*dbrecr(ip,ic)
		endif
		call VecSetValue (xcrhs, i, val,ADD_VALUES,ierr)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	Second half	2i
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		i=8*(ic-1)*numtn + (2*ldem(m,1)  ) - 1
		if(ldem(m,5).ne.0) then
			val=0.d0
		else
			val=Fmat1(2*m  ,ip)*dbrecr(ip,ic)
		endif
		call VecSetValue (xcrhs, i, val,ADD_VALUES,ierr)

			 enddo
		   enddo

		endif

	end do

	end do ! end of element loop

	call VecAssemblyBegin (xcrhs, ierr)
	call VecAssemblyEnd   (xcrhs, ierr)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	do ic=1,nwav
	   if (nphi .gt. 0) then
		do p = 1,nphi
			posp = 8*(ic-1)*numtn + 8*int((sbc_pi(1,p)-1)/4.d0) + 1 -1
			valp = 0.d0
		call VecSetValue (xcrhs, posp, valp,INSERT_VALUES,ierr)
		
			posp = 8*(ic-1)*numtn + 8*int((sbc_pi(1,p)-1)/4.d0) + 2 -1
			valp = 0.d0
		call VecSetValue (xcrhs, posp, valp,INSERT_VALUES,ierr)
		
		end do
	   end if

	   if (na .gt. 0) then
		do q = 1,na
			lext=sbc_a(1,q)-4*int((sbc_a(1,q)-1)/4.d0)

			posq = 8*(ic-1)*numtn + 8*int((sbc_a(1,q)-1)/4.d0) + (2*lext-1) -1
			valq = 0.d0
		call VecSetValue (xcrhs, posq,valq,INSERT_VALUES,ierr)

			posq = 8*(ic-1)*numtn + 8*int((sbc_a(1,q)-1)/4.d0) + (2*lext) -1
			valq = 0.d0
		call VecSetValue (xcrhs, posq,valq,INSERT_VALUES,ierr)
		end do
	   end if
	enddo
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	call VecAssemblyBegin (xcrhs, ierr)
	call VecAssemblyEnd   (xcrhs, ierr)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vector!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	do jind=1,ndiv

           tsj=ts-ndiv+jind

           do i=1,nnp
		do k=1,3
			mechdisp(k,i)=disp((jind-1)*3+k,i)
			mechdisp_1(k,i)=disp_1((jind-1)*3+k,i)
			mechvel(k,i)=vel((jind-1)*3+k,i)
			mechacc(k,i)=acc((jind-1)*3+k,i)
		end do
		end do

		t = dt*tsj
		tmax=2*3.14157/w_me*10.d0

		do nb = 1,nnbsurf
         		mtlidx = ix_nbsurf(5,nb)

			do i = 1,4
				nbc_vector_e(i) = 0.0d0
				do j = 1,3
					e_flux_input(j,i) = 0.0d0
				end do
				do j = 1,3
					sg2(j,i) = 0.0d0
				end do
				xsj2(i) = 0.0d0
				do j = 1,4
					do k = 1,3
						shp2(k,j,i) = 0.0d0
					end do
				end do
			end do

			do i = 1,4
				do j = 1,ndm
					e_flux_input(j,i) = mag_em*sin(w_em*t)
C					e_flux_input(j,i) = 
C     &				((t-tmax)/tmax)**2.d0+
C     &				exp(-t/tmax)*sin(w_me*t)+0.8*sin(w_em*t)
				end do
			end do

         		ndirec = ix_nbsurf(6,nb)
			do i = 1,4
           	 	node1 = ix_nbsurf(i,nb)
				if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
					xl2d(1,i) = x(2,node1)
					xl2d(2,i) = x(3,node1)
					e_mechdisp2d(1,i) = mechdisp(2,node1)
					e_mechdisp2d(2,i) = mechdisp(3,node1)
				elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
					xl2d(1,i) = x(3,node1)
					xl2d(2,i) = x(1,node1)
					e_mechdisp2d(1,i) = mechdisp(3,node1)
					e_mechdisp2d(2,i) = mechdisp(1,node1)
				elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
					xl2d(1,i) = x(1,node1)
					xl2d(2,i) = x(2,node1)
					e_mechdisp2d(1,i) = mechdisp(1,node1)
					e_mechdisp2d(2,i) = mechdisp(2,node1)
				endif
			end do

			g = 1.0d0/((3.0d0)**(0.5d0))
			do i = 1,4
				sg2(1,i) = ig(i)*g
				sg2(2,i) = jg(i)*g
				sg2(3,i) = 1.0d0
			end do

			do i = 1,4 ! lint
				call shp2d_4(shp2,xsj2,xl2d,sg2,2,4,i)
				xsj2(i) = xsj2(i)*sg2(3,i)
			end do

			do i = 1,4
				do j = 1,3
					do k = 1,3
						f2(k,j,i) = 0.0d0
						fi2(k,j,i) = 0.0d0
						c2(k,j,i) = 0.0d0
						ci2(k,j,i) = 0.0d0
						f2d(k,j,i) = 0.0d0
						fi2d(k,j,i) = 0.0d0
						c2d(k,j,i) = 0.0d0
						ci2d(k,j,i) = 0.0d0
					end do
				end do
			end do
         
			do i = 1,4 ! lint
				detfi2(i) = 0.0d0
			end do

			do i = 1,4 ! lint
				call kine2d_lag(shp2,e_mechdisp2d,f2,fi2,c2,ci2,detfi2,i)
			end do

         		ndirec = ix_nbsurf(6,nb)
			do i = 1,4
				do j = 1,4
					if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
						shp2d(1,j,i) = 0.0d0
						shp2d(2,j,i) = shp2(1,j,i)
						shp2d(3,j,i) = shp2(2,j,i)
						shp2d(4,j,i) = shp2(3,j,i)
                  
					elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
						shp2d(1,j,i) = shp2(2,j,i)
						shp2d(2,j,i) = 0.0d0
						shp2d(3,j,i) = shp2(1,j,i)
						shp2d(4,j,i) = shp2(3,j,i)
                  
					elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
						shp2d(1,j,i) = shp2(1,j,i)
						shp2d(2,j,i) = shp2(2,j,i)
						shp2d(3,j,i) = 0.0d0
						shp2d(4,j,i) = shp2(3,j,i)                  
                  
					endif
				end do
			end do

c$    rearrange F
			ndirec = int(ix_nbsurf(6,nb))
			do i = 1,4
				if((ndirec .eq. 1) .or. (ndirec .eq. -1)) then
					f2d(1,1,i) = 1.0d0
					f2d(2,2,i) = f2(1,1,i)
					f2d(2,3,i) = f2(1,2,i)
					f2d(3,2,i) = f2(2,1,i)
					f2d(3,3,i) = f2(2,2,i)
               
				elseif((ndirec .eq. 2) .or. (ndirec .eq. -2)) then
					f2d(1,1,i) = f2(2,2,i)
					f2d(1,3,i) = f2(2,1,i)
					f2d(2,2,i) = 1.0d0
					f2d(3,1,i) = f2(1,2,i)
					f2d(3,3,i) = f2(1,1,i)
               
				elseif((ndirec .eq. 3) .or. (ndirec .eq. -3)) then
					f2d(1,1,i) = f2(1,1,i)
					f2d(1,2,i) = f2(1,2,i)
					f2d(2,1,i) = f2(2,1,i)
					f2d(2,2,i) = f2(2,2,i)
					f2d(3,3,i) = 1.0d0
               
				endif
			end do

c     get : detfi2,Fi, C, Ci
			do i = 1,4 ! lint
				call kine2d_lag_1(shp2,e_mechdisp2d,
     &           		f2d,fi2d,c2d,ci2d,detfi2,i)
			end do

			call natural_bc_d(
     &        		nbc_vector_e,nb,
     &        		ix_nbsurf,
     &        		shp2d,xsj2,
     &        		4,2,nnbsurf,4,
     &        		ep,mu,sigma,
     &        		f2d,fi2d,c2d,ci2d,detfi2,
     &        		e_flux_input)
         
			do i = 1,4

            		grow = 8*geqix(ix_nbsurf(i,nb))-7
				val = 0.d0
				Fmat2(8*i-7,1,nb,jind)=val
				Fmat2(8*i-7,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-6
				lrow = i
            		valnbc=nbc_vector_e(lrow)		!*2.5e16
				val = valnbc		!*2.5d0*10.d0**(16.d0)
				Fmat2(8*i-6,1,nb,jind)=val
				Fmat2(8*i-6,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-5
				val = 0.d0
				Fmat2(8*i-5,1,nb,jind)=val
				Fmat2(8*i-5,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-4
				val = 0.d0
				Fmat2(8*i-4,1,nb,jind)=val
				Fmat2(8*i-4,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-3
				val = 0.d0
				Fmat2(8*i-3,1,nb,jind)=val
				Fmat2(8*i-3,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-2
				val = 0.d0
				Fmat2(8*i-2,1,nb,jind)=val
				Fmat2(8*i-2,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-1
				val = 0.d0
				Fmat2(8*i-1,1,nb,jind)=val
				Fmat2(8*i-1,2,nb,jind)=grow*1.d0

            		grow = 8*geqix(ix_nbsurf(i,nb))-0
				val = 0.d0
				Fmat2(8*i-0,1,nb,jind)=val
				Fmat2(8*i-0,2,nb,jind)=grow*1.d0
			
			end do
		end do
	enddo

	do ic=1,nwav
	do nb=1,nnbsurf
	do k=1,32
	do ip=1,ndiv
			i=int(Fmat2(k,2,nb,1))-1+8*(ic-1)*numtn
			val=(Fmat2(k,1,nb,ip))*dbrecr(ip,ic)
			call VecSetValue(xcrhs,i,val,ADD_VALUES,ierr)
	enddo
	enddo
	enddo
	enddo

	call MatAssemblyBegin (xmatloc, MAT_FLUSH_ASSEMBLY, ierr)
	call MatAssemblyEnd   (xmatloc, MAT_FLUSH_ASSEMBLY, ierr)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	if (nphi .gt. 0) then
!		do p = 1,nphi
!		   do jind=1,ndiv
!			 lnode=int((sbc_pi(1,p)-1)/4.d0)+1
!			 i = 8*(jind-1)*numtn + 8*(lnode-1) + 1 - 1
!			 j = (lnode-1)*8*ndiv + jind - 1
!			 val = 1.d0
!
!		call MatSetValue( xmatloc, i, j, val, INSERT_VALUES, ierr )
!
!			 lnode=int((sbc_pi(1,p)-1)/4.d0)+1
!			 i = 8*(jind-1)*numtn + 8*(lnode-1) + 2 - 1
!			 j = (lnode-1)*8*ndiv + ndiv + jind - 1
!			 val = 1.d0
!
!		call MatSetValue( xmatloc, i, j, val, INSERT_VALUES, ierr )
!
!		   enddo
!		end do
!	end if
!
!	if (na .gt. 0) then
!		do q = 1,na
!		   do jind=1,ndiv
!			 do lext=2,4
!
!			 lnode=int((sbc_a(1,q)-1)/4.d0)+1
!		i = 8*(jind-1)*numtn + 8*(lnode-1) + (2*lext-1) - 1
!		j = (lnode-1)*8*ndiv + 2*(lext-1)*ndiv + jind - 1
!			 val = 1.d0
!
!		call MatSetValue( xmatloc, i, j, val, INSERT_VALUES, ierr )
!
!			 lnode=int((sbc_a(1,q)-1)/4.d0)+1
!		i = 8*(jind-1)*numtn + 8*(lnode-1) + (2*lext  ) - 1
!		j = (lnode-1)*8*ndiv + 2*(lext-1)*ndiv + ndiv + jind - 1
!			 val = 1.d0
!
!		call MatSetValue( xmatloc, i, j, val, INSERT_VALUES, ierr )
!
!			 enddo
!		   enddo 	
!		end do
!	end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	call MatAssemblyBegin (xmatloc, MAT_FINAL_ASSEMBLY, ierr)
	call MatAssemblyEnd   (xmatloc, MAT_FINAL_ASSEMBLY, ierr)
 
	call VecAssemblyBegin (xcrhs, ierr)
	call VecAssemblyEnd   (xcrhs, ierr)


!	call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"xmatloc.m",K_view,
!     &                                ierr)
!	call PetscViewerSetFormat(K_view,PETSC_VIEWER_ASCII_MATLAB,
!     &                                ierr)
!	call MatView             (xmatloc,K_view,ierr)
!	call PetscViewerDestroy  (K_view,ierr)

	
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!--------------KSP solver for the system (Jacobian preconditioner)----------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c	SOLVE XC

	call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"xcrhs.m",B_view,
     &                                ierr)
	call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
     &                                ierr)
	call VecView             (xcrhs,B_view,ierr)
	call PetscViewerDestroy  (B_view,ierr)

	   call KSPCreate        (PETSC_COMM_WORLD, kspxcsol,ierr)
	   call KSPSetOperators  (kspxcsol, xmatloc, xmatloc,
     &                          DIFFERENT_NONZERO_PATTERN,ierr)

	   call KSPGetPC(kspxcsol,pcem,ierr)
	   call PCSetType(pcem,PCLU,ierr)

!	   call KSPSetType(kspxcsol,KSPGMRES,ierr)

	   tol = 1.e-20 
	   call KSPSetTolerances(kspxcsol,tol,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_DOUBLE_PRECISION,
     &        PETSC_DEFAULT_INTEGER,ierr) 

         call KSPSolve (kspxcsol, xcrhs, xcsol, ierr)
	
	call PetscViewerASCIIOpen(PETSC_COMM_WORLD,"xcsol.m",B_view,
     &                                ierr)
	call PetscViewerSetFormat(B_view,PETSC_VIEWER_ASCII_MATLAB,
     &                                ierr)
	call VecView             (xcsol,B_view,ierr)
	call PetscViewerDestroy  (B_view,ierr)

	
!!!!!!!!!!!!!!!!!!!!!!!!!!!! Restore xc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	Phi
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_p ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	dPhi
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 1*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_dp ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_x
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 2*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_a1 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	dA_x
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 3*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_da1 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_y
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 4*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_a2 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	dA_y
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 5*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_da2 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	A_z
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 6*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_a3 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

CCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	dA_z
CCCCCCCCCCCCCCCCCCCCCCCCCCCCC

	do kjj=1,nwav
	   call VecGetArray (xcsol, xc_PS, i_x, ierr)
	   do i = 1,numpn
		   j = (i-1)*8*nwav + 7*nwav + kjj
		   val = xc_PS(i_x + j)
		   xc_pfv (i) = val
	   end do

	   call VecRestoreArray (xcsol,xc_pfv,i_x,ierr)
         
	   pmaxr = 0
	   pmaxs = 0
	   do i = 0, ntasks-1
		pmaxr = max( pmaxr, mr(np(248)+i) )
		pmaxs = pmaxs + mr(np(250)+i)
	   end do
	   pmaxr = pmaxr
	   pmaxs = pmaxs
	   ndf = 1
	   setvar = palloc( 120, 'TEMP0', pmaxr+pmaxs, 2)
	   call psetb (xc_pfv,mr(np(248)),mr(np(249)),
     &              mr(np(250)),mr(np(251)),mr(np( 31)),
     &              ndf, hr(np(120)),hr(np(120)+pmaxr))
	   setvar = palloc( 120, 'TEMP0',    0, 2)

	   do i = 1,numnp
		 ans_xcw_da3 ((i-1)*nwav+kjj)= xc_pfv(i)
	   end do
	enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	end subroutine calc_reduced_mts2







