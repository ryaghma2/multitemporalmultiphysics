c$Id:$
      subroutine init_dbdec(dbmat_dec,norig,n,type)

      implicit  none

	 integer :: n,ist,i,j,norig
	 integer :: dim
	 integer :: col1,col2,ind

	 character :: type*20

	 real*8  :: dbmat_dec(norig,norig)
	 real*8  :: h(100),g(100)


	 h=0.d0

	 if (type .eq. 'db4') then
		dim=4
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db6') then
		dim=6
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db8') then
		dim=8
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db10') then
		dim=10
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db12') then
		dim=12
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db14') then
		dim=14
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db16') then
		dim=16
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db18') then
		dim=18
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db20') then
		dim=20
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db22') then
		dim=22
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db24') then
		dim=24
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db26') then
		dim=26
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db28') then
		dim=28
		call get_filters_db(h,dim)

	 elseif (type .eq. 'db30') then
		dim=30
		call get_filters_db(h,dim)

	 elseif (type .eq. 'sym2') then
		dim=4
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym3') then
		dim=6
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym4') then
		dim=8
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym5') then
		dim=10
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym6') then
		dim=12
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym7') then
		dim=14
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym8') then
		dim=16
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym9') then
		dim=18
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym10') then
		dim=20
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym11') then
		dim=22
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym12') then
		dim=24
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym13') then
		dim=26
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym14') then
		dim=28
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'sym15') then
		dim=30
		call get_filters_sym(h,dim)

	 elseif (type .eq. 'coif1') then
		dim=6
		call get_filters_coif(h,dim)

	 elseif (type .eq. 'coif2') then
		dim=12
		call get_filters_coif(h,dim)

	 elseif (type .eq. 'coif3') then
		dim=18
		call get_filters_coif(h,dim)

	 elseif (type .eq. 'coif4') then
		dim=24
		call get_filters_coif(h,dim)

	 elseif (type .eq. 'coif5') then
		dim=30
		call get_filters_coif(h,dim)

	 elseif (type .eq. 'bior22') then
		dim=6
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior24') then
		dim=10
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior26') then
		dim=14
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior28') then
		dim=18
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior31') then
		dim=4
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior33') then
		dim=8
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior35') then
		dim=12
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior37') then
		dim=16
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior39') then
		dim=20
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior44') then
		dim=10
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior55') then
		dim=12
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'bior68') then
		dim=18
		call get_filters_bior(h,dim,type)

	 elseif (type .eq. 'rbio22') then
		dim=6
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio24') then
		dim=10
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio26') then
		dim=14
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio28') then
		dim=18
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio31') then
		dim=4
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio33') then
		dim=8
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio35') then
		dim=12
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio37') then
		dim=16
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio39') then
		dim=20
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio44') then
		dim=10
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio55') then
		dim=12
		call get_filters_rbio(h,dim,type)

	 elseif (type .eq. 'rbio68') then
		dim=18
		call get_filters_rbio(h,dim,type)

	 else

		write(*,*) 'no such wavelet available'

	 endif




	 dbmat_dec=0.d0

	 g=0.d0
	 do i=1,dim
		g(i)=(-1.d0)**(i+1)*h(dim-i+1)
	 enddo

	 do i=1,2+n-dim,2
		ist=i-1
		do j=1,dim
			dbmat_dec(i,ist+j)  =h(j)
			dbmat_dec(i+1,ist+j)=g(j)
		enddo
	 enddo

	 ind=1
	 do i=3+n-dim,n,2

		col1=n-dim+2*ind+1
		col2=1
		do while (col1 .le. n)
			dbmat_dec(i,col1)    =h(col2)
			dbmat_dec(i+1,col1)  =g(col2)

			col1=col1+1
			col2=col2+1
	 	enddo

		col1=1
		col2=dim-2*ind+1
		do while (col2 .le. dim)
			dbmat_dec(i,col1)    =h(col2)
			dbmat_dec(i+1,col1)  =g(col2)

			col1=col1+1
			col2=col2+1
	 	enddo

		ind=ind+1
	 enddo


      end subroutine init_dbdec
      






