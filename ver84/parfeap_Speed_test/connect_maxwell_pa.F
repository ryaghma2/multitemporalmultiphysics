      
	subroutine connect_maxwell_pa ( 
     &     mat_prop,
     &     idiv,ndiv,
     &     nres,dn,dnprev,ind
     &     )

      implicit  none

#     include   "finclude/petsc.h"
#     include   "pointer.h"
#     include   "comblk.h"
#     include   "pfeapb.h"
#     include   "cdata.h"
#     include   "sdata.h"
#     include   "time_data.h"
#     include   "BC.h"
#     include   "dn_data.h"

	logical   setvar,ualloc

	integer :: idiv,ndiv,nwav
	integer :: nres,ind,dn,dnprev
	integer :: cyc_save(10000),niters(10000) 
	real*8  :: tcyc(10000) 
	real*8    mat_prop(3,2)
	real*8    mag_em,frq_em,w_em
	real*8    pi

	real*8    k_matrix(32,32),c_matrix(32,32)
	real*8    m_matrix(32,32),f_vector(32),F_vec(32)
	integer   nel,lint
	real*8    mag_me,frq_me,w_me

      save

      pi = 3.1415926535 ! pi
      
!      mat_prop(1,1) = 1.0d0	!8.854 ! ep_0 : free space permittivity
!      mat_prop(2,1) = 1.0d0	!4.0d0 ! mu_0 : free space permeability
!      mat_prop(3,1) = 1.0d0     ! to avoid singularity
!      mat_prop(1,2) = 1.0d0	!8.0d0 ! ep_r*ep_0 : aluminium
!      mat_prop(2,2) = 1.0d0	!1.256 ! mu : aluminium
!      mat_prop(3,2) = 1.0d0	!37.8 ! sigma : aluminium conductivity

! VM167 materials
!      mat_prop(1,1) = 8.854*10.d0**(-12.d0) ! ep_0 : free space permittivity
!      mat_prop(2,1) = 4.0d0*pi*10.d0**(-7.d0) ! mu_0 : free space permeability
!      mat_prop(3,1) = 1.0d0     ! to avoid singularity
!      mat_prop(1,2) = 8.0d0*8.854*10.d0**(-12.d0) ! ep_r*ep_0 : aluminium
!      mat_prop(2,2) = 4.0d0*pi*10.d0**(-7.d0) ! mu_0 : free space permeability
!      mat_prop(3,2) = 2.5*10.d0**(6.d0) ! sigma : aluminium conductivity FOR VM167 Ansys

      mat_prop(1,1) = 8.854*10.d0**(-12.d0) ! ep_0 : free space permittivity
      mat_prop(2,1) = 4.0d0*pi*10.d0**(-7.d0) ! mu_0 : free space permeability
      mat_prop(3,1) = 1.0d0     ! to avoid singularity
      mat_prop(1,2) = 8.0d0*8.854*10.d0**(-12.d0) ! ep_r*ep_0 : aluminium
      mat_prop(2,2) = 1.256665*10.d0**(-6.d0) ! mu : aluminium
      mat_prop(3,2) = 37.8*10.d0**(6.d0) ! sigma : aluminium conductivity

      mag_em = 200              	! mag. of current flux
      frq_em = 10000.d0		! Hz
      w_em = 2.0d0*pi*frq_em    	! angular velocity


	 nres=4		!6
	 nwav=16		!19


c     fem_maxwell
      call ry_reduced_SLUSNES(
     &     mat_prop,
     &     ts,dt,
     &     w_em,mag_em,
     &     idiv,ndiv,nwav,
     &     nres,dn,dnprev,ind,
     &     cyc_save,tcyc,niters,

     &     mr(up(1)),mr(up(2)),mr(up(22)),
     &     mr(up(21)),

     &     hr(np(43)),mr(np(33)),
     &     hr(up(13)),hr(up(14)),
     &     hr(up(15)),hr(up(16)),
     &     hr(up(51)),hr(up(52)),
     &     hr(up(53)),hr(up(54)),
     &     nphi,na,
     &     hr(up(18)),hr(up(19)),
     &     nnbsurf,
     &     nnbsurfa,
     &     mr(up(20)),
     &     mr(up(24)),
     &     hr(up(3)),hr(up(4)),hr(up(5)),hr(up(6)),
     &     hr(up(7)),hr(up(8)),
     &     hr(up(9)),hr(up(10)),
     &     hr(up(11)),hr(up(12)),
     &     hr(up(57)),hr(up(58)))
 
  
      end

