c$Id:$
      subroutine fem_maxwell_cm_pa(
     &           hfield,dfield,bfield,efield,
     &           phi_n,d_phi_n,
     &           a_n_1,a_n,d_a_n,dd_a_n,
     &           ts,dt,w_me,mag_me,w_em,mag_em,
     &           x,ix,mat_prop,numnp,numel,
     &           mechvel,mechacc,
     &           mechdisp,mechdisp_1,
     &           ix_nbsurf,nnbsurf,
     &           ix_nbsurfa,nnbsurfa,
     &           sbc_pi,nphi,sbc_a,na)

      implicit  none

#     include   "finclude/petsc.h"
#     include   "pointer.h"
#     include   "comblk.h"
#     include   "pfeapb.h"
#     include   "sdata.h"

      integer   ts
      integer   nel,lint!,ndm
!      integer   ndm,nel,lint

      integer   numnp,numel,nsbc_pi,nsbc_a
      integer   nnbsurf,nnbsurfa

      integer   numnp2

      integer   ix(nen1,*)
      real*8   sbc_pi(2,nphi),sbc_a(2,na)
      real*8   ix_nbsurf(6,nnbsurf)
      real*8   ix_nbsurfa(6,nnbsurfa)

      integer   m1,m2,i,j

      real*8    dt
      real*8    mag_em,frq_em,w_em,mag_me,frq_me,w_me

      real*8    mat_prop(3,2)

      real*8    x(3,numnp)

      real*8    hfield(3,numnp),dfield(3,numnp)
      real*8    bfield(3,numnp),efield(3,numnp)

      real*8    a_n_1(3,numnp),a_n(3,numnp)
      real*8    d_a_n(3,numnp),dd_a_n(3,numnp)
      real*8    phi_n(numnp),d_phi_n(numnp)

      real*8    gradphi(3,numnp)

      real*8    k_matrix(32,32),f_vector(32)

      real*8    mechvel(3,numnp),mechacc(3,numnp)
      real*8    mechdisp(3,numnp),mechdisp_1(3,numnp)



      real*8    x_ans_s(4*numnp)
      real*8    x_ans_pi(numnp),x_ans_a(3,numnp)

      integer nphi, na

      save

      ndm = 3
      nel = 8
      lint = 8

c$    starting parallel program
c     initialize s and b


c     construct stiffness matrix
      

           call form_pi_a_cm_pa(
     &     k_matrix,f_vector,
     &     x,ix,mat_prop,nel,ndm,numnp,numel,lint,
     &     ts,dt,
     &     phi_n,d_phi_n,
     &     a_n_1,a_n,d_a_n,dd_a_n,
     &     w_me,mag_me,w_em,mag_em,
     &     mechvel,mechacc,
     &     mechdisp,mechdisp_1,
     &     nphi,sbc_pi,na,sbc_a,
     &     ix_nbsurf,nnbsurf,
     &     ix_nbsurfa,nnbsurfa,
     &     mr(up(1)),mr(up(2)),mr(up(22)) 
     &     ) 

           end

